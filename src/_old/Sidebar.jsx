import React, { useState } from 'react';
import Transition from './Transition';

function Sidebar({ isOpen }) {
    return (
        <Transition show={isOpen}>
            {/* Shared parent */}
            <div>
                {/* Background overlay */}
                <Transition
                    enter="transition-opacity ease-linear duration-300"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="transition-opacity ease-linear duration-300"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                >
                    {/* ... */}
                    <span>greg1</span>
                </Transition>

                {/* Sliding sidebar */}
                <Transition
                    enter="transition ease-in-out duration-300 transform"
                    enterFrom="-translate-x-full"
                    enterTo="translate-x-0"
                    leave="transition ease-in-out duration-300 transform"
                    leaveFrom="translate-x-0"
                    leaveTo="-translate-x-full"
                >
                    {/* ... */}
                    <span>greg2</span>
                </Transition>
            </div>
        </Transition>
    );
}

export default Sidebar;
