/* eslint-disable no-undef */
import React, { Children } from 'react';
import { CSSTransition } from 'react-transition-group';

function Transition({
    show = true,
    enter,
    enterFrom,
    enterTo,
    leave,
    leaveFrom,
    leaveTo,
    children,
    appear = false,
    onExited = () => {},
}) {
    const enterClasses = enter.split(' ');
    const enterFromClasses = enterFrom.split(' ');
    const enterToClasses = enterTo.split(' ');
    const leaveClasses = leave.split(' ');
    const leaveFromClasses = leaveFrom.split(' ');
    const leaveToClasses = leaveTo.split(' ');

    return (
        <CSSTransition
            addEndListener={(node, done) => {
                node.addEventListener('transitionend', done, false);
            }}
            appear={appear}
            in={show}
            onEnter={(node) => {
                node.classList.add(...enterClasses, ...enterFromClasses);
            }}
            onEntered={(node) => {
                node.classList.remove(...enterClasses);
            }}
            onEntering={(node) => {
                node.classList.remove(...enterFromClasses);
                node.classList.add(...enterToClasses);
            }}
            onExit={(node) => {
                node.classList.add(...leaveClasses, ...leaveFromClasses);
            }}
            onExited={(node) => {
                node.classList.remove(...leaveClasses);
                onExited();
            }}
            onExiting={(node) => {
                node.classList.remove(...leaveFromClasses);
                node.classList.add(...leaveToClasses);
            }}
            unmountOnExit={true}
        >
            {Children.only(children)}
        </CSSTransition>
    );
}

export default Transition;

// import React, { Children } from 'react';
// import { CSSTransition as ReactCSSTransition } from 'react-transition-group';
// import { useRef, useEffect, useContext } from 'react';

// const TransitionContext = React.createContext({
//     parent: {},
// });

// function useIsInitialRender() {
//     const isInitialRender = useRef(true);
//     useEffect(() => {
//         isInitialRender.current = false;
//     }, []);
//     return isInitialRender.current;
// }

// function CSSTransition({
//     show,
//     enter = '',
//     enterFrom = '',
//     enterTo = '',
//     leave = '',
//     leaveFrom = '',
//     leaveTo = '',
//     appear,
//     children,
// }) {
//     const enterClasses = enter.split(' ').filter((s) => s.length);
//     const enterFromClasses = enterFrom.split(' ').filter((s) => s.length);
//     const enterToClasses = enterTo.split(' ').filter((s) => s.length);
//     const leaveClasses = leave.split(' ').filter((s) => s.length);
//     const leaveFromClasses = leaveFrom.split(' ').filter((s) => s.length);
//     const leaveToClasses = leaveTo.split(' ').filter((s) => s.length);

//     function addClasses(node, classes) {
//         classes.length && node.classList.add(...classes);
//     }

//     function removeClasses(node, classes) {
//         classes.length && node.classList.remove(...classes);
//     }

//     return (
//         <ReactCSSTransition
//             appear={appear}
//             unmountOnExit
//             in={show}
//             addEndListener={(node, done) => {
//                 node.addEventListener('transitionend', done, false);
//             }}
//             onEnter={(node) => {
//                 addClasses(node, [...enterClasses, ...enterFromClasses]);
//             }}
//             onEntering={(node) => {
//                 removeClasses(node, enterFromClasses);
//                 addClasses(node, enterToClasses);
//             }}
//             onEntered={(node) => {
//                 removeClasses(node, [...enterToClasses, ...enterClasses]);
//             }}
//             onExit={(node) => {
//                 addClasses(node, [...leaveClasses, ...leaveFromClasses]);
//             }}
//             onExiting={(node) => {
//                 removeClasses(node, leaveFromClasses);
//                 addClasses(node, leaveToClasses);
//             }}
//             onExited={(node) => {
//                 removeClasses(node, [...leaveToClasses, ...leaveClasses]);
//             }}
//         >
//             {children}
//         </ReactCSSTransition>
//     );
// }

// function Transition({ show, appear, ...rest }) {
//     const { parent } = useContext(TransitionContext);
//     const isInitialRender = useIsInitialRender();
//     const isChild = show === undefined;

//     if (isChild) {
//         return (
//             <CSSTransition
//                 appear={parent.appear || !parent.isInitialRender}
//                 show={parent.show}
//                 {...rest}
//             />
//         );
//     }

//     return (
//         <TransitionContext.Provider
//             value={{
//                 parent: {
//                     show,
//                     isInitialRender,
//                     appear,
//                 },
//             }}
//         >
//             <CSSTransition appear={appear} show={show} {...rest} />
//         </TransitionContext.Provider>
//     );
// }

// export default Transition;
