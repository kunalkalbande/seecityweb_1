import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Loading, Search } from './../../components/common';
import { AccountGrid, AccountList } from './components';
import { setAccountFilter, setMarketFilter } from '../../store/actions';
import { classNames } from '../../utils';
import api from '../../api';

const AccountListPage = () => {
    // setup redux dispatcher
    const dispatch = useDispatch();
    // central state
    const marketFilter = useSelector((state) => state.marketFilter.id);
    const accountFilter = useSelector((state) => state.accountFilter);
    // local state
    const [isGridMode, setIsGridMode] = useState(false);
    const [markets, setMarkets] = useState([]);
    const [venues, setVenues] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [market, setMarket] = useState(marketFilter);
    const [filteredAccounts, setFilteredAccounts] = useState([]);

    const filterVenues = (search) => {
        if (search) dispatch(setAccountFilter(search));
        const results = venues.filter((venue) => {
            if (
                venue.name.toLowerCase().includes(search.toLowerCase()) ||
                venue.address.city.toLowerCase().includes(search.toLowerCase())
            )
                return venue;
        });
        setFilteredAccounts(results);
    };

    // fetch markets on initial load
    useEffect(() => {
        const fetch = async () => {
            const results = await api.fetchMarkets();
            setMarkets(results);
        };
        fetch();
    }, []);

    // observe market changes
    useEffect(() => {
        setIsLoading(true);

        const fetch = async () => {
            const results = await api.fetchAccounts(market);
            setVenues(results);
            setIsLoading(false);
        };
        fetch();
        dispatch(setMarketFilter(market));
    }, [market]);

    // apply central state filters on iniitial load
    useEffect(() => {
        if (accountFilter) {
            filterVenues(accountFilter);
        } else {
            setFilteredAccounts(venues);
        }
    }, [venues]);

    return (
        <div className="px-2 sm:px-3 h-full">
            {/* header */}
            <div className="flex flex-col">
                <div className="grid grid-cols-1 sm:grid-cols-3 col-gap-2 px-2 py-2 bg-white">
                    <div className="mt-1 shadow-sm">
                        <select
                            value={market}
                            id="code"
                            className="form-select block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 rounded-none sm:rounded-sm placeholder-gray-300 placeholder-opacity-75"
                            placeholder="-- Select --"
                            onChange={(event) => setMarket(event.target.value)}
                        >
                            <option value="">-- Select --</option>
                            {markets.map((market, index) => (
                                <option key={index} value={market.id}>
                                    {market.name} ({market.code})
                                </option>
                            ))}
                        </select>
                    </div>
                    <Search onSearch={filterVenues} value={accountFilter} />
                    <div className="mt-1 text-right">
                        <div className="flex flex-row relative z-0 inline-flex shadow-sm">
                            <button
                                type="button"
                                onClick={() => setIsGridMode(false)}
                                className={classNames(
                                    '-ml-px relative inline-flex items-center px-2 py-2 sm:rounded-l-sm border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-sc-blue-500 transition ease-in-out duration-150',
                                    isGridMode ? 'bg-white' : 'bg-gray-200'
                                )}
                            >
                                List
                            </button>
                            <button
                                type="button"
                                onClick={() => setIsGridMode(true)}
                                className={classNames(
                                    '-ml-px relative inline-flex items-center px-2 py-2 sm:rounded-r-sm border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-sc-blue-500 transition ease-in-out duration-150',
                                    !isGridMode ? 'bg-white' : 'bg-gray-200'
                                )}
                            >
                                Grid
                            </button>
                            <button
                                type="submit"
                                className="mx-2 my-0 px-4 py-0 border border-sc-blue-200 text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                            >
                                New
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            {/* main content */}
            {isLoading ? (
                <Loading />
            ) : !isLoading && market && filteredAccounts.length === 0 ? (
                <div className="text-center py-4">
                    <span className="text-sm text-gray-400">
                        No Venues Found
                    </span>
                </div>
            ) : !isGridMode ? (
                <div className="flex flex-col shadow overflow-hidden sm:rounded-b-md">
                    <hr />
                    <AccountList items={filteredAccounts} />
                </div>
            ) : (
                <AccountGrid items={filteredAccounts} />
            )}
        </div>
    );
};

export default AccountListPage;
