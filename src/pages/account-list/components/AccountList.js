import React from 'react';
import AccountListItem from './AccountListItem';

const AccountList = ({ items }) => {
    return (
        <ul className="overflow-auto">
            {items.map((item, index) => (
                <AccountListItem key={index} account={item} />
            ))}
        </ul>
    );
};

export default AccountList;
