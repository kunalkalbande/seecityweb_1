import React from 'react';
import lizard from '../../../assets/img/l.jpg';

const AccountGridItem = ({ account }) => {
    return (
        <div className="flex flex-col sm:rounded-sm shadow-sm overflow-hidden">
            <div className="flex-shrink-0">
                <img className="h-48 w-full object-cover" src={lizard} alt="" />
            </div>
            <div className="flex-1 bg-white p-6 flex flex-col justify-between">
                <div className="flex-1">
                    <p className="text-md leading-5 font-medium text-sc-blue-400">
                        <a href="#" className="hover:underline">
                            {account.name}
                        </a>
                    </p>
                </div>
                <div className="mt-6 flex items-center">
                    <div className="flex-shrink-0">
                        <a href="#">
                            <img
                                className="h-10 w-10 rounded-full"
                                src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                                alt=""
                            />
                        </a>
                    </div>
                    <div className="ml-3">
                        <p className="text-sm leading-5 font-medium text-gray-900">
                            <a href="#" className="hover:underline">
                                Biff Tanner
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

const AccountGrid = ({ items }) => {
    return (
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 grid-flow-row gap-4 my-2">
            {items.map((item, index) => (
                <AccountGridItem key={index} account={item} />
            ))}
        </div>
    );
};

export default AccountGrid;
