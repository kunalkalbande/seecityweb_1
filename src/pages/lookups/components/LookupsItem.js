import React, { useState, useEffect } from 'react';
import { DataGrid, Editing, Pager, Column, ValidationRule, FormItem, DateBox } from 'devextreme-react/data-grid';
import { lookup as lookupApi } from '../../../api/graphql/lookup'
import Loading from '../../../components/common/Loading';

const LookupsItem = (props) => {
    const _lookup = props.data;
    const [lookup, setLookup] = useState([]);
    const [stringTags, setStringTags] = useState([]);
    const [isObjectType, setIsObjectType] = useState(true);
    const [isLoading, setIsLoading] = useState(true);
    const [isDisabled, setIsDisabled] = useState(false);

    useEffect(() => {
        const init = () => {
            reload(_lookup);
            setIsLoading(false);
        }
        init();
    }, []);

    const buildStringArray = (_lookup) => {
        if (typeof _lookup.values[0] === "string") {
            setIsObjectType(false);
            let stringObject = [];
            _lookup.values.forEach(element => {
                let obj = {
                    name: element
                }
                stringObject.push(obj);
            });
            setStringTags(stringObject);
        }
    }

    const reload = async (_lookup) => {
        setLookup(_lookup);
        buildStringArray(_lookup);
    }

    const _create = async (obj) => {
        let look = lookup;
        look.values.pop();
        look.values.push(obj);
        setLookup(look);
        const data = await lookupApi.updateLookup(look);
    }

    const create = async (e) => {
        setIsLoading(true);
        if (isObjectType) {
            if (lookup.type === "Business Tags") {
                let obj = {
                    value: (parseInt((lookup.values[lookup.values.length - 2]).value) + 1) + "",
                    name: e.data.name
                }
                await _create(obj);
            } else {
                let obj = {};
                if (e.data.value) obj.value = e.data.value;
                if (e.data.name) obj.name = e.data.name;
                if (e.data.state) obj.state = e.data.state;
                await _create(obj);
            }
        } else {
            let look = lookup;
            look.values.push(e.data.name);
            setLookup(look);
            const data = await lookupApi.updateLookup(look);
        }
        setIsLoading(false);
    }

    const update = async (e) => {
        setIsLoading(true);
        if (isObjectType) {
            const data = await lookupApi.updateLookup(lookup);
        } else {
            let obj = [];
            stringTags.forEach(element => {
                obj.push(element.name);
            });
            let look = lookup;
            look.values = obj;
            const data = await lookupApi.updateLookup(look);
        }
        setIsLoading(false);
    }

    const remove = async (e) => {
        await update();
    }

    if (isLoading) {
        return (<Loading />);
    }
    return (
        <DataGrid
            disabled={isDisabled}
            className="p-2"
            id="grid"
            dataSource={isObjectType ? lookup.values : stringTags}
            showBorders={true}
            onRowInserted={create}
            onRowUpdated={update}
            onRowRemoved={remove}
        >
            {
                lookup.values[0].value && lookup.type !== "Business Tags" ?
                    (<Column dataField="value" visible={true} >
                        <ValidationRule type="required" />
                    </Column>) : (<></>)
            }
            {
                <Column dataField="name" visible={true} >
                    <ValidationRule type="required" />
                </Column>
            }
            {
                lookup.values[0].state ?
                    (<Column dataField="state" visible={true} >
                        <ValidationRule type="required" />
                    </Column>) : (<></>)
            }
            <Pager
                showPageSizeSelector={true}
                defaultPageSize={10}
                allowedPageSizes={[10, 20, 30]}
                showNavigationButtons={true}
            />
            <Editing
                mode="form"
                allowAdding={true}
                allowUpdating={true}
                allowDeleting={lookup.values.length > 1 ? true : false} />
        </DataGrid>
    );
};

export default LookupsItem;