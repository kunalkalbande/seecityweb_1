import React, { useEffect, useState } from 'react';
import { TabPanel } from 'devextreme-react';
import Loading from '../../components/common/Loading';
import LookupsItem from './components/LookupsItem';
import { lookup as lookupApi } from '../../api/graphql/lookup';

const LookupsPage = () => {
    const [lookupItems, setLookupItems] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [selectedIndex, setSelectedIndex] = useState(0);

    useEffect(() => {
        const init = async () => {
            const data = await lookupApi.getAllLookups();
            setLookupItems(data);
            setIsLoading(false);
        };
        init();
    }, []);

    const itemTitleRender = (lookup) => {
        return <span>{lookup.type}</span>;
    }

    const onSelectionChanged = (args) => {
        if (args.name == 'selectedIndex') {
            setSelectedIndex(args.value);
        }
    }

    if (isLoading) {
        return (<Loading />)
    }
    return (
        <div className="px-2 sm:px-3 h-full">
            <div className="min-h-full grid grid-cols-1 sm:grid-cols-1 col-gap-4">
                <div className="mb-2 flex flex-col overflow-y-hidden">
                    <div className="flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                        <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6 flex items-center justify-between flex-wrap sm:flex-no-wrap">
                            <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                Lookups
                            </h3>
                        </div>
                        <div className="h-full">
                            <TabPanel
                                className="p-2 h-full"
                                dataSource={lookupItems}
                                selectedIndex={selectedIndex}
                                onOptionChanged={onSelectionChanged}
                                itemTitleRender={itemTitleRender}
                                itemComponent={LookupsItem}
                                loop={false}
                                animationEnabled={true}
                                swipeEnabled={true}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LookupsPage;