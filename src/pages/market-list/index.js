import React, { useState, useEffect, useRef } from 'react';
import { Icons } from '../../components/icons';
import { DebounceInput } from 'react-debounce-input';
import { CityList, MarketForm, MarketListItem } from './components';
import { getAllMarkets } from '../../api/graphql/queries';
import { createMarket, updateMarket } from '../../api/graphql/mutations';
import { apolloClient } from '../../api/graphql/apolloClient';
import Loading from '../../components/common/Loading';

function usePrevious(value) {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}

const fetchMarketApi = () => {
    return new Promise((resolve) => {
        setTimeout(async () => {
            var data = await apolloClient.performQuery(getAllMarkets, {});
            resolve(data.markets);
        }, 1000);
    });
};

const createMarketApi = (market) => {
    return new Promise((resolve) => {
        setTimeout(async () => {
            var data = await apolloClient.performMutation(createMarket, { market: market });
            resolve(data.createMarket);
        }, 1000);
    });
};

const updateMarketApi = (market) => {
    return new Promise((resolve) => {
        setTimeout(async () => {
            var data = await apolloClient.performMutation(updateMarket, { market: market });
            resolve(data.updateMarket);
        }, 1000);
    });
};

const Markets = () => {
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const getMarkets = async () => {
            await fetchMarkets0();
        }
        getMarkets();
    }, []);

    const fetchMarkets0 = async () => {
        const results = await fetchMarketApi();
        console.log(results);
        setMarkets(results);
        setFilteredMarkets(results);
        setSelected(results[0]);
        setIsLoading(false);
    };

    const fetchMarkets = async () => {
        const results = await fetchMarketApi();
        setMarkets(results);
        setFilteredMarkets(results);
        //setSelected(results[0]);
        setIsLoading(false);
    };

    const [markets, setMarkets] = useState([]);

    // const [markets, setMarkets] = useState([
    //     {
    //         name: 'Dallas Forth Worth',
    //         selected: false,
    //         code: 'DFW',
    //         cities: [
    //             { id: 1, name: 'Frisco', state: 'TX' },
    //             { id: 2, name: 'Plano', state: 'TX' },
    //             { id: 3, name: 'Allen', state: 'TX' },
    //             { id: 4, name: 'McKinney', state: 'TX' },
    //             { id: 5, name: 'Dallas', state: 'TX' },
    //             { id: 6, name: 'Fort Worth', state: 'TX' },
    //             { id: 7, name: 'South Lake', state: 'TX' },
    //             { id: 8, name: 'Flower Mound', state: 'TX' },
    //             { id: 9, name: 'Keller', state: 'TX' },
    //             { id: 10, name: 'Carrolton', state: 'TX' },
    //             { id: 11, name: 'Richardson', state: 'TX' },
    //         ],
    //     },
    //     {
    //         name: 'New York City',
    //         selected: false,
    //         code: 'NYC',
    //         cities: [
    //             { id: 1, name: 'New York City', state: 'TX' },
    //             { id: 2, name: 'Queens', state: 'TX' },
    //             { id: 3, name: 'Brooklyn', state: 'TX' },
    //             { id: 4, name: 'Bronx', state: 'TX' },
    //             { id: 5, name: 'Long Island', state: 'TX' },
    //             { id: 6, name: 'Staten Island', state: 'TX' },
    //         ],
    //     },
    //     {
    //         name: 'San Diego',
    //         selected: false,
    //         code: 'SDG',
    //         cities: [
    //             { id: 1, name: 'San Diego', state: 'TX' },
    //             { id: 2, name: 'Oceanside', state: 'TX' },
    //             { id: 2, name: 'Encinitas', state: 'TX' },
    //             { id: 2, name: 'Vista', state: 'TX' },
    //             { id: 2, name: 'Del Mar', state: 'TX' },
    //         ],
    //     },
    // ]);

    const [filteredMarkets, setFilteredMarkets] = useState([...markets]);
    const [selected, setSelected] = useState(filteredMarkets[0]);
    const previousSelected = usePrevious(selected);
    const [search, setSearch] = useState('');

    useEffect(() => {
        const results = markets.filter((market) => {
            if (market.name.toLowerCase().includes(search.toLowerCase()))
                return market;
        });
        setFilteredMarkets(results);
    }, [search]);

    const callback = (handler) => {
        return new Promise((resolve) => {
            setTimeout(() => {
                handler();
                resolve();
            }, 1000);
        });
    };

    const addMarket = async (market) => {
        market.selected = false;
        const result = await createMarketApi(market);
        if (result) {
            await fetchMarkets();
        }
        // const handler = () => {
        //     const newMarkets = [...markets, { ...market }];
        //     setFilteredMarkets([...newMarkets]);
        //     console.log({ message: 'Market Added', market: market });
        // };
        // return callback(handler);
    };

    const updateMarket = async (market) => {
        const result = await updateMarketApi(market);
        if (result) {
            await fetchMarkets();
        }
        // console.log("Update -> ", market);
        // const handler = () =>
        //     console.log({ message: 'Market Updated', market: market });
        // return callback(handler);
    };

    useEffect(() => { }, [selected]);

    return (
        <div className="px-2 sm:px-3 h-full">
            <div className="min-h-full grid grid-cols-1 sm:grid-cols-2 col-gap-4">
                <div className="flex flex-col bg-white shadow overflow-hidden sm:rounded-b-md">
                    <div className="h-0 flex-1 flex flex-col">
                        <div className="py-2 relative w-full text-gray-300 focus-within:text-gray-400">
                            <div className="flex items-center">
                                <div className="absolute inset-y-0 left-2 flex items-center pointer-events-none">
                                    <Icons.SEARCH />
                                </div>
                                <DebounceInput
                                    maxLength="10"
                                    minLength={3}
                                    debounceTimeout={200}
                                    id="market_search"
                                    value={search}
                                    onChange={(event) =>
                                        setSearch(event.target.value)
                                    }
                                    className="block w-full h-full pl-8 pr-3 py-2 text-sm text-gray-900 placeholder-gray-300 focus:outline-none focus:placeholder-gray-400 xs:text-sm"
                                    placeholder="Search"
                                />
                                {!search ? (
                                    ''
                                ) : (
                                        <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-auto">
                                            <button
                                                type="button"
                                                className="px-2 py-2"
                                                onClick={() => setSearch('')}
                                            >
                                                <Icons.SM_X_CIRCLE />
                                            </button>
                                        </div>
                                    )}
                            </div>
                        </div>
                        <hr />
                        {!isLoading ? (
                            <ul className="pt-2 pb-2 overflow-auto">
                                {filteredMarkets.map((market, index) => (
                                    <MarketListItem
                                        onSelected={setSelected}
                                        key={index}
                                        index={index}
                                        market={market}
                                        updateMarket={updateMarket}
                                    />
                                ))}
                            </ul>
                        ) : (
                                <Loading />
                            )}

                    </div>
                    <hr />
                    <div className="flex flex-shrink-0 group block bg-white p-2">
                        <MarketForm
                            onSave={addMarket}
                            market={{ name: '', code: '' }}
                        />
                    </div>
                </div>
                {selected ?
                    <CityList
                        onSave={fetchMarkets}
                        market={selected}
                    /> : ""}
            </div>
        </div>
    );
};
export default Markets;
