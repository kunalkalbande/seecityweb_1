import React, { useState, useEffect } from 'react';
import MarketForm from './MarketForm';

const MarketListItem = ({ market, index, updateMarket, onSelected }) => {
    const [isEditMode, setIsEditMode] = useState(false);
    const [selected, setSelected] = useState(false);

    const handleEditClick = () => setIsEditMode(true);

    const save = async () => {
        await updateMarket(market);
        setIsEditMode(false);
    };

    useEffect(() => {
        if (selected) {
            onSelected(market);
            setSelected(false);
        }
    }, [selected]);

    return !isEditMode ? (
        <a
            onClick={() => setSelected(true)}
            href="#"
            className="block hover:bg-sc-blue-100 focus:outline-none focus:bg-sc-blue-100 transition duration-150 ease-in-out"
        >
            <div className="flex items-center px-2 py-2 sm:px-3">
                <div className="min-w-0 flex-1 flex items-center">
                    <div className="text-sm text-sc-blue-300 truncate">
                        {market.name} ({market.code})
                    </div>
                </div>
                <div>
                    <button
                        className="text-xs text-sc-blue-200 underline"
                        onClick={(event) => handleEditClick(event, false)}
                    >
                        Edit
                    </button>
                </div>
            </div>
        </a>
    ) : (
        <div className="p-2">
            <MarketForm
                market={market}
                onSave={save}
                onCancel={() => setIsEditMode(false)}
                mode="Save"
            />
        </div>
    );
};
export default MarketListItem;
