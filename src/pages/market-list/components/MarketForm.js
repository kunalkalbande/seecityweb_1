import React, { useState } from 'react';

const MarketForm = ({ onSave, onCancel, market, mode }) => {
    const [name, setName] = useState(market.name);
    const [code, setCode] = useState(market.code);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (!name || !code) return;
        market.name = name;
        market.code = code;
        onSave(market).then(() => {
            if (!mode) {
                setName('');
                setCode('');
            }
        });
    };

    return (
        <form
            onSubmit={handleSubmit}
            onReset={() => onCancel()}
            className="w-full"
        >
            <div className="grid grid-cols-1 sm:grid-cols-12 col-gap-4">
                <div className="sm:col-span-6">
                    <div className="mt-1 shadow-sm">
                        <input
                            id="name"
                            type="text"
                            className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 rounded-none sm:rounded-sm placeholder-gray-300 placeholder-opacity-75"
                            value={name}
                            placeholder="Name"
                            onChange={(e) => setName(e.target.value)}
                        />
                    </div>
                </div>

                <div className="sm:col-span-3">
                    <div className="mt-1 shadow-sm">
                        <input
                            id="code"
                            className="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 rounded-none sm:rounded-sm placeholder-gray-300 placeholder-opacity-75"
                            value={code}
                            placeholder="Code"
                            onChange={(e) => setCode(e.target.value)}
                        />
                    </div>
                </div>
                <div className="mt-2 sm:col-span-3 text-right">
                    <button
                        type="submit"
                        className="py-1 px-2 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                    >
                        {!mode ? 'New' : 'Save'}
                    </button>
                    {mode ? (
                        <button
                            type="reset"
                            className="ml-1 py-1 px-2 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-gray-dark shadow-sm hover:bg-sc-gray-light focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                        >
                            Cancel
                        </button>
                    ) : (
                        ''
                    )}
                </div>
            </div>
        </form>
    );
};
export default MarketForm;
