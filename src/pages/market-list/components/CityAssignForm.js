import React, { useState, useEffect } from 'react';
import { lookup } from '../../../api/graphql/lookup';

const CityAssignForm = ({ onSave, onNew }) => {
    const [name, setName] = useState(null);
    const [state, setState] = useState('');
    const [cities,setCities]=useState([]);
    // const cities = [
    //     { name: 'Bingo', state: 'IN' },
    //     { name: 'Boston', state: 'MA' },
    //     { name: 'Burlington', state: 'VT' },
    //     { name: 'Wilmington', state: 'NC' },
    //     { name: 'Atlanta', state: 'GA' },
    //     { name: 'Syracuse', state: 'NY' },
    // ];
    useEffect(() => {
        const init = async () => {
            var citiesList=await lookup.getLookupByType("Cities");
            console.log(citiesList);
            setCities(citiesList);
        }
        init();
    }, []);
    const handleSubmit = async (e) => {
        const city = cities[state];
        e.preventDefault();
        if (!city.name || !city.state) return;
        onSave({ name: city.name, state: city.state }).then(() => {
            setName('');
            setState('');
        });
    };

    return (
        <form onSubmit={handleSubmit} className="w-full">
            <div className="grid grid-cols-1 sm:grid-cols-12 col-gap-4">
                <div className="sm:col-span-8">
                    <div className="mt-1 shadow-sm">
                        <select
                            id="code"
                            className="form-select block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 rounded-none sm:rounded-sm placeholder-gray-300 placeholder-opacity-75"
                            value={state}
                            placeholder="State"
                            onChange={(e) => setState(e.target.value)}
                        >
                            <option value="">-- Select --</option>
                            {cities.map((city, index) => (
                                <option key={index} value={index}>
                                    {city.name} ({city.state})
                                </option>
                            ))}
                        </select>
                    </div>
                </div>
                <div className="mt-2 sm:col-span-4 text-right">
                    <button
                        type="submit"
                        className="py-1 px-2 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-200 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                    >
                        Assign
                    </button>

                    <button
                        type="button"
                        onClick={() => onNew()}
                        className="ml-1 py-1 px-2 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-200 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                    >
                        New
                    </button>
                </div>
            </div>
        </form>
    );
};
export default CityAssignForm;
