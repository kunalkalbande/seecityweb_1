import React, { useState, useEffect } from 'react';
import CityAssignForm from './CityAssignForm';
import CityEditForm from './CityEditForm';
import CityListItem from './CityListItem';
import { createCity, updateCity ,updateMarket,updateLookup} from '../../../api/graphql/mutations';
import { citiesByMarketId } from '../../../api/graphql/queries';
import { apolloClient } from '../../../api/graphql/apolloClient';
import { lookup } from '../../../api/graphql/lookup';

const fetchCitiesApi = (_id) => {
    return new Promise((resolve) => {
        setTimeout(async () => {
            var data = await apolloClient.performQuery(citiesByMarketId, { marketId: _id });
            resolve(data.marketById.cities);
        }, 1000);
    });
};

const createCityApi = (city) => {
    return new Promise((resolve) => {
        setTimeout(async () => {
            var data = await apolloClient.performMutation(createCity, { city: city });
            resolve(data.createCity);
        }, 1000);
    });
};

const updateCityApi = (city) => {
    return new Promise((resolve) => {
        setTimeout(async () => {
    
            var data = await apolloClient.performMutation(updateCity, { city: city });
            resolve(data.createCity);
        }, 1000);
    });
};

const CityList = ({ market, onSave }) => {
    const [action, setAction] = useState('Assign');
    const [states, setStates] = useState([]);
    const [cities, setCities] = useState([]);
    const [citiesUpToDate, setCitiesUpToDate] = useState(true);

    useEffect(() => {
        const init = async () => {
            const data = await lookup.getLookupByType("States");
            setStates(data);
            var citiesList=await lookup.getCities();
            setCities(citiesList);
        
        }
        init();
    }, []);

    const save = async (city) => {
        console.log("cityList save");
        console.log("city->", city);
       // city.marketId = market._id;
        market.cities.push(city);
        console.log(market);
        var data = await apolloClient.performMutation(updateMarket, { market });
        //const result = await createCityApi(city);
        //setAction('Assign');
        await getLatestCities();
    }

    const addCity= async (city) => {
        console.log("addCity");
        console.log(cities);

        console.log("city->", city);
       // city.marketId = market._id;
       cities.values.push(city);
        market.cities.push(city);
        console.log(market);
        var citydata=await apolloClient.performMutation(updateLookup, { lookup:cities });
        console.log(citydata);
        var data = await apolloClient.performMutation(updateMarket, { market });
        //const result = await createCityApi(city);
        setAction('Assign');
        await getLatestCities();
    }
    const update = async (city) => {
        console.log(city);
        var existingCity=market.cities.find(x=>x._id===city._id);
        existingCity.name=city.name;
        existingCity.state=city.state;
        var data = await apolloClient.performMutation(updateMarket, { market });
        //const result = await updateCityApi(city);
        await getLatestCities();
    }

    const getLatestCities = async () => {
        const result = await fetchCitiesApi(market._id);
        setCitiesUpToDate(false);
        market.cities = result;
        setCitiesUpToDate(true);
        onSave();
    }

    //const save = () => console.log('City Saved');
    const cancel = () => setAction('Assign');

    if (!citiesUpToDate) {
        return (
            <div></div>
        );
    }
    return (
        <div className="flex flex-col bg-white shadow overflow-hidden sm:rounded-b-md">
            <div className="h-0 flex-1 flex flex-col">
                <div className="px-2 py-3 relative w-full text-sc-blue-300 ">
                    <div className="flex items-center">
                        {market.name} ({market.code})
                    </div>
                </div>
                <hr />
                <ul className="pt-2 pb-2 overflow-auto">
                    {market.cities.map((city, index) => (
                        <CityListItem key={index} index={index} city={city} states={states} onSave={update} />
                    ))}
                </ul>
            </div>
            <hr />
            <div className="flex flex-shrink-0 gorup block bg-white p-2">
                {action === 'Assign' ? (
                    <CityAssignForm
                        onSave={save}
                        onNew={() => setAction('New')}
                    />
                ) : (
                        <CityEditForm
                            city={{ name: '', state: '' }}
                            states={states}
                            onSave={addCity}
                            onCancel={() => setAction('Assign')}
                        />
                    )}
            </div>
        </div>
    );
};
export default CityList;
