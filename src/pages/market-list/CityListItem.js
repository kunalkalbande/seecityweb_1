import React, { useState } from 'react';
import CityEditForm from './components/CityEditForm';

const CityListItem = ({ city }) => {
    const [isEditMode, setIsEditMode] = useState(false);

    const save = (city) => {
        console.log(city);
    };

    return !isEditMode ? (
        <a
            href="#"
            className="block hover:bg-sc-blue-100 focus:outline-none focus:bg-sc-blue-100 transition duration-150 ease-in-out"
        >
            <div className="flex items-center px-2 py-1 sm:px-3">
                <div className="min-w-0 flex-1 flex items-center">
                    <div className="text-sm text-sc-blue-300 truncate">
                        {city.name}, {city.state}
                    </div>
                </div>
                <div>
                    <button
                        className="text-xs text-sc-blue-200 underline"
                        onClick={() => setIsEditMode(true)}
                    >
                        Edit
                    </button>
                </div>
            </div>
        </a>
    ) : (
        <div className="p-2">
            <CityEditForm
                city={city}
                onSave={save}
                onCancel={() => setIsEditMode(false)}
                mode="Edit"
            />
        </div>
    );
};
export default CityListItem;
