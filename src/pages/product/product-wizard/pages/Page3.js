/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import image1 from '../../../../assets/img/sales-channels/MySalesChannels.jpg';
import image2 from '../../../../assets/img/sales-channels/LocalPartners.jpg';
import image3 from '../../../../assets/img/sales-channels/LocalAndNationalSearch.jpg';
import { CheckBox, TextBox } from 'devextreme-react';

const Page3 = (props) => {
    const salesChannels = [
        {
            id: 1,
            name: 'MY SALES CHANNELS',
            img: image1,
            description: `You can sell your products and others' products through any screen you control.`,
        },
        {
            id: 2,
            name: 'LOCAL PARTNERS',
            img: image2,
            description: `You allow local attractions, tour operators and agencies to sell your product.`,
        },
        {
            id: 3,
            name: 'LOCAL & NATIONAL SEARCH',
            img: image3,
            description: `You automatically get local and national sales via See.City.`,
        },
    ];

    const [isMySalesChannelSelected, setIsMySalesChannelSelected] = useState(
        false
    );
    const [isLocalPartnersSelected, setIsLocalPartnersSelected] = useState(
        false
    );
    const [
        isLocalAndNationalSearchSelected,
        setIsLocalAndNationalSearchSelected,
    ] = useState(false);
    const [revealCode, setRevealCode] = useState('');
    const [isPublicMode, setIsPublicMode] = useState(false);
    const [isPrivateMode, setIsPrivateMode] = useState(false);

    useEffect(() => {
        props.product.revealCode = revealCode;
        props.product.isModePublic = isPublicMode;
        props.product.isModePrivate = isPrivateMode;
        props.product.isMySalesChannel = isMySalesChannelSelected;
        props.product.isLocalPartners = isLocalPartnersSelected;
        props.product.isLocalAndNationalSearch = isLocalAndNationalSearchSelected;
    }, [
        revealCode,
        isPublicMode,
        isPrivateMode,
        isMySalesChannelSelected,
        isLocalPartnersSelected,
        isLocalAndNationalSearchSelected,
    ]);

    useEffect(() => {
        if (props.product.salesAccess != undefined) {
            var salesAccess = props.product.salesAccess;
            setRevealCode(salesAccess.revealCode);
            setIsPublicMode(salesAccess.isModePublic);
            setIsPrivateMode(salesAccess.isModePrivate);
            setIsPublicMode(salesAccess.isModePublic);
            setIsMySalesChannelSelected(salesAccess.isMySalesChannel);
            setIsLocalPartnersSelected(salesAccess.isLocalPartners);
            setIsLocalAndNationalSearchSelected(
                salesAccess.isLocalAndNationalSearch
            );
        }
    }, [props.product]);

    return (
        <div className="overflow-x-hidden">
            <div className="max-h-screen">
                <div className="mx-2 sm:mx-5 flex flex-col bg-white">
                    <div className="flex flex-col items-left ">
                        <div className="mt-4 sm:pt-1">
                            <label
                                htmlFor="priceByPeriod"
                                className="pb-1 block text-sm font-medium leading-5 text-gray-400"
                            >
                                Select the channels through which you plan to sell your
                                tickets:
                            </label>
                        </div>
                    </div>
                    <div className="grid grid-cols-1 col-gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3">
                        {salesChannels.map((channel, index) => (
                            <div key={index}>
                                <div className="py-2 flex flex-col items-center">
                                    <CheckBox
                                        className="pb-3"
                                        text=""
                                        value={
                                            channel.id === 1
                                                ? isMySalesChannelSelected
                                                : channel.id === 2
                                                    ? isLocalPartnersSelected
                                                    : isLocalAndNationalSearchSelected
                                        }
                                        onValueChanged={(e) => {
                                            switch (channel.id) {
                                                case 1:
                                                    setIsMySalesChannelSelected(
                                                        e.value
                                                    );
                                                    break;
                                                case 2:
                                                    setIsLocalPartnersSelected(e.value);
                                                    break;
                                                case 3:
                                                    setIsLocalAndNationalSearchSelected(
                                                        e.value
                                                    );
                                                    break;
                                            }
                                        }}
                                    />
                                    <div className="w-full border">
                                        <img
                                            className="h-full w-full"
                                            src={channel.img}
                                            alt="see.city"
                                        />
                                        <h1 className="w-full mx-auto px-2 mt-2 h-8 text-center text-lg font-medium text-gray-400">
                                            {channel.name}
                                        </h1>
                                        <h6 className="w-full mx-auto px-2 mt-2 h-12 md:h-16 lg:h-12 text-center text-xs text-gray-400">
                                            {channel.description}
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>

                    <div className="mt-6" />
                    <div className="pb-5 grid grid-cols-1 col-gap-3 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3">
                        <div className="pt-2 grid grid-cols-1 col-gap-3">
                            <div className="py-2 flex flex-col items-left">
                                <span className="pt-3 text-lg leading-6 font-medium text-sc-blue-400 pb-1">
                                    Availability Mode:
                                </span>
                                <div className="flex items-left mt-4">
                                    <CheckBox
                                        text=""
                                        value={isPublicMode}
                                        onValueChanged={(e) => setIsPublicMode(e.value)}
                                    />
                                    <label className="flex justify-start items-start ml-2">
                                        <span className="text-sm font-medium leading-5 text-gray-400">
                                            Public
                                        </span>
                                        <span className="ml-2 text-xs text-gray-400">
                                            (Reveal Code Optional)
                                        </span>
                                    </label>
                                </div>

                                <div className="flex items-left mt-4">
                                    <CheckBox
                                        text=""
                                        value={isPrivateMode}
                                        onValueChanged={(e) =>
                                            setIsPrivateMode(e.value)
                                        }
                                    />
                                    <label className="flex justify-start items-start ml-2">
                                        <span className="text-sm font-medium leading-5 text-gray-400">
                                            Private
                                        </span>
                                        <span className="ml-2 text-xs text-gray-400">
                                            (Reveal Code Required)
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div className="pt-2 grid grid-cols-1 col-gap-3">
                            <div className="py-2 flex flex-col items-left">
                                <span className="pt-3 text-lg leading-6 font-medium text-sc-blue-400 pb-1">
                                    Special Reveal Code:
                                </span>
                                <div className="py-2 mt-2">
                                    <TextBox
                                        id="revealCode"
                                        type="text"
                                        name="revealCode"
                                        placeholder="VIPTICKET"
                                        showClearButton={true}
                                        value={revealCode}
                                        onValueChanged={(e) => setRevealCode(e.value)}
                                    />
                                </div>
                                <span className="pt-3 text-xs text-gray-400">
                                    This code will take Guest directly to the product.
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Page3;
