/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import images from '../../components/images';

const Page1 = (props) => {
    const history = useHistory();
    const [selected, setSelected] = useState(null);
    const categories = [
        {
            id: '1',
            name: 'Attraction Ticket',
            img: images.productCategories.ATTRACTION_TICKET,
            enabled: true,
        },
        {
            id: '2',
            name: 'Event Ticket',
            img: images.productCategories.EVENT_TICKET,
        },
        {
            id: '3',
            name: 'Parking',
            img: images.productCategories.PARKING,
            enabled: true,
        },
        {
            id: '4',
            name: 'Food & Beverage',
            img: images.productCategories.FOOD_AND_BEVERAGE,
            enabled: true,
        },
        {
            id: '5',
            name: 'Value Bundle',
            img: images.productCategories.VALUE_BUNDLE,
            enabled: true,
        },
        {
            id: '6',
            name: 'Wearable Merchandise',
            img: images.productCategories.WEARABLE_MERCHANDISE,
            enabled: true,
        },
        {
            id: '7',
            name: 'Souvenir',
            img: images.productCategories.SOUVENIR,
            enabled: true,
        },
    ];

    const onSelectCategory = (cat) => {
        setSelected(cat);
    };
    useEffect(() => {
        if (selected != undefined) {
            // props.product.categoryId = selected.id;
            localStorage.setItem('categoryId', selected.id);
        }
    }, [selected]);
    const disabledClassName =
        'w-24 mx-2 px-4 py-2 border border-sc-blue-200 text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm';

    const enabledClassName =
        'w-24 mx-2 px-4 py-2 border border-sc-blue-200 text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-200 active:bg-sc-blue-200 transition duration-150 ease-in-out';

    return (
        <div className="overflow-x-hidden">
            <div className="max-h-screen">
                <div className="px-2 pt-4 sm:px-3 h-full">
                    <div className="flex flex-col items-center ">
                        <span className="text-3xl font-bold text-sc-blue-500">
                            Let's Create Your Product!
                </span>
                        <span className="pt-3 text-lg font-bold text-sc-blue-400">
                            Select Product Category
                </span>
                    </div>
                    <div className="pt-2 grid grid-cols-1 col-gap-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
                        {categories.map((cat, index) => (
                            <button
                                className={
                                    selected && selected.name === cat.name
                                        ? 'bg-sc-purple'
                                        : ''
                                }
                                onClick={() => onSelectCategory(cat)}
                                key={index}
                            >
                                <div className="py-2 flex flex-col items-center">
                                    <img
                                        className="h-full w-auto"
                                        src={cat.img}
                                        alt="see.city"
                                    />
                                    <h1
                                        className={
                                            selected && selected.name === cat.name
                                                ? 'pt-2 text-2xl font-bold text-white'
                                                : 'pt-2 text-2xl font-bold text-sc-blue-400'
                                        }
                                    >
                                        {cat.name}
                                    </h1>
                                </div>
                            </button>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Page1;
