/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { CheckBox, NumberBox, TagBox, Validator } from 'devextreme-react';
import { arrayUtils } from '../../../../utils';
import { BasePricing } from '../../components';
import { lookup } from '../../../../api/graphql/lookup';
import { RequiredRule,RangeRule } from 'devextreme-react/data-grid';

const product_tags = [
    'General Admission',
    'Adult',
    'Child',
    'Senior',
    'Military',
];

const Page4 = (props) => {
    const [basePricing, setBasePricing] = useState(props.product.basePricing);
    const [priceByDay, setPriceByDay] = useState(false);
    const [priceByDayPart, setPriceByDayPart] = useState(true);
    const [priceByPeriod, setPriceByPeriod] = useState(true);

    const [directCommission, setDirectCommission] = useState(0.1);
    const [partnerCommission, setPartnerCommission] = useState(0.11);
    const [productTags, setProductTags] = useState([]);
    const [basePricingArray, setBasePricingArray] = useState([]);
    const [basePricingItems, setBasePricingItems] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            //const data = await lookup.getLookupByType("Product Tags");
            //setProductTags(data);
            setProductTags(product_tags);
        };
        fetchData();
    }, []);

    useEffect(() => {
        props.product.basePricing = basePricing;
    }, [basePricing]);

    useEffect(() => {
        props.product.priceByDay = priceByDay;
    }, [priceByDay]);

    useEffect(() => {
        props.product.directCommission = directCommission;
        props.product.partnerCommission = partnerCommission;
        props.product.priceByPeriod = priceByPeriod;
        props.product.priceByDayPart = priceByDayPart;
    }, [directCommission, partnerCommission]);

    useEffect(() => {
        if (
            props.product.directCommission != undefined &&
            props.product.basePricing
        )
            setBasePricing(props.product.basePricing);
        setDirectCommission(props.product.directCommission);
        setPartnerCommission(props.product.partnerCommission);
        setPriceByDay(props.product.priceByDay);
    }, [props.product]);

    const onProductTagsChanged = (args) => {
        let array = [...basePricing];
        if (args.previousValue.length < args.value.length) {
            let tags = arrayUtils.except(args.value, args.previousValue);
            tags.forEach((tag) => {
                if (tag !== '') {
                    array.push({ productType: tag, retail: 0, wholesale: 0 });
                }
            });
        } else {
            let tag = arrayUtils.except(args.previousValue, args.value)[0];
            let idx = [...basePricing].map((e) => e.name).indexOf(tag);
            array.splice(idx, 1);
        }
        setBasePricing(array);
    };

    return (
        <div className="overflow-x-hidden">
            <div className="max-h-screen">
                <div className="mx-2 sm:mx-5 flex flex-col bg-white">
                    <div className="mt-4 sm:pt-1">
                        <label
                            htmlFor="priceByPeriod"
                            className="pb-1 block text-sm font-medium leading-5 text-gray-400"
                        >
                            Select all Product Types that apply
                        </label>
                        <TagBox
                            items={productTags}
                            value={basePricing.map((m) => m.productType)}
                            onValueChanged={onProductTagsChanged}
                            applyValueMode="useButtons"
                            showSelectionControls={true}
                            hideSelectedItems={true}
                            showDropDownButton={true}
                            placeholder="Select..."
                        >
                            <Validator>
                                <RequiredRule message=" required" />
                            </Validator>
                        </TagBox>
                    </div>
                    <div className="mt-4 sm:pt-1 sm:border-b sm:border-gray-200">
                        <h3 className="text-lg leading-6 font-medium text-sc-blue-400 pb-1">
                            Base Pricing
                        </h3>
                    </div>
                    <div className="mt-1 flex flex-1 flex-col">
                        <div className="invisible h-0 sm:h-2 sm:visible sm:mt-4 grid grid-cols-3 sm:grid-cols-6 col-gap-4">
                            <div className="text-gray-400 text-xs">
                                Product Type
                            </div>
                            <div className="text-gray-400 text-xs">Retail</div>
                            <div className="text-gray-400 text-xs">
                                Wholesale
                            </div>
                        </div>
                        <div className="mt-2 sm:mt-3">
                            {basePricing.map((item, index) => (
                                <BasePricing key={index} item={item} />
                            ))}
                        </div>
                    </div>
                    <div className="mt-4 sm:pt-1 sm:border-b sm:border-gray-200">
                        <h3 className="text-lg leading-6 font-medium text-sc-blue-400 pb-1">
                            Dynamic Pricing
                        </h3>
                    </div>
                    <div className="mt-4 sm:pt-1 flex flex-1 flex-col">
                        <div className="mt-2 flex flex-0 items-center sm:items-start">
                            <CheckBox
                                id="priceByPeriod"
                                value={props.product.priceByPeriod}
                                disabled={true}
                            />
                            <label
                                htmlFor="priceByPeriod"
                                className="ml-2 block text-sm font-medium leading-5 text-gray-400"
                            >
                                By Operating Period
                            </label>
                        </div>
                        <div className="mt-2 flex flex-0 items-center sm:items-start">
                            <CheckBox
                                id="priceByDayPart"
                                value={props.product.priceByDayPart}
                                disabled={true}
                            />
                            <label
                                htmlFor="priceByPeriodPart"
                                className="ml-2 block text-sm font-medium leading-5 text-gray-400"
                            >
                                By Day Part
                            </label>
                        </div>
                        <div className="mt-2 flex flex-0 items-center sm:items-start">
                            <CheckBox
                                id="priceByDay"
                                value={priceByDay}
                                onValueChanged={(e) => setPriceByDay(e.value)}
                            />
                            <label
                                htmlFor="priceByPeriod"
                                className="ml-2 block text-sm font-medium leading-5 text-gray-400"
                            >
                                By Day
                            </label>
                        </div>
                    </div>
                    <div className="mt-4 sm:pt-1 sm:border-b sm:border-gray-200">
                        <h3 className="text-lg leading-6 font-medium text-sc-blue-400 pb-1">
                            Seller's Commission
                        </h3>
                    </div>
                    <div className="mt-2 sm:pt-1 ">
                        <p className="text-xs text-gray-400">
                            What is the percent (%) you will offer other Sellers
                            of this product when the product is sold directly to
                            a Guest or bundled wholesale via Partners? (10-20%
                            is recommended)
                        </p>
                    </div>
                    <div className="mt-4 grid grid-cols-1 row-gap-6 sm:grid-cols-4 sm:col-gap-8">
                        <div className="sm:col-span-1">
                            <label
                                htmlFor="directCommission"
                                className="block text-sm font-medium leading-5 text-gray-400"
                            >
                                Retail
                            </label>
                            <div className="mt-1">
                                <NumberBox
                                    id="directCommission"
                                    value={directCommission}
                                    format="#0%"
                                    step={0.01}
                                    onValueChanged={(e) =>
                                        setDirectCommission(e.value)
                                    }
                                >
                                    <Validator>
                                        <RequiredRule message="directCommission required" />
                                        <RangeRule
                                            message="directCommission must be > $0%"
                                            min="0.01"
                                        />
                                    </Validator>
                                </NumberBox>
                            </div>
                        </div>
                        <div className="sm:col-span-1">
                            <label
                                htmlFor="partnerCommission"
                                className="block text-sm font-medium leading-5 text-gray-400"
                            >
                                Wholesale
                            </label>
                            <div className="mt-1">
                                <NumberBox
                                    id="partnerCommission"
                                    format="#0%"
                                    step={0.01}
                                    value={partnerCommission}
                                    onValueChanged={(e) =>
                                        setPartnerCommission(e.value)
                                    }
                                >
                                  <Validator>
                                        <RequiredRule message="partnerCommission required" />
                                        <RangeRule
                                            message="partnerCommission must be > $0%"
                                            min="0.01"
                                        />
                                    </Validator>
                                </NumberBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Page4;
