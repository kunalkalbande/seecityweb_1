/* eslint-disable react/prop-types */
import React from 'react';
import { TaxesAndFees } from '../../components';

const Page6 = (props) => {
    return (
        <TaxesAndFees product={props.product} />
    );
};

export default Page6;