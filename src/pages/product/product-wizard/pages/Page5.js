/* eslint-disable react/prop-types */
import React from 'react';
import { ProductPricing } from '../../components';

const Page5 = (props) => {
    return (
        <div className="overflow-x-hidden">
            <div className="max-h-screen">
                <ProductPricing product={props.product} />
            </div>
        </div>
    );
};

export default Page5;
