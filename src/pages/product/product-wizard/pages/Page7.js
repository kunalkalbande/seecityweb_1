import React, { useEffect, useState } from 'react';
import { Loading } from '../../../../components/common';
import { Button } from 'devextreme-react';
import visualStyleImg from '../../../../assets/img/visual-style-img.jpg';

const ticket = {
    basics: {
        ticketName: `Space Needle`,
        ticketDescription: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Sed dapibus magna eu blandit fringilla. Sed elit urna,
                            venenatis egestas ligula ac, rutrum blandit tellus.`,
        expirationDate: `14`,
        priceType: `Adult, Child, Senior`,
        termsAndConditions: `None`,
        advanceSale: `30`,
    },
    pricing: {
        standard: `20`,
        senior: `15`,
        child: `10`,
        standard_summer: `25`,
        senior_summer: `20`,
        child_summer: `15`,
    },
    salesChannels: `Web, Box Office`,
    taxes: `Start Sales Tax, 6.7% exclusive tax`,
    fees: `Service Fee, 5% exclusive, Web`,
    discounts: `Opening Discount, 10%, 01/01/18 - 02/01/18`,
    visualStyling: {},
};

const Page7 = (props) => {
    const [isLoading, setIsLoading] = useState(true);
    const [ticketData, setTicketData] = useState({});
    const [product, setProduct] = useState({});

    useEffect(() => {
        const init = async () => {
            console.log("here -> ", props)
            setIsLoading(true);
            setTicketData(ticket);
            setProduct(props.product);
            setIsLoading(false);
        };
        init();
    }, []);

    const edit = (step) => {
        props.onEdit(step);
    }

    if (isLoading) {
        return <Loading />;
    }
    return (
        <div className="overflow-x-hidden">
            <div className="max-h-screen">
                <div className="px-2 sm:px-3 h-full">
                    <div className="min-h-full grid grid-cols-1 sm:grid-cols-1 col-gap-4">
                        <div className="mb-2 flex flex-col overflow-y-hidden">
                            <div className="flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6 flex items-left justify-between flex-col sm:flex-no-wrap">
                                    <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                        Finalize Your Ticket
                                    </h3>
                                    <div className="w-full pt-3 text-sm text-sc-blue-400">
                                        {`You're almost done! Simply confirm the account of 
                                    your ticket information and edit as needed.`}
                                    </div>
                                </div>
                                <div className="h-full">
                                    {/* The Basics */}
                                    <div className="flex flex-col py-3 border-b border-gray-200 px-4 sm:px-6">
                                        <div className="w-full flex flex-wrap pb-3">
                                            <h3 className="w-2/5 sm:w-2/6 text-lg leading-6 font-medium text-sc-blue-400">
                                                The Basics
                                            </h3>
                                            <span className="w-3/5 sm:w-4/6 float-right text-xs">
                                                <span
                                                    className="float-right cursor-pointer text-xs text-sc-blue-200 underline"
                                                    onClick={() => edit(2)}
                                                >
                                                    {/* EDIT THIS SECTION */} Edit
                                                </span>
                                            </span>
                                        </div>
                                        <div className="w-full flex flex-wrap pb-2">
                                            <div className="w-2/5 sm:w-2/6 font-semibold text-sm text-gray-500">
                                                Ticket Name:
                                            </div>
                                            <div className="w-3/5 sm:w-4/6 text-sm text-gray-500">
                                                {product.ticketName === undefined ? product.name : product.ticketName}
                                            </div>
                                        </div>
                                        <div className="w-full flex flex-wrap pb-2">
                                            <div className="w-2/5 sm:w-2/6 font-semibold text-sm text-gray-500">
                                                Ticket Description:
                                            </div>
                                            <div className="w-3/5 sm:w-4/6 text-sm text-gray-500">
                                                {product.shortDesc === undefined ? product.shortDescription : product.shortDesc}
                                            </div>
                                        </div>
                                        <div className="w-full flex flex-wrap pb-2">
                                            <div className="w-2/5 sm:w-2/6 font-semibold text-sm text-gray-500">
                                                Expiration Date:
                                            </div>
                                            <div className="w-3/5 sm:w-4/6 text-sm text-gray-500">
                                                {(product.expirationDays === undefined ? product.expireDays : product.expirationDays) +
                                                    ' days'}
                                            </div>
                                        </div>
                                        <div className="w-full flex flex-wrap pb-2">
                                            <div className="w-2/5 sm:w-2/6 font-semibold text-sm text-gray-500">
                                                Price Type:
                                            </div>
                                            <div className="w-3/5 sm:w-4/6 text-sm text-gray-500">
                                                <div className="flex flex-row">
                                                    {
                                                        product.basePricing.map((item, index) => (
                                                            <div key={index} className="pr-2">
                                                                {item.productType + ((product.basePricing.length - 1) === index ? '' : ', ')}
                                                            </div>
                                                        ))
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-full flex flex-wrap pb-2">
                                            <div className="w-2/5 sm:w-2/6 font-semibold text-sm text-gray-500">
                                                Terms & Conditions:
                                            </div>
                                            <div className="w-3/5 sm:w-4/6 text-sm text-gray-500">
                                                {product.termsAndConditions}
                                            </div>
                                        </div>
                                        <div className="w-full flex flex-wrap pb-2">
                                            <div className="w-2/5 sm:w-2/6 font-semibold text-sm text-gray-500">
                                                Advance Sale:
                                            </div>
                                            <div className="w-3/5 sm:w-4/6 text-sm text-gray-500">
                                                {product.advanceSaleDays +
                                                    ' days'}
                                            </div>
                                        </div>
                                    </div>

                                    {/* Ticket Pricing */}
                                    <div className="flex flex-col py-3 border-b border-gray-200 px-4 sm:px-6">
                                        <div className="w-full flex flex-wrap pb-3">
                                            <h3 className="w-2/5 sm:w-2/6 text-lg leading-6 font-medium text-sc-blue-400">
                                                Ticket Pricing
                                            </h3>
                                            <span className="w-3/5 sm:w-4/6 float-right text-xs">
                                                <span
                                                    className="float-right cursor-pointer text-xs text-sc-blue-200 underline"
                                                    onClick={() => edit(4)}
                                                >
                                                    {/* EDIT THIS SECTION */} Edit
                                                </span>
                                            </span>
                                        </div>
                                        <div className="w-full flex flex-wrap pb-2">
                                            <div className="w-2/5 sm:w-2/6 font-semibold text-sm text-gray-500">
                                                {}
                                            </div>
                                            <div className="w-3/5 sm:w-4/6 text-sm text-gray-500">
                                                <div className="flex flex-row">
                                                    <div className="w-1/2">
                                                        Retail
                                                    </div>
                                                    <div className="w-1/2">
                                                        Wholesale
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        {
                                            product.basePricing.map((item, index) => (
                                                <div key={index}>
                                                    <div className="w-full flex flex-wrap pb-2">
                                                        <div className="w-2/5 sm:w-2/6 font-semibold text-sm text-gray-500">
                                                            {item.productType}
                                                        </div>
                                                        <div className="w-3/5 sm:w-4/6 text-sm text-gray-500">
                                                            <div className="flex flex-row">
                                                                <div className="w-1/2">
                                                                    {'$' + item.retail}
                                                                </div>
                                                                <div className="w-1/2">
                                                                    {'$' + item.wholesale}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            ))
                                        }
                                    </div>

                                    {/* Sales Channels */}
                                    <div className="flex flex-col py-3 border-b border-gray-200 px-4 sm:px-6">
                                        <div className="w-full flex flex-wrap pb-3">
                                            <h3 className="w-2/5 sm:w-2/6 text-lg leading-6 font-medium text-sc-blue-400">
                                                Sales Channels
                                            </h3>
                                            <span className="w-3/5 sm:w-4/6 float-right text-xs">
                                                <span
                                                    className="float-right cursor-pointer text-xs text-sc-blue-200 underline"
                                                    onClick={() => edit(3)}
                                                >
                                                    {/* EDIT THIS SECTION */} Edit
                                                </span>
                                            </span>
                                        </div>
                                        <div className="w-full flex flex-wrap pb-2">
                                            <div className="w-2/5 sm:w-2/6 font-semibold text-sm text-gray-500">
                                                Sales Channels:
                                            </div>
                                            <div className="w-3/5 sm:w-4/6 text-sm text-gray-500">
                                                {product.isMySalesChannel === undefined ? (product.salesAccess.isMySalesChannel ? 'MY SALES CHANNELS, ' : '') : (product.isMySalesChannel ? 'MY SALES CHANNELS, ' : '')}
                                                {product.isLocalPartners === undefined ? (product.salesAccess.isLocalPartners ? 'LOCAL PARTNERS, ' : '') : (product.isLocalPartners ? 'LOCAL PARTNERS, ' : '')}
                                                {product.isLocalAndNationalSearch === undefined ? (product.salesAccess.isLocalAndNationalSearch ? 'LOCAL & NATIONAL SEARCH, ' : '') : (product.isLocalAndNationalSearch ? 'LOCAL & NATIONAL SEARCH, ' : '')}
                                            </div>
                                        </div>
                                    </div>

                                    {/* Tax, Fees & Discounts */}
                                    <div className="flex flex-col py-3 border-b border-gray-200 px-4 sm:px-6">
                                        <div className="w-full flex flex-wrap pb-3">
                                            <h3 className="w-2/5 sm:w-2/6 text-lg leading-6 font-medium text-sc-blue-400">
                                                Tax, Fees & Discounts
                                            </h3>
                                            <span className="w-3/5 sm:w-4/6 float-right text-xs">
                                                <span
                                                    className="float-right cursor-pointer text-xs text-sc-blue-200 underline"
                                                    onClick={() => edit(6)}
                                                >
                                                    {/* EDIT THIS SECTION */} Edit
                                                </span>
                                            </span>
                                        </div>
                                        <div className="w-full flex flex-wrap pb-2">
                                            <div className="w-2/5 sm:w-2/6 font-semibold text-sm text-gray-500">
                                                Taxes:
                                            </div>
                                            <div className="w-3/5 sm:w-4/6 text-sm text-gray-500">
                                                <div className="flex flex-col">
                                                    {
                                                        product.taxes !== undefined && product.taxes.length > 0 ? (
                                                            product.taxes.map((item, index) => (
                                                                <div key={index}>
                                                                    {item.taxName + ', $' + item.taxAmount + ', ' + item.appliedType}
                                                                </div>
                                                            ))) : 'NA'
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-full flex flex-wrap pb-2">
                                            <div className="w-2/5 sm:w-2/6 font-semibold text-sm text-gray-500">
                                                Fees:
                                            </div>
                                            <div className="w-3/5 sm:w-4/6 text-sm text-gray-500">
                                                <div className="flex flex-col">
                                                    {
                                                        product.fees !== undefined && product.fees.length > 0 ? (
                                                            product.fees.map((item, index) => (
                                                                <div key={index}>
                                                                    {item.feeName + ', $' + item.feeAmount + ', ' + item.appliedType}
                                                                </div>
                                                            ))) : 'NA'
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-full flex flex-wrap pb-2 hidden">
                                            <div className="w-2/5 sm:w-2/6 font-semibold text-sm text-gray-500">
                                                Discounts:
                                            </div>
                                            <div className="w-3/5 sm:w-4/6 text-sm text-gray-500">
                                                {ticketData.discounts}
                                            </div>
                                        </div>
                                    </div>

                                    {/* Visual Styling */}
                                    <div className="flex flex-col py-3 border-b border-gray-200 px-4 sm:px-6">
                                        <div className="w-full flex flex-wrap pb-3">
                                            <h3 className="w-2/5 sm:w-2/6 text-lg leading-6 font-medium text-sc-blue-400">
                                                Visual Styling
                                            </h3>
                                            <span className="w-3/5 sm:w-4/6 float-right text-xs">
                                                <span
                                                    className="float-right cursor-pointer text-xs text-sc-blue-200 underline"
                                                    onClick={() => alert('Edit Mode')}
                                                >
                                                    {/* EDIT THIS SECTION */} Edit
                                                </span>
                                            </span>
                                        </div>
                                        <div className="w-full flex flex-wrap pb-2">
                                            <div>
                                                <img src={visualStyleImg} />
                                            </div>
                                        </div>
                                    </div>

                                    {/* Visual Styling */}
                                    {/* <div className="flex flex-col py-3 border-b border-gray-200 px-4 sm:px-6">
                                <div className="w-full flex flex-wrap pb-3">
                                    <div className="w-full p-2">
                                        <Button
                                            className="float-right border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                                            text="FINISH UP →"
                                            //type="default"
                                            stylingMode="contained"
                                            //stylingMode="outlined"
                                            onClick={() => alert('FINISH UP →')}
                                        />
                                    </div>
                                </div>
                            </div> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Page7;
