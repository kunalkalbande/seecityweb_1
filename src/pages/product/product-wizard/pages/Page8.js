import React, { useState } from 'react';
import productBuilt from '../../../../assets/img/Product_Built_Sucess.jpg';
import logo from '../../../../assets/img/logos/logo-primary-white.png';
import { Button } from 'devextreme-react';
import { useHistory } from 'react-router-dom';

const Page8 = (props) => {
    const history = useHistory();
    return (
        <div className="overflow-x-hidden">
            <div className="max-h-screen">
                <div className="min-h-screen sm:h-auto lg:h-screen w-full">
                    <div className="flex flex-col items-center flex-1 h-full justify-center px-4 py-4 sm:px-0">
                        <div className="w-full auth-w-900 sm:w-4/5 lg:w-8/12 xl:w-7/12 sm:mx-0">
                            <img className="mb-5 w-40" src={logo} alt="see-city-img" />
                            <div className="auth-form flex rounded-lg shadow-lg w-full bg-indigo-100 sm:mx-0">
                                <div className="sm:w-12/12 md:w-8/12 lg:w-8/12 rounded overflow-hidden m-auto">
                                    <img
                                        className="max-w-sm w-3/4 mx-auto mt-5"
                                        src={productBuilt}
                                        alt="Product Built Success"
                                    />
                                    <div className="px-6 py-4">
                                        <div className="flex flex-col w-full p-0">
                                            <div className="flex flex-col flex-1 justify-center mb-4">
                                                <h1 className="text-2xl text-center font-bold">
                                                    Congratulations!
                                        </h1>
                                                <h9 className="mt-3 font-semibold text-sm text-gray-500 text-center">
                                                    You have successfully built your
                                                    product!
                                        </h9>
                                                <div className="w-full mt-0">
                                                    <div
                                                        id="error-message"
                                                        className="alert alert-danger text-red-700"
                                                    ></div>
                                                    <form className="form-horizontal w-full mx-auto">
                                                        <div className="flex flex-col sm:flex-row sm:flex-wrap mt-4">
                                                            <div className="w-full sm:w-1/2">
                                                                <div className="w-full p-1">
                                                                    <Button
                                                                        className="w-full border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                                                                        text="GO TO DASHBOARD →"
                                                                        //type="default"
                                                                        stylingMode="contained"
                                                                        //stylingMode="outlined"
                                                                        onClick={() =>
                                                                            history.push(
                                                                                '/member/dashboard'
                                                                            )
                                                                        }
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div className="w-full sm:w-1/2">
                                                                <div className="w-full p-1">
                                                                    <Button
                                                                        className="w-full border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                                                                        text="CREATE ANOTHER PRODUCT →"
                                                                        //type="default"
                                                                        stylingMode="contained"
                                                                        //stylingMode="outlined"
                                                                        onClick={() =>
                                                                            //window.location.reload(false)
                                                                            history.push(
                                                                                '/callback'
                                                                            )
                                                                        }
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Page8;
