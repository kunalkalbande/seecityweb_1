/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { Validator, ValidationSummary } from 'devextreme-react';
import { RequiredRule } from 'devextreme-react/data-grid';

import {
    CheckBox,
    FileUploader,
    NumberBox,
    TextArea,
    TextBox,
} from 'devextreme-react';

const Page2 = (props) => {
    const [ticketName, setTicketName] = useState('');
    const [shortDesc, setShortDesc] = useState('');
    const [longDesc, setLongDesc] = useState('');
    const [termsAndConditions, setTermsAndConditions] = useState('');
    const [isActive, setIsActive] = useState(false);
    const [expirationDays, setExpirationDays] = useState(0);
    const [advanceSaleDays, setAdvanceSaleDays] = useState(0);
    const [productImages, setProductImages] = useState([]);
    const [imgPath, setImgPath] = useState(
        'http://www.myiconfinder.com/uploads/iconsets/256-256-37736f35fd7d98b10b35287679b223b8.png'
    );
    const [isImageVisible, setIsImageVisible] = useState(false);
    useEffect(() => {
        props.product.ticketName = ticketName;
        props.product.shortDesc = shortDesc;
        props.product.longDesc = longDesc;
        props.product.termsAndConditions = termsAndConditions;
        props.product.expirationDays = expirationDays;
        props.product.advanceSaleDays = advanceSaleDays;
        props.product.isActive = isActive;
        props.product.productImages = productImages;
    }, [
        ticketName,
        shortDesc,
        longDesc,
        termsAndConditions,
        expirationDays,
        advanceSaleDays,
        isActive,
        productImages,
    ]);

    useEffect(() => {
        if (props.product.name != undefined && props.product.name != null) {
            setTicketName(props.product.name);
            console.log('Page 2' + props.product.name);
            setShortDesc(props.product.shortDescription);
            setLongDesc(props.product.longDescription);
            setTermsAndConditions(props.product.termsAndConditions);
            setExpirationDays(props.product.expireDays);
            setAdvanceSaleDays(props.product.advanceSaleDays);
            setIsActive(props.product.isActive);
            if (props.product.file != undefined && props.product.file.length>0) {
                setIsImageVisible(true);
                setImgPath('/' + props.product.file[0].path);
            }
        }
    }, [props.product]);

    const onImageUpload = async (args) => {
        console.log(args.value);
        setIsImageVisible(true);
        setProductImages(args.value);
        setImgPath(URL.createObjectURL(args.value[0]));
    };

    return (
        <div className="overflow-x-hidden">
            <div className="max-h-screen">
                <div className="mt-4 mx-2 sm:mx-5 flex flex-col bg-white">
                    <div className="grid grid-cols-1 sm:grid-cols-2 col-gap-6">
                        <div className="col-span-1 flex flex-col">
                            <div className="pb-4">
                                <label
                                    htmlFor="ticket_name"
                                    className="pb-1 block text-sm font-medium leading-5 text-gray-400"
                                >
                                    Ticket Name (shown on ticket)
                        </label>
                                <div className=" mt-1">
                                    <TextBox
                                        id="ticket_name"
                                        showClearButton={true}
                                        value={ticketName}
                                        onValueChanged={(e) => setTicketName(e.value)}
                                    >
                                        <Validator>
                                            <RequiredRule message="Ticket Name is required" />
                                        </Validator>
                                    </TextBox>
                                </div>
                            </div>
                            <div className="pb-4">
                                <label
                                    htmlFor="short_description"
                                    className="pb-1 block text-sm font-medium leading-5 text-gray-400"
                                >
                                    Short Description
                        </label>
                                <div className=" mt-1">
                                    <TextArea
                                        id="about"
                                        showClearButton={true}
                                        value={shortDesc}
                                        onValueChanged={(e) => setShortDesc(e.value)}
                                        height={80}
                                    >
                                        <Validator>
                                            <RequiredRule message="Short Description is required" />
                                        </Validator>
                                    </TextArea>
                                </div>
                            </div>
                            <div className="pb-4">
                                <label
                                    htmlFor="about"
                                    className="block text-sm font-medium leading-5 text-gray-400"
                                >
                                    Long Description
                        </label>
                                <div className=" mt-1">
                                    <TextArea
                                        id="about"
                                        height={80}
                                        value={longDesc}
                                        showClearButton={true}
                                        onValueChanged={(e) => setLongDesc(e.value)}
                                    >
                                        <Validator>
                                            <RequiredRule message="Ticket Long Description is required" />
                                        </Validator>
                                    </TextArea>
                                </div>
                            </div>
                            <div className="pb-4">
                                <label
                                    htmlFor="about"
                                    className="block text-sm font-medium leading-5 text-gray-400"
                                >
                                    Terms & Conditions
                        </label>
                                <div className="mt-1">
                                    <TextArea
                                        id="about"
                                        height={80}
                                        value={termsAndConditions}
                                        showClearButton={true}
                                        onValueChanged={(e) =>
                                            setTermsAndConditions(e.value)
                                        }
                                    >
                                        <Validator>
                                            <RequiredRule message="Ticket Terms & Conditions is required" />
                                        </Validator>
                                    </TextArea>
                                </div>
                            </div>
                        </div>
                        <div className="col-span-1 flex-flex-col">
                            <label
                                htmlFor="about"
                                className="block text-sm font-medium leading-5 text-gray-400"
                            >
                                Product Image
                    </label>
                            <div
                                style={{ minHeight: '280px' }}
                                className="w-full h-px-200 border-dashed border-2 mt-2 rounded "
                            >
                                <FileUploader
                                    id="FileUpload"
                                    selectButtonText="Browse to upload (200 x 300)"
                                    labelText=""
                                    accept="image/*"
                                    uploadMode="useForm"
                                    onValueChanged={onImageUpload}
                                    multiple={false}
                                    disabled={false}
                                    style={{
                                        height: '290px',
                                        maxHeight: '290px',
                                        index: 10,
                                        position: 'absolute',
                                    }}
                                ></FileUploader>
                                {isImageVisible ? (
                                    <div
                                        id="divimg1"
                                        className="px-2 flex justify-center"
                                    >
                                        <img
                                            id="img1"
                                            src={imgPath}
                                            style={{
                                                margin: '10px',
                                                width: '70%',
                                                maxHeight: '200px',
                                            }}
                                        />
                                    </div>
                                ) : (
                                        <div></div>
                                    )}
                            </div>
                        </div>
                    </div>
                    <div className="mt-4  sm:pt-1 sm:border-b sm:border-gray-200">
                        <h3 className="text-lg leading-6 font-medium text-sc-blue-400 pb-1">
                            Availability Status
                </h3>
                    </div>
                    <div className="mt-4 mb-1 grid grid-cols-1 sm:grid-cols-6 sm:col-gap-8">
                        <div className="sm:col-span-1 flex flex-row">
                            <CheckBox
                                id="active"
                                value={isActive}
                                onValueChanged={(e) => setIsActive(e.value)}
                            />
                            <label
                                htmlFor="active"
                                className="ml-2 block text-sm font-medium leading-5 text-gray-400"
                            >
                                Active
                    </label>
                        </div>
                        <div className="sm:col-span-1">
                            <label
                                htmlFor="active"
                                className="block text-sm font-medium leading-5 text-gray-400"
                            >
                                Expiration Date
                    </label>
                            <div className="mt-1">
                                <NumberBox
                                    showSpinButtons={true}
                                    showClearButton={true}
                                    value={expirationDays}
                                    onValueChanged={(e) => setExpirationDays(e.value)}
                                >
                                    <Validator>
                                        <RequiredRule message="Expiration Date is required" />
                                    </Validator>
                                </NumberBox>
                            </div>
                        </div>
                        <div className="sm:col-span-1">
                            <label
                                htmlFor="active"
                                className="block text-sm font-medium leading-5 text-gray-400"
                            >
                                Advance Sale
                    </label>
                            <div className="mt-1">
                                <NumberBox
                                    showSpinButtons={true}
                                    value={advanceSaleDays}
                                    onValueChanged={(e) => setAdvanceSaleDays(e.value)}
                                    showClearButton={true}
                                >
                                    <Validator>
                                        <RequiredRule message="Advance Sale is required" />
                                    </Validator>
                                </NumberBox>
                            </div>
                        </div>
                    </div>
                    <div className="sm:col-span-2">
                        <ValidationSummary id="summary"></ValidationSummary>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Page2;
