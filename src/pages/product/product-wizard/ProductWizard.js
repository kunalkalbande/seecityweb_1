/* eslint-disable react/prop-types */
import React, { Fragment, useState, useEffect } from 'react';
import StepWizard from 'react-step-wizard';
import { classNames } from '../../../utils';
import { Page1, Page2, Page3, Page4, Page5, Page6, Page7, Page8 } from './pages';
import { daysOfTheWeek } from '../../../data/days';
import data from '../../../data/operating-periods';
import { Button } from 'devextreme-react';
import { useDispatch, useSelector } from 'react-redux';
import Loading from '../../../components/common/Loading';
import { product } from '../../../api/graphql/product';
import moment from 'moment';

const mockFetchAccountApi = () => {
    return new Promise((resolve) => {
        setTimeout(async () => {
            resolve({ operatingPeriods: data.sample });
        }, 500);
    });
};

const WizardControls = ({ wizard, step }) => {
    const totalSteps = wizard.getSteps().length;
    const buttonClassName =
        'mx-2 px-4 py-2 border border-sc-blue-200 text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-200 active:bg-sc-blue-200 transition duration-150 ease-in-out';
    const finishButtonClassName =
        'bg-sc-purple border-sc-purple hover:bg-sc-purple focus:outline-none focus:shadow-outline-purple focus:bg-sc-purple active:bg-sc-purple';

    // const onNext = async (args) => {
    //     await onSave();
    //     if (args) wizard.nextStep();
    // };

    return (
        <Fragment>
            <div className="flex justify-between">
                <div>
                    {step === 1 && step !== totalSteps && (
                        <Button className={buttonClassName}>Cancel</Button>
                    )}

                    {step !== 1 && step !== totalSteps && (
                        <Button
                            className={buttonClassName}
                            onClick={wizard.previousStep}
                        >
                            Go Back
                        </Button>
                    )}
                </div>
                <div>
                    {step === 1 && (
                        <Button
                            id="GetStarted"
                            className={buttonClassName}
                            useSubmitBehavior={true}
                        >
                            Get Started
                        </Button>
                    )}
                    {step > 1 && step !== totalSteps && (
                        <Button
                            id="SaveandExit"
                            className={buttonClassName}
                            // onClick={() => onNext(false)}
                            useSubmitBehavior={true}
                        >
                            Save and Exit
                        </Button>
                    )}
                    {step > 1 && step !== totalSteps - 1 && (
                        <Button
                            id="SaveandContinue"
                            className={buttonClassName}
                            useSubmitBehavior={true}

                        // onClick={() => onNext(true)}
                        >
                            Save and Continue
                        </Button>
                    )}

                    {step > 1 && step === totalSteps - 1 && (
                        <Button
                            id="Finish"
                            className={classNames(
                                buttonClassName,
                                finishButtonClassName
                            )}
                            useSubmitBehavior={true}

                        //onClick={onFinish}
                        >
                            FINISH UP
                        </Button>
                    )}
                </div>
            </div>
        </Fragment>
    );
};

const ProductWizard = (props) => {
    const [operatingPeriods, setOperatingPeriods] = useState([]);
    let productObj = {};
    var productImages = [];
    const steps = [
        'Product Category',
        'General Ticket Information',
        'Sales Channels & Access to this Product',
        'Product Types, Pricing, & Commision',
        'Pricing Details',
        'Taxes & Fees',
        'Inventory',
    ];

    const [title, setTitle] = useState(steps[0]);
    const [step, setStep] = useState(1);
    const [isLoading, setIsLoading] = useState(false);
    const [productId, setProductId] = useState(null);
    const [prodObj, setProduct] = useState({});
    const [isEditMode, setIsEditMode] = useState(false);

    const [wizardState, setWizardState] = useState({
        data: {
            basePricing: [],
            priceByPeriod: false,
            priceByDay: false,
            priceByDayPart: false,
            directCommission: 0,
            partnerCommission: 0,
            pricingDetails: [],
        },
    });
    const currentUser = useSelector((state) => state.user);

    const setInstance = (SW) => {
        setWizardState({
            ...wizardState,
            SW,
        });
    };

    useEffect(() => {
        setTitle(steps[step - 1]);
    }, [step]);

    useEffect(() => {
        if (isEditMode) {
            if (prodObj) wizardState.data = prodObj;
            if (SW) {
                if (prodObj.step === 4) {
                    let results = initializePricingDetails();
                    wizardState.data.pricingDetails = results;
                }
                SW.goToStep(prodObj.step + 1);
            }
        }
    }, [prodObj]);

    useEffect(() => {
        const init = async () => await initialize();
        init();
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            await fetchWizard();
        };
        fetchData();
    }, []);

    const { SW, demo } = wizardState;

    const handleSubmit = () => { };

    const onStepChange = (args) => {
        setStep(args.activeStep);
        if (args.previousStep === 4 && args.activeStep === 5) {
            let results = initializePricingDetails();
            wizardState.data.pricingDetails = results;
        }
    };

    const editSection = (step) => {
        SW.goToStep(step);
    }

    const initializePricingDetails = () => {
        let results = [];
        let bp = [...wizardState.data.basePricing];
        operatingPeriods.forEach((p) => {
            if (p.dayParts && p.dayParts.length > 0) {
                if (wizardState.data.priceByDay) {
                    p.dayParts.forEach((dp) => {
                        let type = 'DayPartAndDaily';
                        let pd = getDailyPricing(p, dp.name, bp, type);
                        results.push(pd);
                    });
                } else {
                    results.push(getDayPartPricing(p, bp));
                }
            } else {
                if (wizardState.data.priceByDay) {
                    let pd = getDailyPricing(p, 'All', bp);
                    results.push(pd);
                } else {
                    results.push({
                        type: 'Default',
                        name: p.name,
                        products: bp.map((m) => {
                            return {
                                product: m.productType,
                                retail: m.retail,
                                wholesale: m.wholesale,
                            };
                        }),
                    });
                }
            }
        });
        return results;
    };

    const getDailyPricing = (period, dayPart, pricingInfo, type = 'Daily') => {
        let products = [];
        pricingInfo.forEach((bp) => {
            let items = [];
            daysOfTheWeek.forEach((d) => {
                let isClosed = false;
                if (period.dailySchedule && period.dailySchedule.length > 0) {
                    let found = period.dailySchedule.filter(
                        (f) => f.name === d
                    );
                    if (found && found.length > 0) {
                        isClosed = found[0].isClosed;
                    }
                }
                items.push({
                    name: d,
                    isClosed: isClosed,
                    retail: bp.retail,
                    wholesale: bp.wholesale,
                });
            });
            products.push({ product: bp.productType, items });
        });
        return {
            type: type,
            name: period.name,
            dayPart,
            products,
        };
    };

    const getDayPartPricing = (period, pricingInfo) => {
        let products = [];
        pricingInfo.forEach((bp) => {
            let items = [];
            period.dayParts.forEach((d) => {
                items.push({
                    name: d.name,
                    retail: bp.retail,
                    wholesale: bp.wholesale,
                });
            });
            products.push({ product: bp.productType, items });
        });
        return {
            type: 'DayPart',
            name: period.name,
            products,
        };
    };

    const save = () => {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, 1000);
        });
    };

    const finish = () => {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, 2000);
        });
    };

    const initialize = async () => {
        const account = await mockFetchAccountApi();
        setOperatingPeriods(account.operatingPeriods);

        // TODO: Get Product & replace wizardState.data with product
        if (prodObj) wizardState.data = prodObj;
        const hasOperatingPeriods =
            account.operatingPeriods.filter((f) => f.name !== 'Calendar Year')
                .length > 0;
        wizardState.data.priceByPeriod = hasOperatingPeriods;

        const hasDayParts =
            account.operatingPeriods.filter(
                (f) => f.dayParts && f.dayParts.length > 0
            ).length > 0;
        wizardState.data.priceByDayPart = hasDayParts;
    };

    const fetchWizard = async () => {
        console.log('categoryId =' + props);
        var productId = localStorage.getItem('selectedProductId');

        if (productId != undefined && productId.length > 0) {
            const res = await fetchApi(productId);
            localStorage.setItem('selectedProductId', '');
            console.log('fetchWizard =' + res);
            setProductId(res._id);
            setIsEditMode(true);
            setProduct(res);
        } else {
            setProduct({});
        }
    };

    const fetchApi = (productId) => {
        return new Promise((resolve) => {
            setTimeout(async () => {
                var res = await product.productById(productId);
                resolve(res.productById);
            }, 500);
        });
    };
    const onFormSubmit = async (e) => {
        e.preventDefault();

        productObj.accountId = currentUser.bag.accountId;
        productObj.step = step;

        if (step === 1) {
            wizardState.data = prodObj;

            SW.nextStep();
        }
        if (step === 2) {
            if (
                wizardState.data.ticketName != null &&
                wizardState.data.ticketName != undefined
            ) {
                if (productId) {
                    productObj._id = productId;
                } else {
                    productObj.status = 'setup';
                }
                productObj.categoryId = localStorage.getItem('categoryId');
                productObj.name = productObj.name = wizardState.data.ticketName;
                productObj.shortDescription = wizardState.data.shortDesc;
                productObj.longDescription = wizardState.data.longDesc;
                productObj.termsAndConditions =
                    wizardState.data.termsAndConditions;
                productObj.endDate = moment(new Date())
                    .add(wizardState.data.expirationDays, 'days')
                    .format('YYYY-MM-DD');
                productObj.advanceSaleEndDate = moment(new Date())
                    .add(wizardState.data.advanceSaleDays, 'days')
                    .format('YYYY-MM-DD');
                productObj.expireDays = wizardState.data.expirationDays;
                productObj.advanceSaleDays = wizardState.data.advanceSaleDays;

                productObj.isActive = wizardState.data.isActive;
                productImages = wizardState.data.productImages;
                await saveProductInfo();
                SW.nextStep();
            }
        }
        if (step === 3) {
            productObj._id = productId;
            var salesAccess = {};
            salesAccess.revealCode = wizardState.data.revealCode;
            salesAccess.isModePublic = wizardState.data.isModePublic;
            salesAccess.isModePrivate = wizardState.data.isModePrivate;
            salesAccess.isMySalesChannel = wizardState.data.isMySalesChannel;
            salesAccess.isLocalPartners = wizardState.data.isLocalPartners;
            salesAccess.isLocalAndNationalSearch =
                wizardState.data.isLocalAndNationalSearch;
            productObj.salesAccess = salesAccess;
            await saveProductInfo();
            SW.nextStep();
        }
        if (step === 4) {
            productObj._id = productId;

            productObj.priceByDay = wizardState.data.priceByDay;
            productObj.directCommission = wizardState.data.directCommission;
            productObj.partnerCommission = wizardState.data.partnerCommission;
            productObj.priceByPeriod = wizardState.data.priceByPeriod;
            productObj.priceByDayPart = wizardState.data.priceByDayPart;
            productObj.basePricing = wizardState.data.basePricing;
            await saveProductInfo();
            //setProduct(productObj);
            SW.nextStep();
        }
        if (step === 5) {
            productObj._id = productId;
            productObj.pricingDetails = wizardState.data.pricingDetails;
            await saveProductInfo();
            SW.nextStep();
        }
        if (step === 6) {
            productObj._id = productId;
            if (wizardState.data.taxes) {
                productObj.taxes = wizardState.data.taxes;
            }
            if (wizardState.data.fees) {
                productObj.fees = wizardState.data.fees;
            }

            await saveProductInfo();
            SW.nextStep();
        }
        if (step === 7) {
            productObj._id = productId;
            productObj.status = 'pending';
            await saveProductInfo();
            SW.nextStep();
        }

        // needs to handle steps here, for now called nextstep
    };

    const saveProductInfo = async () => {
        console.log('product api log =' + productObj);
        var prod = await saveProductInfoApi(productObj);
    };
    const saveProductInfoApi = (productObj) => {
        return new Promise((resolve, error) => {
            setTimeout(async () => {
                var res = await product.saveProduct(productObj, productImages);
                if (res.saveProduct != undefined && res.saveProduct != null) {
                    setProductId(res.saveProduct._id);

                    resolve(res);
                } else {
                    setIsLoading(false);
                    alert('Error');
                }
                console.log('res   =' + res);
            }, 1000);
        });
    };

    return (
        <div className="px-2 sm:px-3 h-full">
            {!isLoading ? (
                <form onSubmit={onFormSubmit}>
                    <div className="min-h-full grid grid-cols-1 sm:grid-cols-1 col-gap-4">
                        <div className="mb-2 flex flex-col overflow-y-hidden">
                            <div className="flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                {step !== 1 && (
                                    <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6 flex items-center justify-between flex-wrap sm:flex-no-wrap">
                                        <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                            Product Wizard: {title}
                                        </h3>
                                    </div>
                                )}
                                <div className="h-full text-sm leading-5 text-gray-500">
                                    <StepWizard
                                        //className="overflow-x-hidden"
                                        className="pb-3"
                                        onStepChange={onStepChange}
                                        instance={setInstance}
                                    >
                                        <Page1 product={wizardState.data} />
                                        <Page2 product={wizardState.data} />
                                        <Page3 product={wizardState.data} />
                                        <Page4 product={wizardState.data} />
                                        <Page5 product={wizardState.data} />
                                        <Page6 product={wizardState.data} />
                                        {
                                            step === 7 ?
                                                (<Page7 product={wizardState.data} onEdit={editSection} />) :
                                                (<></>)
                                        }
                                        <Page8 product={wizardState.data} />
                                    </StepWizard>
                                </div>

                                {SW && (
                                    <div className="bg-white px-2 py-4 border-t border-gray-200 sm:px-4 flex flex-col">
                                        <WizardControls
                                            wizard={SW}
                                            step={step}
                                        // onSave={save}
                                        // onFinish={finish}
                                        />
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </form>
            ) : (
                    <Loading />
                )}
        </div>
    );
};

export default ProductWizard;
