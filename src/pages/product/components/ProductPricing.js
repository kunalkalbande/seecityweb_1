/* eslint-disable react/prop-types */
import { Accordion } from 'devextreme-react';
import { DefaultPricingGrid, ItemsPricingGrid } from './';
import React, { useState, useEffect } from 'react';
import { propTypes } from 'react-json-pretty';

const TitleRender = (data) => {
    return (
        <React.Fragment>
            {data.type === 'DayPartAndDaily' ? (
                <h1 className="text-sc-blue-400">
                    {data.name} - {data.dayPart}
                </h1>
            ) : (
                <h1 className="text-sc-blue-400">{data.name}</h1>
            )}
        </React.Fragment>
    );
};

const ProductPricing = (props) => {
    console.log("prop",props)
    console.log("prop",props.editMode)
   
    return (
        <div className="">
            <Accordion
                dataSource={props.product.pricingDetails}
                multiple={true}
                collapsible={true}
                itemRender={(data) =>
                    data.type === 'Default' ? (
                        <DefaultPricingGrid data={data} editMode={props.editMode===false?props.editMode:true} />
                    ) : (
                        <ItemsPricingGrid data={data} editMode={props.editMode===false?props.editMode:true} />
                    )
                }
                itemTitleRender={TitleRender}
            ></Accordion>
        </div>
    );
};
export default ProductPricing;
