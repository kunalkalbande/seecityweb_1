/* eslint-disable react/prop-types */
import React, { useState, useEffect, useRef } from 'react';
import { Accordion, DataGrid, SpeedDialAction } from 'devextreme-react';
import { Editing, Column, ValidationRule, Lookup } from 'devextreme-react/data-grid';
import Loading from '../../../components/common/Loading';

const appliedTypes = [
    {
        value: "Exclusive",
        name: "Exclusive"
    },
    {
        value: "Inclusive",
        name: "Inclusive"
    }
]

const taxNames = [
    {
        value: "Tax - 1",
        name: "Tax - 1"
    },
    {
        value: "Tax - 2",
        name: "Tax - 2"
    },
    {
        value: "Tax - 3",
        name: "Tax - 3"
    }
]

const feeNames = [
    {
        value: "Fee - 1",
        name: "Fee - 1"
    },
    {
        value: "Fee - 2",
        name: "Fee - 2"
    },
    {
        value: "Fee - 3",
        name: "Fee - 3"
    }
]

const TitleRender = (data) => {
    return (
        <React.Fragment>
            <h1 className="text-left text-sc-blue-400">{data.type}</h1>
        </React.Fragment>
    );
};

const TaxesGrid = (props) => {
    console.log(props);
    const update = () => {
        props.onTaxChange(props.data.items);
    }
    return (
        <div>
            <DataGrid
                id="taxes_grid"
                dataSource={props.data.items}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={false}
                rowAlternationEnabled={true}
                onRowInserted={update}
                onRowUpdated={update}
                onRowRemoved={update}
            >
                <Editing mode="form" allowAdding={props.isEdit} allowUpdating={props.isEdit} allowDeleting={props.isEdit} />

                <Column
                    dataField="taxName"
                    dataType="string"
                    caption="Tax Name"
                    allowSorting={false}
                >
                    <Lookup dataSource={taxNames} displayExpr="name" valueExpr="value" />
                    <ValidationRule type="required" />
                </Column>
                <Column
                    dataField="taxAmount"
                    dataType="number"
                    alignment="left"
                    format={{ type: 'currency', precision: 2 }}
                    caption="Tax Amount"
                    allowSorting={false}
                >
                    <ValidationRule type="required" />
                </Column>
                <Column
                    dataField="appliedType"
                    dataType="string"
                    caption="How is it applied?"
                    allowSorting={false}
                >
                    <Lookup dataSource={appliedTypes} displayExpr="name" valueExpr="value" />
                    <ValidationRule type="required" />
                </Column>
            </DataGrid>
        </div>
    );
}

const FeesGrid = (props) => {
    const update = () => {
        props.onFeeChange(props.data.items);
    }
    return (
        <div>
            <DataGrid
                id="fees_grid"
                dataSource={props.data.items}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={false}
                rowAlternationEnabled={true}
                onRowInserted={update}
                onRowUpdated={update}
                onRowRemoved={update}
            >
                <Editing mode="form" allowAdding={props.isEdit} allowUpdating={props.isEdit} allowDeleting={props.isEdit} />

                <Column
                    dataField="feeName"
                    dataType="string"
                    caption="Fee Name"
                    allowSorting={false}
                >
                    <Lookup dataSource={feeNames} displayExpr="name" valueExpr="value" />
                    <ValidationRule type="required" />
                </Column>
                <Column
                    dataField="feeAmount"
                    dataType="number"
                    alignment="left"
                    format={{ type: 'currency', precision: 2 }}
                    caption="Fee Amount"
                    allowSorting={false}
                >
                    <ValidationRule type="required" />
                </Column>
                <Column
                    dataField="appliedType"
                    dataType="string"
                    caption="How is it applied?"
                    allowSorting={false}
                >
                    <Lookup dataSource={appliedTypes} displayExpr="name" valueExpr="value" />
                    <ValidationRule type="required" />
                </Column>
            </DataGrid>
        </div>
    );
}

const TaxesAndFees = (props) => {
    const [dataSource, setDataSource] = useState([
        {
            type: "Taxes",
            items: []
        },
        {
            type: "Fees",
            items: []
        }
    ]);
    const [isLoading, setIsLoading] = useState(true);
    const [taxes, setTaxes] = useState([]);
    const [fees, setFees] = useState([]);
    const [isEdit, setIsEdit] = useState(props.isEditMode === false ? false : true);
    // useEffect(() => {
    //     const init = async () => {
    //         console.log("tax -> ", props)
    //         setIsLoading(true);
    //         //setDataSource(taxAndFeesData);
    //         setIsLoading(false);
    //     }
    //     init();
    // }, []);
    console.log(props);
    useEffect(() => {
        if (props.product.taxes != undefined) {
            var taxAndFeesData = [
                {
                    type: "Taxes",
                    items: props.product.taxes
                },
                {
                    type: "Fees",
                    items: props.product.fees
                }
            ]
            setTaxes(props.product.taxes);
            setFees(props.product.fees);
            setDataSource(taxAndFeesData);
        }
        setIsLoading(false);
    }, [props.product]);

    useEffect(() => {
        props.product.taxes = taxes;
        props.product.fees = fees;
        if (props.onChange !== undefined) {
            props.onChange({ taxes, fees });
        }
    }, [taxes, fees]);

    const updateTaxes = (data) => {
        var temp = [];
        data.forEach(element => {
            temp.push({
                taxName: element.taxName,
                taxAmount: element.taxAmount,
                appliedType: element.appliedType
            });
        });
        setTaxes(temp);
    }

    const updateFees = (data) => {
        var temp = [];
        data.forEach(element => {
            temp.push({
                feeName: element.feeName,
                feeAmount: element.feeAmount,
                appliedType: element.appliedType
            });
        });
        setFees(temp);
    }

    if (isLoading) {
        return (<Loading />);
    }
    return (
        <>
            <div className="overflow-x-hidden">
                <div className="max-h-screen">
                    <div className="p-3">
                        <Accordion
                            dataSource={dataSource}
                            multiple={true}
                            collapsible={true}
                            itemRender={(data) =>
                                data.type === 'Taxes' ? (
                                    <TaxesGrid data={data} onTaxChange={updateTaxes} isEdit={props.isEditMode === false ? false : true} />
                                ) : (
                                        <FeesGrid data={data} onFeeChange={updateFees} isEdit={props.isEditMode === false ? false : true} />
                                    )
                            }
                            itemTitleRender={TitleRender}
                        ></Accordion>
                    </div>
                </div>
            </div>
        </>
    );
};

export default TaxesAndFees;