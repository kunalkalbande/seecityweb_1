/* eslint-disable react/prop-types */
import React from 'react';
import DataGrid, { Column, Editing } from 'devextreme-react/data-grid';

const DefaultPricingGrid = ({ data,editMode }) => {
    let items = [...data.products];

    const onRowUpdated = (args) => {
        let target = data.products.filter(
            (f) => f.product === args.data.product
        )[0];

        target.retail = args.data.retail;
        target.wholesale = args.data.wholesale;
    };

    return (
        <DataGrid
            id="default_pricing_grid"
            dataSource={items}
            columnAutoWidth={true}
            showBorders={true}
            showColumnLines={true}
            showRowLines={false}
            rowAlternationEnabled={true}
            onRowUpdated={onRowUpdated}
        >
            <Editing mode="cell" allowUpdating={editMode} />

            <Column
                dataField="product"
                caption="Product"
                width={100}
                allowSorting={false}
                allowEditing={false}
            />

            <Column
                width={85}
                dataField="retail"
                dataType="number"
                caption="Retail"
                allowSorting={false}
            />
            <Column
                width={85}
                dataField="wholesale"
                dataType="number"
                caption="Wholesale"
                allowSorting={false}
            />
        </DataGrid>
    );
};
export default DefaultPricingGrid;
