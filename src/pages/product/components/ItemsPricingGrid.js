/* eslint-disable react/prop-types */
import React from 'react';
import DataGrid, { Column, Editing } from 'devextreme-react/data-grid';

const toProductsDataSource = (products, isDaily) => {
    /*
        This code essentially converts the items array into properties
    */
    let items = products.map((product) => {
        let details = product.items.map((item) => {
            return {
                [item.name]: {
                    closed: item.isClosed || false,
                    retail: item.retail,
                    wholesale: item.wholesale,
                },
            };
        });
        let result = { product: product.product };
        details.forEach((d) => {
            result[Object.keys(d)[0]] = Object.values(d)[0];
        });
        return result;
    });
    return {
        items,
        columns: products[0].items.map((m) => {
            return { name: m.name, isClosed: isDaily ? m.isClosed : false };
        }),
    };
};

const ItemsPricingGrid = ({ data ,editMode}) => {
    console.log("editMode",editMode);
    let config = toProductsDataSource(
        [...data.products],
        data.type.includes('Daily')
    );

    let showClosedMessage = config.columns.filter((f) => f.isClosed).length > 0;

    const onRowUpdated = (args) => {
        let target = data.products.filter(
            (f) => f.product === args.data.product
        )[0];

        target.items.forEach((item) => {
            item.retail = args.data[item.name].retail;
            item.wholesale = args.data[item.name].wholesale;
        });
    };

    return (
        <div>
            <DataGrid
                id="pricing_grid"
                dataSource={config.items}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={false}
                rowAlternationEnabled={true}
                onRowUpdated={onRowUpdated}
            >
                <Editing mode="cell" allowUpdating={editMode} />

                <Column
                    dataField="product"
                    caption="Product"
                    width={100}
                    allowSorting={false}
                    allowEditing={false}
                />
                {config.columns.map((column, index) => (
                    <Column
                        key={index}
                        caption={`${column.name} ${column.isClosed ? '*' : ''}`}
                        alignment="right"
                    >
                        <Column
                            width={85}
                            dataField={`${column.name}.retail`}
                            dataType="number"
                            caption="Retail"
                            allowSorting={false}
                        />
                        <Column
                            width={85}
                            dataField={`${column.name}.wholesale`}
                            dataType="number"
                            caption="Wholesale"
                            allowSorting={false}
                        />
                    </Column>
                ))}
            </DataGrid>
            {showClosedMessage && (
                <span className="mt-2 text-xs italic text-gray-400">
                    * indicates a day operations are closed
                </span>
            )}
        </div>
    );
};
export default ItemsPricingGrid;
