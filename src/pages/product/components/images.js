import attractionTicket from '../../../assets/img/Attraction_Ticket_Option.png';
import eventTicket from '../../../assets/img/Attraction_Ticket_Option.png';
import parking from '../../../assets/img/Attraction_Ticket_Option.png';
import foodAndBeverage from '../../../assets/img/Attraction_Ticket_Option.png';
import valueBundle from '../../../assets/img/Attraction_Ticket_Option.png';
import souvenir from '../../../assets/img/Attraction_Ticket_Option.png';

export default {
    productCategories: {
        ATTRACTION_TICKET: attractionTicket,
        EVENT_TICKET: eventTicket,
        PARKING: parking,
        FOOD_AND_BEVERAGE: foodAndBeverage,
        VALUE_BUNDLE: valueBundle,
        WEARABLE_MERCHANDISE: valueBundle,
        SOUVENIR: souvenir,
    },
};
