/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { NumberBox, ValidationSummary } from 'devextreme-react';
import { RangeRule, RequiredRule, Validator } from 'devextreme-react/validator';

const BasePricingForm = ({ item,readOnly }) => {
    const [retail, setRetail] = useState(item.retail);
    const [wholesale, setWholesale] = useState(item.wholesale);
    const[isreadMode,setIsReadMode]=useState(readOnly?readOnly:false);
    useEffect(() => {
        item.retail = retail;
    }, [retail]);

    useEffect(() => {
        item.wholesale = wholesale;
    }, [wholesale]);

    // const onWholeSalePriceChange = (e) => {
    //     setWholesale(e.value);
    //     var priceItem = {};
    //     priceItem.wholesale = e.value;
    //     priceItem.productType = e.element.id;
    //     onChange(priceItem);
    // };
    // const onRetailPriceChange = (e) => {
    //     setRetail(e.value);
    //     var priceItem = {};
    //     priceItem.retail = e.value;
    //     priceItem.productType = e.element.id;
    //    //onChange(priceItem);
    // };

    return (
        <div className="mt-2 mb-2 sm:mb-1 grid grid-cols-3 sm:grid-cols-6 row-gap-2 col-gap-4">
            <div className="col-span-3 sm:col-span-1">
                <label
                    htmlFor="name"
                    className="sm:invisible sm:h-0 block text-sm font-medium leading-5 text-gray-400"
                >
                    Type
                </label>
                <div className="mt-1 text-gray-500 text-sm">
                    {item.productType}
                </div>
            </div>
            <div className="col-span-3 sm:col-span-1">
                <label
                    htmlFor="retail"
                    className="sm:invisible sm:h-0 block text-sm font-medium leading-5 text-gray-400"
                >
                    Retail
                </label>
                <div className="mt-1">
                    <NumberBox
                        id={item.productType}
                        format="$ #,##0.##"
                        mode="number"
                        value={retail}
                        readOnly={readOnly?readOnly:false}
                        onValueChanged={(e) => setRetail(e.value)}
                    >
                        <Validator>
                            <RequiredRule message="retail price required" />
                            <RangeRule
                                message="retail price must be > $0"
                                min="1"
                            />
                        </Validator>
                    </NumberBox>
                </div>
            </div>
            <div className="col-span-3 sm:col-span-1">
                <label
                    htmlFor="wholesale"
                    className="sm:invisible sm:h-0 block text-sm font-medium leading-5 text-gray-400"
                >
                    Wholesale
                </label>
                <div className="mt-1">
                    <NumberBox
                        id={item.productType}
                        format="$ #,##0.##"
                        mode="number"
                        value={wholesale}
                        readOnly={readOnly?readOnly:false}
                        onValueChanged={(e) => setWholesale(e.value)}
                    >
                        <Validator>
                            <RequiredRule message="Wholesale price required" />
                            <RangeRule
                                message="Wholesale price must be > $0"
                                min="1"
                            />
                        </Validator>
                    </NumberBox>
                </div>
            </div>
            <div className="sm:col-span-2">
                <ValidationSummary id="summary"></ValidationSummary>
            </div>
        </div>
    );
};

export default BasePricingForm;
