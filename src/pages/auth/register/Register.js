import React, { useState, Component } from "react";
import logo from "../../../assets/img/logos/logo-primary-white.png";
import ReCaptcha from "../recaptcha";
import { EmailConfirmationPage } from "../../../pages";
import facebook_img from "../../../assets/img/Sign-up-with-Facebook.png";
import google_img from "../../../assets/img/Sign-up-with-Google.png";
import { render } from "@testing-library/react";
import { authenticationService } from "../../../services/authentication.service";
import { useForm } from "react-hook-form";
import { Tooltip } from "devextreme-react";
import LoadingV2 from "../../../components/common/LoadingV2";

const CreateAccount = () => {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [phone, setPhone] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [result, setResult] = useState("");
	const [isEmailConfirmationPage, setIsEmailConfirmationPage] = useState(false);
	const { handleSubmit } = useForm();

	const [emailError, setEmailError] = useState(false);
	const [passwordError, setPasswordError] = useState(false)
	const [phoneError, setPhoneError] = useState(false)
	const [firstNameError, setFirstNameError] = useState(false)
	const [lastNameError, setLastNameError] = useState(false)

	const [isEmailFocused, setIsEmailFocused] = useState(false)
	const [isPasswordFocused, setIsPasswordFocused] = useState(false)
	const [isPhoneFocused, setIsPhoneFocused] = useState(false)
	const [isFirstNameFocused, setIsFirstNameFocused] = useState(false)
	const [isLastNameFocused, setIsLastNameFocused] = useState(false)

	const [isShowPassword, setIsShowPassword] = useState(false)
	const handleShowHidePasswordClick = () => setIsShowPassword(!isShowPassword);

	const [isLoading, setIsLoading] = useState(false);

	const validateEmail = () => {
		setIsEmailFocused(false)
		if (email.length > 0) {
			setEmailError(false)
		}
		else {
			setEmailError(true)
		}
	}

	const validatePassword = () => {
		setIsPasswordFocused(false)
		if (password.length > 0) {
			setPasswordError(false)
		}
		else {
			setPasswordError(true)
		}
	}

	const validatePhone = () => {
		setIsPhoneFocused(false)
		if (phone.length > 0) {
			setPhoneError(false)
		}
		else {
			setPhoneError(true)
		}
	}

	const validateFirstName = () => {
		setIsFirstNameFocused(false)
		if (firstName.length > 0) {
			setFirstNameError(false)
		}
		else {
			setFirstNameError(true)
		}
	}

	const validateLastName = () => {
		setIsLastNameFocused(false)
		if (lastName.length > 0) {
			setLastNameError(false)
		}
		else {
			setLastNameError(true)
		}
	}

	const validate = () => {
		validateFirstName()
		validateLastName()
		validateEmail()
		validatePhone()
		validatePassword()
		if (firstName.length > 0 &&
			lastName.length > 0 &&
			email.length > 0 &&
			phone.length > 0 &&
			password.length > 0) {
			return true
		}
		else {
			return false
		}
	}

	const createAccount = async () => {
		setIsLoading(true);
		if (validate()) {
			var result = await authenticationService.SignUp(email, password, firstName, lastName, phone
			);
			if (sessionStorage.getItem("isEmailVerified") === "true") {
				setIsEmailConfirmationPage({
					isEmailConfirmationPage: true,
				});
			} else {
				var errorMessage = document.getElementById("error-message");
				errorMessage.innerHTML = result.description;
				errorMessage.style.display = "block";
			}
		}
		setIsLoading(false);
	};

	if (isEmailConfirmationPage) {
		return <EmailConfirmationPage email={email} />;
	}
	return (
		<div className="auth-bg bg-blue-400 min-h-screen sm:h-auto lg:h-screen w-full">
			{
				isLoading ? (<LoadingV2 />) : (<></>)
			}
			<div className="flex flex-col items-center flex-1 h-full justify-center px-4 py-4 sm:px-0">
				<div className="w-full auth-w-900 sm:w-3/4 lg:w-8/12 xl:w-7/12 sm:mx-0">
					<img className="mb-5 w-40" src={logo} alt="see-city-img" />
					<div className="auth-form form-img flex rounded-lg shadow-lg w-full bg-indigo-100 sm:mx-0">
						<div className="form-container max-w-sm sm:w-12/12 lg:w-8/12 rounded overflow-hidden">
							<div className="px-8 py-4">
								<div className="flex flex-col w-full p-0">
									<div className="flex flex-col flex-1 justify-center mb-4">
										<h1 className="text-2xl text-center font-bold">
											Create Account
                    </h1>
										<div className="w-full mt-0">
											<div
												id="error-message"
												className="alert alert-danger text-red-700"
											></div>
											<form
												className="form-horizontal w-4/4 mx-auto"
												onSubmit={handleSubmit(createAccount)}
											>
												<div className="flex flex-col mt-3">
													<div className={`flex items-center bg-white border ${firstNameError || lastNameError ? "border-2 border-red-500" : "border-gray-400"} shadow-md rounded`}>
														<span className="px-3">
															<svg
																xmlns="http://www.w3.org/2000/svg"
																width="25"
																height="25"
																viewBox="0 0 24 24"
																fill="none"
																stroke={firstNameError || lastNameError ? "red" : "#909090"}
															>
																<path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
																<circle cx="12" cy="7" r="4"></circle>
															</svg>
														</span>
														<div className="w-full">
															<input
																id="firstName"
																value={firstName}
																onChange={(e) => setFirstName(e.target.value)}
																onFocus={() => setIsFirstNameFocused(true)}
																onBlur={() => validateFirstName()}
																type="text"
																className={`w-6/12 h-10 px-4 rounded-r border-l border-gray-400 ${firstNameError ? "outline-red" : "outline"}`}
																name="firstName"
																//required
																placeholder="First Name"
																style={{
																	width: "calc(50% - 1px)",
																}}
															/>

															<span className="border-l border-gray-400"></span>
															<input
																id="lastName"
																value={lastName}
																onChange={(e) => setLastName(e.target.value)}
																onFocus={() => setIsLastNameFocused(true)}
																onBlur={() => validateLastName()}
																type="text"
																className={`w-6/12 h-10 px-4 rounded-r ${lastNameError ? "outline-red" : "outline"}`}
																name="lastName"
																//required
																placeholder="Last Name"
															/>

														</div>
														<Tooltip
															target="#firstName"
															visible={firstNameError && isFirstNameFocused}
															closeOnOutsideClick={false}
														>
															<div className="text-red-500">First Name is required</div>
														</Tooltip>
														<Tooltip
															target="#lastName"
															visible={lastNameError && isLastNameFocused}
															closeOnOutsideClick={false}
														>
															<div className="text-red-500">Last Name is required</div>
														</Tooltip>
													</div>
												</div>
												<div className="flex flex-col mt-3">
													<div className={`flex items-center bg-white border ${emailError ? "border-2 border-red-500" : "border-gray-400"} shadow-md rounded`}>
														<span className="px-3">
															<svg
																xmlns="http://www.w3.org/2000/svg"
																width="25"
																height="25"
																viewBox="0 0 24 24"
																fill="none"
																stroke={emailError ? "red" : "#909090"}
															>
																<path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
																<polyline points="22,6 12,13 2,6"></polyline>
															</svg>
														</span>
														<input
															id="email"
															value={email}
															onChange={(e) =>
																setEmail(
																	e.target
																		.value
																)
															}
															onFocus={() => setIsEmailFocused(true)}
															onBlur={() =>
																validateEmail()
															}
															type="text" autoComplete="off"
															className={`w-full h-10 px-4 rounded-r ${emailError ? "outline-red" : "outline"}`}
															name="email"
															placeholder="Email"
															//required
															style={{
																borderLeft:
																	'1px solid #a0aec0',
															}}
														/>
														<Tooltip
															target="#email"
															visible={emailError && isEmailFocused}
															closeOnOutsideClick={false}
														>
															<div className="text-red-500">Email is required</div>
														</Tooltip>
													</div>
												</div>
												<div className="flex flex-col mt-3">
													<div className={`flex items-center bg-white border ${phoneError ? "border-2 border-red-500" : "border-gray-400"} shadow-md rounded`}>
														<span className="px-3">
															<svg
																xmlns="http://www.w3.org/2000/svg"
																width="25"
																height="25"
																viewBox="0 0 24 24"
																fill="none"
																stroke={phoneError ? "red" : "#909090"}
															>
																<path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
															</svg>
														</span>
														<input
															id="phone"
															value={phone}
															onChange={(e) => setPhone(e.target.value)}
															onFocus={() => setIsPhoneFocused(true)}
															onBlur={() => validatePhone()}
															type="text"
															className={`w-full h-10 px-4 rounded-r ${phoneError ? "outline-red" : "outline"}`}
															name="phone"
															placeholder="Phone"
															style={{
																borderLeft: "1px solid #a0aec0",
															}}
														/>
														<Tooltip
															target="#phone"
															visible={phoneError && isPhoneFocused}
															closeOnOutsideClick={false}
														>
															<div className="text-red-500">Phone is required</div>
														</Tooltip>
													</div>
												</div>
												<div className="flex flex-col mt-3">
													<div className={`flex items-center bg-white border ${passwordError ? "border-2 border-red-500" : "border-gray-400"} shadow-md rounded`}>
														<span className="px-3">
															<svg
																xmlns="http://www.w3.org/2000/svg"
																width="25"
																height="25"
																viewBox="0 0 24 24"
																fill="none"
																stroke={passwordError ? "red" : "#909090"}
															>
																<rect
																	x="3"
																	y="11"
																	width="18"
																	height="11"
																	rx="2"
																	ry="2"
																></rect>
																<path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
															</svg>
														</span>
														<input
															id="password"
															value={password}
															onFocus={() => setIsPasswordFocused(true)}
															onChange={(e) =>
																setPassword(
																	e.target
																		.value
																)
															}
															onBlur={() => validatePassword()}
															type={isShowPassword ? "text" : "password"}
															className={`w-full h-10 px-4 rounded-r ${passwordError ? "outline-red" : "outline"}`}
															name="password"
															//required
															placeholder="Password"
															style={{
																borderLeft:
																	'1px solid #a0aec0',
															}}
														/>
														<Tooltip
															target="#password"
															visible={passwordError && isPasswordFocused}
															closeOnOutsideClick={false}
														>
															<div className="text-red-500">Password is required</div>
														</Tooltip>
														<span className="px-3 cursor-pointer" onClick={() => handleShowHidePasswordClick()}>
															<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24" fill="none" stroke="#909090"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle>{isShowPassword ? <line id="svg_4" y2="3.357021" x2="20.356156" y1="20.891263" x1="3.095886" stroke="#909090" fill="none" /> : ""}</svg>
														</span>
													</div>
												</div>
												<ReCaptcha />
												<div className="flex flex-col mt-8">
													<div className="w-full">
														<button
															type="submit"
															id="btn-create-account"
															className="bg-blue-500 hover:bg-blue-700 text-white text-sm font-semibold py-2 px-4 rounded w-full"
															style={{
																backgroundColor: "#3db5e7",
															}}
														>
															CREATE ACCOUNT →
                            </button>
													</div>
												</div>
											</form>
											<div className="w-full py-2">
												<div
													className="text-center text-gray-500 leading-none my-2"
													style={{
														borderBottom: "1px solid #a0aec0",
														lineHeight: "0.3em",
													}}
												>
													<span
														className="px-3"
														style={{
															background: "#f6fbfe",
														}}
													>
														or
                          </span>
												</div>
											</div>
											<div className="flex flex-col md:flex-row w-full mt-2">
												<button
													className="w-full md:w-1/2 text-xs m-1"
													id="btn-google"
												>
													<img
														src={google_img}
														className="h-10 w-full md:h-8"
													></img>
												</button>
												<button
													className="w-full md:w-1/2 text-xs m-1"
													id="btn-facebook"
												>
													<img
														src={facebook_img}
														className="h-10 w-full md:h-8"
													></img>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CreateAccount;
