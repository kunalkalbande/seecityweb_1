import React, { useState, useEffect } from "react";
import logo from "../../../assets/img/logos/logo-primary-white.png"
import { LoginPage, RegisterPage, VenueWelcomeScreen, AccountRecoveryMessage } from "../../../pages"
import { authenticationService } from "../../../services";
import { Tooltip } from "devextreme-react";
import { useForm } from "react-hook-form";

const AccountRecovery = ({ _email }) => {
	useEffect(() => {
		if (_email) {
			setEmail(_email);
		}
	}, []);
	const { handleSubmit } = useForm();

	const [email, setEmail] = useState('');

	const [emailError, setEmailError] = useState(false);
	const [isEmailFocused, setIsEmailFocused] = useState(false)

	const validateEmail = () => {
		setIsEmailFocused(false)
		if (email.length > 0) {
			setEmailError(false)
		}
		else {
			setEmailError(true)
		}
	}

	const findAccount = () => {
		validateEmail()
		if (email.length > 0) {
			authenticationService.findAccount(email);
			setIsAccountRecoveryMessagePage(true);
		}
	}

	const [isLoginPage, setIsLoginPage] = useState(false);
	const handleSignInClick = () => setIsLoginPage(true);

	const [isRegisterPage, setIsRegisterPage] = useState(false);
	const handleJoinNowClick = () => setIsRegisterPage(true);

	const [isWelcomePage, setIsWelcomePage] = useState(false);
	const handleCancelClick = () => setIsWelcomePage(true);

	const [isAccountRecoveryMessagePage, setIsAccountRecoveryMessagePage] = useState(false);

	if (isLoginPage) {
		return (<LoginPage />);
	}
	else if (isRegisterPage) {
		return (<RegisterPage />);
	}
	else if (isWelcomePage) {
		return (<VenueWelcomeScreen />);
	}
	else if (isAccountRecoveryMessagePage) {
		return (<AccountRecoveryMessage email={email} />);
	}
	else {
		return (
			<div className="auth-bg bg-blue-400 min-h-screen sm:h-auto lg:h-screen w-full">
				<div className="flex flex-col items-center flex-1 h-full justify-center px-4 py-4 sm:px-0">

					<div className="w-full auth-w-900 sm:w-3/4 lg:w-8/12 xl:w-7/12 sm:mx-0">

						<img className="mb-5 w-40" src={logo} alt="see-city-img" />
						<div className="auth-form form-img flex rounded-lg shadow-lg w-full bg-indigo-100 sm:mx-0">

							<div className="form-container max-w-sm sm:w-12/12 lg:w-8/12 rounded overflow-hidden">
								<div className="px-8 py-10">

									<div className="flex flex-col w-full p-0">
										<div className="flex flex-col flex-1 justify-center mb-4">
											<h1 className="text-2xl text-center font-bold">First, let's find your account...</h1>
											<div className="w-full mt-0">
												<div id="error-message" className="alert alert-danger text-red-700">
												</div>
												<form
													className="form-horizontal w-4/4 mx-auto"
													onSubmit={(handleSubmit(findAccount))}
												>
													<div className="flex flex-col mt-4">
														<div className={`flex items-center bg-white border ${emailError ? "border-2 border-red-500" : "border-gray-400"} shadow-md rounded`}>
															<span className="px-3">
																<svg
																	xmlns="http://www.w3.org/2000/svg"
																	width="25"
																	height="25"
																	viewBox="0 0 24 24"
																	fill="none"
																	stroke={emailError ? "red" : "#909090"}
																>
																	<path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
																	<polyline points="22,6 12,13 2,6"></polyline>
																</svg>
															</span>
															<input
																id="email"
																value={email}
																onChange={(e) =>
																	setEmail(
																		e.target
																			.value
																	)
																}
																onFocus={() => setIsEmailFocused(true)}
																onBlur={() =>
																	validateEmail()
																}
																type="text" autoComplete="off"
																className={`w-full h-10 px-4 rounded-r ${emailError ? "outline-red" : "outline"}`}
																name="email"
																placeholder="Email"
																//required
																style={{
																	borderLeft:
																		'1px solid #a0aec0',
																}}
															/>
															<Tooltip
																target="#email"
																visible={emailError && isEmailFocused}
																closeOnOutsideClick={false}
															>
																<div className="text-red-500">Email is required</div>
															</Tooltip>
														</div>
													</div>

													<div className="px-0 mt-4">
														<div className="flex -mx-2">
															<div className="w-1/2 px-2">
																<button onClick={() => findAccount()} id="btn-find-account" className="bg-blue-500 hover:bg-blue-700 text-white text-sm font-semibold py-2 px-4 rounded w-full" style={{ backgroundColor: "#3db5e7" }}>
																	FIND ACCOUNT →
                                </button>
															</div>
															<div className="w-1/2 px-2">
																<button onClick={(event) => handleCancelClick(event, false)} type="submit" id="btn-cancel" className="bg-white border border-blue-700 hover:bg-white-500 text-blue-500 text-sm font-semibold py-2 px-4 rounded w-full" style={{ borderColor: "#3db5e7", color: "#0692d4" }}>
																	CANCEL
                                </button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>

										<div className="flex flex-col flex-1 justify-center mb-4 mt-20">
											<div className="w-full py-2">
												<div className="text-center text-gray-500 leading-none my-2" style={{ borderBottom: "1px solid #a0aec0", lineHeight: "0.3em" }}>
													<span className="px-3" style={{ background: "#f6fbfe" }}>
														or
                          </span>
												</div>
											</div>
											<div className="w-full py-2">
												<button onClick={(event) => handleSignInClick(event, false)} type="submit" id="btn-sign-in" className="bg-blue-500 hover:bg-blue-700 text-white text-sm font-semibold py-2 px-4 rounded w-full" style={{ backgroundColor: "#3db5e7" }}>
													SIGN IN →
                        </button>
											</div>
											<div className="w-full py-2">
												<div className="text-center text-gray-500 leading-none my-2" style={{ borderBottom: "1px solid #a0aec0", lineHeight: "0.3em" }}>
													<span className="px-3" style={{ background: "#f6fbfe" }}>
														or
                        </span>
												</div>
											</div>
											<div className="w-full py-2">
												<button onClick={(event) => handleJoinNowClick(event, false)} type="submit" id="btn-join-now" className="bg-white border border-blue-700 hover:bg-white-500 text-blue-500 text-sm font-semibold py-2 px-4 rounded w-full" style={{ borderColor: "#3db5e7", color: "#0692d4" }}>
													JOIN NOW →
                      </button>
											</div>
										</div>

									</div>

								</div>
							</div>

						</div>

					</div>

				</div>
			</div >

		);
	}
}

export default AccountRecovery;