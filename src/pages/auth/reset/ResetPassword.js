import React, { useState, useEffect } from "react";
import user from "../../../assets/img/GuestX_VerifyEmailIllustrationTrans.png"
import logo from "../../../assets/img/logos/logo-primary-white.png"
import ReCaptcha from "../recaptcha"
import { Tooltip } from "devextreme-react";
import { useForm } from "react-hook-form";
import { useHistory, Redirect } from "react-router-dom";
import { authenticationService } from "../../../services/authentication.service";
import LoginPage from "../login/Login";

const ForgotPassword = () => {
	const [newPassword, setNewPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("")
	const [isShowNewPassword, setIsShowNewPassword] = useState(false)
	const [isShowConfirmPassword, setIsShowConfirmPassword] = useState(false)

	const [confirmPasswordErrorText, setConfirmPasswordErrorText] = useState("")

	const [newPasswordError, setNewPasswordError] = useState(false);
	const [confirmPasswordError, setConfirmPasswordError] = useState(false)
	const [isNewPasswordFocused, setIsNewPasswordFocused] = useState(false)
	const [isConfirmPasswordFocused, setIsConfirmPasswordFocused] = useState(false)
	const [isPasswordChanged,setPasswordChanged] = useState(false);

	const { handleSubmit } = useForm();
	const history = useHistory();

	useEffect(() => {
		authenticationService.getManagementAccessToken();		
	}, []);

	const updateUser = async() =>
	{
		var token= sessionStorage.getItem('Token');	
		var id = sessionStorage.getItem('id');	
		const requestOptions = {
			method: 'PATCH',
			headers: { 'Content-Type': 'application/json' ,
			'Authorization': 'Bearer '+token
		            },
			body: JSON.stringify({ 
			user_metadata: {"forceReset":false},
			"password": newPassword,


		})
		};
	   await fetch(`https://seecity.auth0.com/api/v2/users/${id}`, requestOptions)
			.then(response => response.json())
			.then( data => {
				authenticationService.logout();
				history.push('/login');
			});
			sessionStorage.removeItem("id");
			sessionStorage.removeItem("Token");	

	}

	const resetPassword = async () => {
		validateNewPassword()
		validateConfirmPassword()
		if (newPassword.length > 0 && confirmPassword.length > 0) {
		}
	};

	const validateNewPassword = () => {
		setIsNewPasswordFocused(false)
		if (newPassword.length > 0 && newPassword === confirmPassword) {
			setNewPasswordError(false)
		}
		else {
			setNewPasswordError(true)

		}
	}

	const validateConfirmPassword = () => {
		setIsConfirmPasswordFocused(false)
		if (confirmPassword.length > 0) {
			if (newPassword === confirmPassword) {
				setConfirmPasswordError(false)
			} else {
				setConfirmPasswordError(true)
				setConfirmPasswordErrorText("New password & confirm password should be same")
			}
		}
		else {
			setConfirmPasswordError(true)
			setConfirmPasswordErrorText("Confirm password is required")
		}
	}

	const handleShowHideNewPasswordClick = () => setIsShowNewPassword(!isShowNewPassword);
	const handleShowHideConfirmPasswordClick = () => setIsShowConfirmPassword(!isShowConfirmPassword);


	return (
		<div className="auth-bg bg-blue-400 min-h-screen sm:h-auto lg:h-screen w-full">
			<div className="flex flex-col items-center flex-1 h-full justify-center px-4 py-4 sm:px-0">

				<div className="w-full auth-w-900 sm:w-3/4 lg:w-8/12 xl:w-7/12 sm:mx-0">

					<img className="mb-5 w-40" src={logo} alt="see-city-img" />
					<div className="auth-form flex rounded-lg shadow-lg w-full bg-indigo-100 sm:mx-0">

						<div className="max-w-sm sm:w-12/12 lg:w-8/12 rounded overflow-hidden m-auto">
							<img className="w-1/2 mx-auto mt-8" src={user} alt="Sunset in the mountains" />
							<div className="px-6 py-4">

								<div className="flex flex-col w-full p-0">
									<div className="flex flex-col flex-1 justify-center mb-4">
										<h1 className="text-2xl text-center font-bold">Password Reset</h1>
										<div className="w-full mt-0">
											<div id="error-message" className="alert alert-danger text-red-700">
											</div>
											<form
												className="form-horizontal w-4/4 mx-auto"
												onSubmit={handleSubmit(updateUser)}
											>
												<div className="flex flex-col mt-4">
													<div className={`flex items-center bg-white border ${newPasswordError ? "border-2 border-red-500" : "border-gray-400"} shadow-md rounded`}>
														<span className="px-3">
															<svg
																xmlns="http://www.w3.org/2000/svg"
																width="25"
																height="25"
																viewBox="0 0 24 24"
																fill="none"
																stroke={newPasswordError ? "red" : "#909090"}
															>
																<rect
																	x="3"
																	y="11"
																	width="18"
																	height="11"
																	rx="2"
																	ry="2"
																></rect>
																<path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
															</svg>
														</span>
														<input
															id="newPassword"
															value={newPassword}
															onFocus={() => setIsNewPasswordFocused(true)}
															onChange={(e) =>
																setNewPassword(
																	e.target
																		.value
																)
															}
															onBlur={() => validateNewPassword()}
															type={isShowNewPassword ? "text" : "password"}
															className={`w-full h-10 px-4 rounded-r ${newPasswordError ? "outline-red" : "outline"}`}
															name="newPassword"
															//required
															placeholder="New Password"
															style={{
																borderLeft:
																	'1px solid #a0aec0',
															}}
														/>
														<Tooltip
															target="#newPassword"
															visible={newPasswordError && isNewPasswordFocused}
															closeOnOutsideClick={false}
														>
															<div className="text-red-500">New Password is required</div>
														</Tooltip>
														<span className="px-3 cursor-pointer" onClick={() => handleShowHideNewPasswordClick()}>
															<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24" fill="none" stroke="#909090"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle>{isShowNewPassword ? <line id="svg_4" y2="3.357021" x2="20.356156" y1="20.891263" x1="3.095886" stroke="#909090" fill="none" /> : ""}</svg>
														</span>
													</div>
												</div>
												<div className="flex flex-col mt-4">
													<div className={`flex items-center bg-white border ${confirmPasswordError ? "border-2 border-red-500" : "border-gray-400"} shadow-md rounded`}>
														<span className="px-3">
															<svg
																xmlns="http://www.w3.org/2000/svg"
																width="25"
																height="25"
																viewBox="0 0 24 24"
																fill="none"
																stroke={confirmPasswordError ? "red" : "#909090"}
															>
																<rect
																	x="3"
																	y="11"
																	width="18"
																	height="11"
																	rx="2"
																	ry="2"
																></rect>
																<path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
															</svg>
														</span>
														<input
															id="confirmPassword"
															value={confirmPassword}
															onFocus={() => setIsConfirmPasswordFocused(true)}
															onChange={(e) =>
																setConfirmPassword(
																	e.target
																		.value
																)
															}
															onBlur={() => validateConfirmPassword()}
															type={isShowConfirmPassword ? "text" : "password"}
															className={`w-full h-10 px-4 rounded-r ${confirmPasswordError ? "outline-red" : "outline"}`}
															name="confirmPassword"
															//required
															placeholder="Confirm Password"
															style={{
																borderLeft:
																	'1px solid #a0aec0',
															}}
														/>
														<Tooltip
															target="#confirmPassword"
															visible={confirmPasswordError && isConfirmPasswordFocused}
															closeOnOutsideClick={false}
														>
															<div className="text-red-500">{confirmPasswordErrorText}</div>
														</Tooltip>
														<span className="px-3 cursor-pointer" onClick={() => handleShowHideConfirmPasswordClick()}>
															<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24" fill="none" stroke="#909090"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle>{isShowConfirmPassword ? <line id="svg_4" y2="3.357021" x2="20.356156" y1="20.891263" x1="3.095886" stroke="#909090" fill="none" /> : ""}</svg>
														</span>
													</div>
												</div>
												<ReCaptcha />
												<div className="flex flex-col mt-4">
													<button type="submit"   id="btn-reset-password" className="bg-blue-500 hover:bg-blue-700 text-white text-sm font-semibold py-2 px-4 rounded sm:w-1/1 lg:w-3/4 mx-auto" style={{ backgroundColor: "#3db5e7" }}>
														SUBMIT & LOGIN →
                          </button>
												</div>
											
											</form>
										</div>
									</div>
								</div>

							</div>
						</div>

					</div>

				</div>

			</div>
		</div>

	);
}

export default ForgotPassword;