import React, { useState } from "react";
import user from "../../../assets/img/GuestX_VerifyEmailIllustrationTrans.png"
import logo from "../../../assets/img/logos/logo-primary-white.png"
import { LoginPage, EmailVerificationCofirmation } from "../../../pages"

const EmailConfirmation=({ email })=> {
  const [isLoginPage, setIsLoginPage] = useState(false);
  const handleSignInClick = () => setIsLoginPage(true);

  const [isEmailVerificationCofirmation, setIsEmailVerificationCofirmation] = useState(false);
  const handleResendClick = () => setIsEmailVerificationCofirmation(true);

  if (isLoginPage) {
    return (<LoginPage />);
  }
  else if (isEmailVerificationCofirmation) {
    return (<EmailVerificationCofirmation />);
  }
  else {
    return (
      <div className="auth-bg bg-blue-400 min-h-screen sm:h-auto lg:h-screen w-full">
        <div className="flex flex-col items-center flex-1 h-full justify-center px-4 py-4 sm:px-0">

          <div className="w-full auth-w-900 sm:w-3/4 lg:w-8/12 xl:w-7/12 sm:mx-0">

            <img className="mb-5 w-40" src={logo} alt="see-city-img" />
            <div className="auth-form flex rounded-lg shadow-lg w-full bg-indigo-100 sm:mx-0">

              <div className="max-w-sm sm:w-12/12 lg:w-8/12 rounded overflow-hidden m-auto">
                <img className="w-1/2 mx-auto mt-8" src={user} alt="Sunset in the mountains" />
                <div className="px-6 py-4">

                  <div className="flex flex-col w-full p-0">
                    <div className="flex flex-col flex-1 justify-center mb-4">
                      <h1 className="text-2xl text-center font-bold">Verify Your Email.</h1>
                      <div className="w-full mt-0">
                        <div id="error-message" className="alert alert-danger text-red-700">
                        </div>
                        <form className="form-horizontal w-11/12 mx-auto">
                          <div className="flex flex-col mt-4 text-center font-bold">
                            <div className="text-gray-500 text-xs">
                              Please take a moment to verify your email address by responding to the confirmation we just delivered to:
                            </div>
                            <div id="email" className="text-s mt-2" style={{ color: "#5cb2cc" }}>
                              {email}
                              {/* admission@guestx.com */}
                            </div>
                          </div>

                          <div className="flex flex-col mt-4">
                            <button onClick={(event) => handleSignInClick(event, false)} type="submit" id="btn-go-to-login" className="bg-blue-500 hover:bg-blue-700 text-white text-sm font-semibold py-2 px-4 rounded sm:w-1/1 lg:w-3/4 mx-auto" style={{ backgroundColor: "#3db5e7" }}>
                              GO TO LOGIN →
                            </button>
                          </div>

                          <div className="flex flex-col mt-4 text-center font-bold">
                            <div>
                              <span className="text-gray-500 text-xs">
                                Didn't receive an email?
                              </span>
                              <button id="resend">
                                <div onClick={(event) => handleResendClick(event, false)} className="text-xs font-bold ml-1 underline" style={{ color: "#5cb2cc" }}>
                                  Resend
                                </div>
                              </button>
                            </div>
                          </div>

                        </form>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

            </div>

          </div>

        </div>
      </div>

    );
  }
}

export default EmailConfirmation;