import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import cookie from 'react-cookies'
import { authenticationService } from '../../../services';
import { setUser } from '../../../store/actions';
import { RegisterPage, RecoverPasswordPage } from '../../../pages';
import logo from '../../../assets/img/logos/logo-primary-white.png';
import facebook_img from '../../../assets/img/Sign-up-with-Facebook.png';
import google_img from '../../../assets/img/Sign-up-with-Google.png';
import Loading from '../../../components/common/Loading';

import { useForm } from 'react-hook-form';
import { CheckBox } from 'devextreme-react';
import { Tooltip } from 'devextreme-react';
import LoadingV2 from '../../../components/common/LoadingV2';

const LoginPage = () => {
	const [isLoading, setIsLoading] = useState(false);
	const dispatch = useDispatch();
	const history = useHistory();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isValidEmail, setIsValidEmail] = useState(true);
	const [isRegister, setIsRegister] = useState(false);
	const [isForgotPassword, setIsForgotPassword] = useState(false);
	const [rememberMe, setRememberMe] = useState(undefined);

	const [emailError, setEmailError] = useState(false);
	const [passwordError, setPasswordError] = useState(false)
	const [isEmailFocused, setIsEmailFocused] = useState(false)
	const [isPasswordFocused, setIsPasswordFocused] = useState(false)

	const [isShowPassword, setIsShowPassword] = useState(false)
	const handleShowHidePasswordClick = () => setIsShowPassword(!isShowPassword);

	const handleForgotPasswordClick = () => setIsForgotPassword(true);
	const handleJoinNowClick = () => setIsRegister(true);

	const { handleSubmit } = useForm();

	useEffect(() => {
		var user = cookie.load('user')
		if (user !== undefined) {
			setEmail(user.email)
			setPassword(user.password)
			setRememberMe(true)
		}
		if (authenticationService.currentUserValue) {
			const role = authenticationService.currentUserValue.role;
			const route = `/${role.toLowerCase()}`;
			history.push(route);
		}
	}, []);

	// const validateEmail = () => {
	// 	var reg_email = /^((?!\.)[\w-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$/;
	// 	var val_email = reg_email.test(email);
	// 	if (!val_email) {
	// 		setIsValidEmail(false);
	// 	} else {
	// 		setIsValidEmail(true);
	// 	}
	// };

	const validateEmail = () => {
		setIsEmailFocused(false)
		if (email.length > 0) {
			setEmailError(false)
		}
		else {
			setEmailError(true)
		}
	}

	const validatePassword = () => {
		setIsPasswordFocused(false)
		if (password.length > 0) {
			setPasswordError(false)
		}
		else {
			setPasswordError(true)
		}
	}

	const loginFacebook = async () => {
		var resultFacebook = await authenticationService.loginFacebook();
	};

	const loginGoogle = async () => {
		var resultGoogle = await authenticationService.loginGoogle();
	};
	const login = async () => {
		setIsLoading(true);
		validateEmail()
		validatePassword()
		if (email.length > 0 && password.length > 0) {
			if (rememberMe) {
				cookie.save('user', { 'email': email, 'password': password }, { path: '/' })
			} else if (cookie.load('user') !== undefined) {
				cookie.remove('user', { path: '/' })
			}
			var result = await authenticationService.login(email, password);
			var errorMessage = document.getElementById("error-message");
			errorMessage.innerHTML = result.description;
			errorMessage.style.display = "block";
		}
		setIsLoading(false);
	};

	if (isRegister) {
		return <RegisterPage />;
	}
	if (isForgotPassword) {
		return <RecoverPasswordPage />;
	}
	return (
		<div className="auth-bg bg-blue-400 min-h-screen sm:h-auto lg:h-screen w-full">
			{
				isLoading ? (<LoadingV2 />) : (<></>)
			}
			<div className="flex flex-col items-center flex-1 h-full justify-center px-4 py-4 sm:px-0">
				<div className="w-full auth-w-900 sm:w-3/4 lg:w-8/12 xl:w-7/12 sm:mx-0">
					<img
						className="mb-5 w-40"
						src={logo}
						alt="see-city-img"
					/>
					<div className="auth-form login flex rounded-lg shadow-lg w-full bg-indigo-100 sm:mx-0">
						<div className="form-container max-w-sm sm:w-12/12 lg:w-8/12 rounded overflow-hidden">
							<div className="px-8 py-4">
								<div className="flex flex-col w-full py-5">
									<div className="flex flex-col flex-1 justify-center mb-4">
										<h1 className="text-2xl text-center font-bold">
											Welcome Back
                                            </h1>
										<div className="w-full mt-0">
											<div
												id="error-message"
												className="alert alert-danger text-red-700"
											></div>
											<form
												className="form-horizontal w-4/4 mx-auto"
												onSubmit={handleSubmit(
													login
												)}
											>
												<div className="flex flex-col mt-3">
													<div className={`flex items-center bg-white border ${emailError ? "border-2 border-red-500" : "border-gray-400"} shadow-md rounded`}>
														<span className="px-3">
															<svg
																xmlns="http://www.w3.org/2000/svg"
																width="25"
																height="25"
																viewBox="0 0 24 24"
																fill="none"
																stroke={emailError ? "red" : "#909090"}
															>
																<path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
																<polyline points="22,6 12,13 2,6"></polyline>
															</svg>
														</span>
														<input
															id="email"
															value={email}
															onChange={(e) =>
																setEmail(
																	e.target
																		.value
																)
															}
															onFocus={() => setIsEmailFocused(true)}
															onBlur={() =>
																validateEmail()
															}
															type="text" autoComplete="off"
															className={`w-full h-10 px-4 rounded-r ${emailError ? "outline-red" : "outline"}`}
															name="email"
															placeholder="Email"
															//required
															style={{
																borderLeft:
																	'1px solid #a0aec0',
															}}
														/>
														<Tooltip
															target="#email"
															visible={emailError && isEmailFocused}
															closeOnOutsideClick={false}
														>
															<div className="text-red-500">Email is required</div>
														</Tooltip>
													</div>
												</div>
												<div className="flex flex-col mt-3">
													<div className={`flex items-center bg-white border ${passwordError ? "border-2 border-red-500" : "border-gray-400"} shadow-md rounded`}>
														<span className="px-3">
															<svg
																xmlns="http://www.w3.org/2000/svg"
																width="25"
																height="25"
																viewBox="0 0 24 24"
																fill="none"
																stroke={passwordError ? "red" : "#909090"}
															>
																<rect
																	x="3"
																	y="11"
																	width="18"
																	height="11"
																	rx="2"
																	ry="2"
																></rect>
																<path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
															</svg>
														</span>
														<input
															id="password"
															value={password}
															onFocus={() => setIsPasswordFocused(true)}
															onChange={(e) =>
																setPassword(
																	e.target
																		.value
																)
															}
															onBlur={() => validatePassword()}
															type={isShowPassword ? "text" : "password"}
															className={`w-full h-10 px-4 rounded-r ${passwordError ? "outline-red" : "outline"}`}
															name="password"
															//required
															placeholder="Password"
															style={{
																borderLeft:
																	'1px solid #a0aec0',
															}}
														/>
														<Tooltip
															target="#password"
															visible={passwordError && isPasswordFocused}
															closeOnOutsideClick={false}
														>
															<div className="text-red-500">Password is required</div>
														</Tooltip>
														<span className="px-3 cursor-pointer" onClick={() => handleShowHidePasswordClick()}>
															<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24" fill="none" stroke="#909090"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle>{isShowPassword ? <line id="svg_4" y2="3.357021" x2="20.356156" y1="20.891263" x1="3.095886" stroke="#909090" fill="none" /> : ""}</svg>
														</span>
													</div>
												</div>
												<div className="flex items-left mt-4">
													<CheckBox
														text="Remember Me"
														defaultValue={rememberMe}
														value={rememberMe}
														onValueChanged={(e) => setRememberMe(e.value)}
													/>
												</div>
												<div className="flex flex-col mt-8">
													<div className="w-full">
														<button
															type="submit"
															id="btn-login"
															className="bg-blue-500 hover:bg-blue-700 text-white text-sm font-semibold py-2 px-4 rounded w-full"
															style={{
																backgroundColor:
																	'#3db5e7',
															}}
														>
															LOGIN →
                                                            </button>
													</div>
												</div>
											</form>
											<div className="w-full py-2">
												<div
													className="text-center text-gray-500 leading-none my-2"
													style={{
														borderBottom:
															'1px solid #a0aec0',
														lineHeight: '0.3em',
													}}
												>
													<span
														className="px-3"
														style={{
															background:
																'#f6fbfe',
														}}
													>
														or
                                                        </span>
												</div>
											</div>
											<div className="flex flex-col md:flex-row w-full mt-2">
												<button onClick={() =>
													loginGoogle()
												}
													className="w-full md:w-1/2 text-xs m-1"
													id="btn-google"
												>
													<img
														src={google_img}
														className="h-10 w-full md:h-8"
													></img>
												</button>
												<button onClick={() =>
													loginFacebook()
												}
													className="w-full md:w-1/2 text-xs m-1"
													id="btn-facebook"
												>
													<img
														src={facebook_img}
														className="h-10 w-full md:h-8"
													></img>
												</button>
											</div>
											<div className="w-full py-2">
												<div
													className="text-center text-gray-500 leading-none my-2"
													style={{
														borderBottom:
															'1px solid #a0aec0',
														lineHeight: '0.3em',
													}}
												>
													<span
														className="px-3"
														style={{
															background:
																'#f6fbfe',
														}}
													>
														or
                                                        </span>
												</div>
											</div>
											<div className="w-full py-2">
												<button
													onClick={(e) =>
														handleForgotPasswordClick(
															e,
															false
														)
													}
													className="text-center font-bold w-full"
													style={{
														color: '#5cb2cc',
													}}
												>
													Forgot password?
                                                    </button>
											</div>
											<div className="flex flex-col mt-4 text-center font-bold">
												<div>
													<span className="text-gray-500 text-xs">
														New to See.City?
                                                        </span>
													<button
														id="btn-join-now"
														onClick={(e) =>
															handleJoinNowClick(
																e,
																false
															)
														}
													>
														<div
															className="text-xs font-bold ml-1 underline"
															style={{
																color:
																	'#5cb2cc',
															}}
														>
															Join Now
                                                            </div>
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default LoginPage;
