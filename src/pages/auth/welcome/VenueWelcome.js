import React, { useState, useEffect } from 'react';
import logo from '../../../assets/img/logos/logo-primary-white.png';
import logo_color from '../../../assets/img/logos/logo-primary-full-color-tm.png';
import { LoginPage, RegisterPage } from '../../../pages';
import { authenticationService } from '../../../services';
import { Redirect } from 'react-router-dom';
import { PrivateRoute } from '../../../components/router/PrivateRoute';

const VenueWelcomeScreen = () => {
	useEffect(() => {
		const currentUser = authenticationService.currentUserValue;
		if (currentUser) {
			setIsCurrentUser(true);
		}
	});

	const [isCurrentUser, setIsCurrentUser] = useState(false);

	const [isLoginPage, setIsLoginPage] = useState(false);
	const handleSignInClick = () => setIsLoginPage(true);

	const [isRegisterPage, setIsRegisterPage] = useState(false);
	const handleJoinNowClick = () => setIsRegisterPage(true);

	if (isCurrentUser) {
		return <PrivateRoute />;
	}
	if (isLoginPage) {
		return <LoginPage />;
	}
	if (isRegisterPage) {
		return <RegisterPage />;
	}
	return (
		<div className="auth-bg bg-blue-400 min-h-screen sm:h-auto lg:h-screen w-full">
			<div className="flex flex-col items-center flex-1 h-full justify-center px-4 py-4 sm:px-0">
				<div className="w-full auth-w-900 sm:w-3/4 lg:w-8/12 xl:w-7/12 sm:mx-0">
					<img className="mb-5 w-40" src={logo} alt="see-city-img" />
					<div className="auth-form form-img flex rounded-lg shadow-lg w-full bg-indigo-100 sm:mx-0">
						<div className="form-container max-w-sm sm:w-12/12 lg:w-8/12 rounded overflow-hidden">
							<div className="px-8 py-10">
								<div className="flex flex-col w-full p-0">
									<div className="flex flex-col flex-1 justify-center mb-4">
										<h1 className="text-2xl text-center font-bold w-full">
											Welcome
                                        </h1>
										<h1 className="text-2xl text-center font-bold w-full">
											to
                                        </h1>
										<img
											className="mb-5 w-3/4 mx-auto"
											src={logo_color}
											alt="see-city-img"
										/>
									</div>

									<div className="flex flex-col flex-1 justify-center mb-4 mt-4">
										<div className="w-full py-2">
											<button
												onClick={(event) =>
													handleSignInClick(
														event,
														false
													)
												}
												type="submit"
												id="btn-sign-in"
												className="bg-blue-500 hover:bg-blue-700 text-white text-sm font-semibold py-2 px-4 rounded w-full"
												style={{
													backgroundColor: '#3db5e7',
												}}
											>
												SIGN IN →
                                            </button>
										</div>
										<div className="w-full py-2">
											<div
												className="text-center text-gray-500 leading-none my-2"
												style={{
													borderBottom:
														'1px solid #a0aec0',
													lineHeight: '0.3em',
												}}
											>
												<span
													className="px-3"
													style={{
														background: '#f6fbfe',
													}}
												>
													or
                                                </span>
											</div>
										</div>
										<div className="w-full py-2">
											<button
												onClick={(event) =>
													handleJoinNowClick(
														event,
														false
													)
												}
												type="submit"
												id="btn-join-now"
												className="bg-white border border-blue-700 hover:bg-white-500 text-blue-500 text-sm font-semibold py-2 px-4 rounded w-full"
												style={{
													borderColor: '#3db5e7',
													color: '#0692d4',
												}}
											>
												JOIN NOW →
                                            </button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default VenueWelcomeScreen;
