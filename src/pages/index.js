export { default as MarketListPage } from './market-list';
export { default as AccountListPage } from './account-list';
export { default as AccountDashboardPage } from './account/dashboard/Dashboard';
export { default as AccountOperationsPage } from './account/operations/Operations';
export { default as AccountProfilePage } from './account/profile/Profile';
export { default as AccountProductsPage } from './account/products/Products';
export { default as AccountRedeemPage } from './account/redeem/Redeem';
export { default as AccountSellPage } from './account/sell/Sell';
export { default as AccountSetupPage } from './account/setup/Setup';
export { default as CreateUser } from './create-user';
export { default as LookupsPage } from './lookups/Lookups';

// TODO: Refactor Product Wizard Pages

export { default as ProductWizard } from './product/product-wizard/ProductWizard';
export { default as Page1 } from './product/product-wizard/pages/Page1';
export { default as Page2 } from './product/product-wizard/pages/Page2';
export { default as Page3 } from './product/product-wizard/pages/Page3';
export { default as Page4 } from './product/product-wizard/pages/Page4';

export { AccountPage } from './account';

export {
    EmailConfirmationPage,
    LoginPage,
    RecoverPasswordPage,
    RegisterPage,
    ResetPasswordPage,
    AccountRecoveryMessage,
    VenueWelcomeScreen,
    EmailVerificationCofirmation,
} from './auth';
