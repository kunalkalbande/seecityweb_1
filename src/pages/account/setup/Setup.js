import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { setUser } from '../../../store/actions';
import ProfileDetails from '../components/Profile';
import Tags from '../components/Tags';
import OperatingDetails from '../components/OperatingHours';
import SellingMode from '../components/SellingMode';
import Address from '../components/Address';
import PublicPhone from '../components/PublicPhone';
import LogoDetail from '../components/LogoDetails';
import Brand from '../components/Brand';
import BankingDetails from '../components/BankingDetails';
import { accountSetup } from '../../../api/graphql/accountSetup';
import Loading from '../../../components/common/Loading';
import operatingPeriods from '../../../data/operating-periods';
import { BehaviorSubject } from 'rxjs';
import { Button } from 'devextreme-react';
import { authenticationService } from '../../../services/authentication.service';

const currentUserSubject = new BehaviorSubject(
    JSON.parse(localStorage.getItem('currentUser'))
);
const defaultPeriod = operatingPeriods.complex.filter(
    (el) => el.year === '2020' && el.name === 'Calendar Year'
);
const AccountSetup = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const [setupDetail, setDetail] = useState('');
    const currentUser = useSelector((state) => state.user);
    const [isLoading, setIsLoading] = useState(true);
    const [isConfirm, setIsConfirm] = useState(false);
    const [adminRole, setAdminRole] = useState(['rol_n14cpU57YrHEK9Dg']);
    const [setupRole, setSetupRole] = useState(['rol_DlvXJ1UyLs1N6Uv6']);
    const [btnText, setBtnText] = useState('Save');

    var logoImage = [];
    var setupObj = {};
    var phone = {};
    useEffect(() => {
        const fetchData = async () => {
            await fetchSetup();
        };
        fetchData();
    }, []);
    if (isConfirm) {
        const role = currentUser.role;
        const route = '/login';
        history.push(route);
    }

    const getProfileChange = (account) => {
        console.log('acc', setupObj);
        setupObj.companyName = account.companyName;
        setupObj.venueName = account.venueName;
        setupObj.accountTypeId = account.accountTypeId;
    };
    const getPhoneChange = (account) => {
        phone.isSameAddress = account.isSameAddress;
        phone.phoneNumber = account.phoneNumber;
        if (
            account != null &&
            account != undefined &&
            account.addressOne != undefined &&
            account.addressOne.length > 0
        ) {
            phone.addressTwo = account.addressTwo;
        }
    };
    const getOPChange = (account) => {
        setupObj.operatingPolicy = account;
    };

    const getAddressChange = (account) => {
        var address = {};
        address.addressOne = account.addressOne;
        address.addressTwo = account.addressTwo;
        address.city = account.city;
        address.stateId = account.stateId;
        address.zip = account.zip;
        setupObj.address = address;
    };
    const getLogoChange = (logo) => {
        console.log('logo', logo);
        logoImage = logo;
    };

    const getBankDetailChange = (account) => {
        var bankDetail = {};
        bankDetail.routingNumber = account.routingNumber;
        bankDetail.accountNumber = account.accountNumber;
        bankDetail.bankAccountType = account.bankAccountType;
        setupObj.bankdetail = bankDetail;
    };
    const getTagsChange = (account) => {
        setupObj.tags = account;
    };
    const getBrandChange = (account) => {
        setupObj.brandDetail = account;
    };
    const getSellingModeChange = (account) => {
        setupObj.isProviderSeller = account.isProviderSeller;
        setupObj.isSeller = account.isSeller;
    };

    const assignAndDelUserRole = () => {
        return new Promise((resolve) => {
            setTimeout(async () => {
                await authenticationService.getManagementAccessToken();

                var id = sessionStorage.getItem('id');
                var token = sessionStorage.getItem('Token');
                const addRoleRequest = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        Authorization: 'Bearer ' + token,
                    },
                    body: JSON.stringify({
                        roles: adminRole,
                    }),
                };

                await fetch(
                    `https://seecity.auth0.com/api/v2/users/${id}/roles`,
                    addRoleRequest
                ).then((data) => {
                    console.log('Add role respobse = ' + data);
                });

                const deleteRoleRequest = {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        Authorization: 'Bearer ' + token,
                    },
                    body: JSON.stringify({
                        roles: setupRole,
                    }),
                };

                await fetch(
                    `https://seecity.auth0.com/api/v2/users/${id}/roles`,
                    deleteRoleRequest
                ).then((data) => {
                    resolve(data);
                });
            }, 1000);
        });
    };
    const saveApi = (setupObj) => {
        console.log(setupObj);
        return new Promise((resolve, error) => {
            setTimeout(async () => {
                let res;
                if (btnText === 'Save') {
                    res = await accountSetup.createAccount(setupObj, logoImage);
                    if (
                        res.createAccount != undefined &&
                        res.createAccount != null
                    ) {
                        currentUser.bag = { accountId: res.createAccount._id };

                        var myRoles = currentUser.roles;
                        var index = myRoles.indexOf('Setup');
                        if (index > -1) {
                            myRoles.splice(index, 1);
                        }
                        myRoles.push('MemberAdmin');

                        currentUser.roles = myRoles;

                        localStorage.setItem(
                            'currentUser',
                            JSON.stringify(currentUser)
                        );
                        currentUserSubject.next(currentUser);
                        resolve(setupObj);
                    } else {
                        setIsLoading(false);
                        alert('Error');
                    }
                } else {
                    res = await accountSetup.updateAccount(setupObj, logoImage);
                    if (
                        res.updateAccount != undefined &&
                        res.updateAccount != null
                    ) {
                        resolve(setupObj);
                    } else {
                        setIsLoading(false);
                        alert('Error');
                    }
                }
                console.log(res);

                console.log('res   =' + res);
            }, 1000);
        });
    };
    const save = async (e) => {
        e.preventDefault();

        setupObj.userId = currentUser.userId;
        setupObj.address.phoneNumber = phone.phoneNumber;
        setupObj.address.isSameAddress = phone.isSameAddress;
        setupObj.addressTwo = phone.addressTwo;
        setIsLoading(true);
        if (btnText === 'Update') {
            setupObj._id = setupDetail._id;
            if (setupObj.accountTypeId instanceof Object) {
                setupObj.accountTypeId = setupDetail.accountTypeId;
            }
            if (setupObj.address.stateId instanceof Object) {
                setupObj.address.stateId = setupDetail.address.stateId;
            }
            setupObj.changeType = 'Account Adjustment';
            setupObj.status = 'pending';
            await saveApi(setupObj);
        } else {
            setupObj.operatingPeriods = defaultPeriod;
            setupObj.changeType = 'Initial Submision';
            setupObj.status = 'pending';
            await saveApi(setupObj);
            await assignAndDelUserRole();
        }

        console.log('before saveApi' + setupObj);
        setIsLoading(false);
        setIsConfirm(true);
    };

    const fetchSetup = async () => {
        if (
            currentUser.bag != undefined &&
            currentUser.bag.accountId != undefined
        ) {
            var accountId = currentUser.bag.accountId;
            setIsLoading(true);
            const res = await fetchApi(accountId);
            if (res) {
                setBtnText('Update');
            }
            console.log(res);
            setDetail(res);
            setIsLoading(false);
        } else {
            setDetail(null);
            setIsLoading(false);
        }
    };

    const fetchApi = (accountId) => {
        return new Promise((resolve) => {
            setTimeout(async () => {
                var res = await accountSetup.getAccountSetupDetails(accountId);
                console.log(res);
                resolve(res.accountById);
            }, 500);
        });
    };

    return (
        <div className="mx-2 flex flex-col overflow-y-hidden">
            {!isLoading ? (
                <form onSubmit={save}>
                    <div className="flex flex-wrap">
                        <div className="w-full sm:w-1/2 sm:pr-1">
                            <div className="my-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6">
                                    <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                        Profile Details
                                    </h3>
                                </div>
                                <div className="my-3 mx-3 text-sm leading-5 ">
                                    <div className="flex flex-wrap">
                                        <ProfileDetails
                                            onChange={(value) =>
                                                getProfileChange(value)
                                            }
                                            setupDetail={setupDetail}
                                        />
                                        <Address
                                            isFromPhone={false}
                                            onChange={(value) =>
                                                getAddressChange(value)
                                            }
                                            setupDetail={setupDetail}
                                        />
                                        <PublicPhone
                                            onChange={(value) => {
                                                getPhoneChange(value);
                                            }}
                                            setupDetail={setupDetail}
                                        />

                                        {/* [Form goes here] */}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="sm:w-1/2 sm:pl-1">
                            <div className="my-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6">
                                    <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                        OKCZoo.OKC.see.city
                                    </h3>
                                </div>
                                <div className="my-3 mx-3 sm:pb-14 text-sm leading-5">
                                    {/* [Form goes here] */}

                                    <LogoDetail
                                        onChange={(value) => {
                                            getLogoChange(value);
                                        }}
                                        setupDetail={setupDetail}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="flex flex-wrap">
                        <div className="w-full sm:w-1/2 sm:pr-1">
                            <div className="my-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6">
                                    <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                        Operating Days/Hours
                                    </h3>
                                </div>
                                <div className="my-3 mx-3 text-sm leading-5 ">
                                    <OperatingDetails
                                        onChange={(value) => getOPChange(value)}
                                        setupDetail={setupDetail}
                                    />
                                    {/* [Form goes here] */}
                                </div>
                            </div>
                        </div>

                        <div className="sm:w-1/2 sm:pl-1">
                            <div className="my-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6">
                                    <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                        My Business Tags
                                    </h3>
                                </div>
                                <div className="my-3 mx-3 sm:pb-9 text-sm leading-5">
                                    {/* [Form goes here] */}
                                    <Tags
                                        onChange={(value) =>
                                            getTagsChange(value)
                                        }
                                        setupDetail={setupDetail}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="flex flex-wrap">
                        <div className="sm:w-1/2 sm:pr-1">
                            <div className="my-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6">
                                    <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                        Link Bank Account
                                    </h3>
                                </div>
                                <div className="my-3 mx-3 text-sm leading-5 text-gray-500">
                                    <BankingDetails
                                        onChange={(value) =>
                                            getBankDetailChange(value)
                                        }
                                        setupDetail={setupDetail}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="sm:w-1/2 sm:pl-1">
                            <div className="my-2 mb-4 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6">
                                    <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                        Brand Details
                                    </h3>
                                </div>
                                <div className="my-3 mx-3 text-sm leading-5 ">
                                    {/* [Form goes here] */}
                                    <Brand
                                        onChange={(value) =>
                                            getBrandChange(value)
                                        }
                                        setupDetail={setupDetail}
                                    />
                                </div>
                            </div>

                            <div className="my-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6">
                                    <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                        My Selling Mode
                                    </h3>
                                </div>
                                <div className="my-3 mx-3 text-sm leading-5 ">
                                    {/* [Form goes here] */}
                                    <SellingMode
                                        onChange={(value) =>
                                            getSellingModeChange(value)
                                        }
                                        setupDetail={setupDetail}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="flex justify-end my-2">
                        <div>
                            <Button
                                className="mx-2 px-4 py-2 border border-sc-blue-200 text-xs font-medium rounded sm:rounded text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-200 active:bg-sc-blue-200 transition duration-150 ease-in-out"
                                id="button"
                                text={btnText}
                                useSubmitBehavior={true}
                            />
                        </div>
                    </div>
                </form>
            ) : (
                <Loading />
            )}
        </div>
    );
};

export default AccountSetup;
