import React, { useState } from 'react';
import email from '../../../assets/img/email-img.jpg';
import logo from '../../../assets/img/logos/logo-primary-white.png';
import { AccountDashboardPage } from '../..';
import { LoginPage } from '../../auth';

const AccountConfirmation = () => {
    const [isConfirm, setIsConfirm] = useState(false);

    const handleGoToDhashboardClick = () => {
        setIsConfirm(true);
    };

    return (
        <div className="auth-bg bg-blue-400 min-h-screen sm:h-auto lg:h-screen w-full">
            <div className="flex flex-col items-center flex-1 h-full justify-center px-4 py-4 sm:px-0">
                <div className="w-full auth-w-900 sm:w-3/4 lg:w-8/12 xl:w-7/12 sm:mx-0">
                    <img className="mb-5 w-40" src={logo} alt="see-city-img" />
                    <div className="auth-form flex rounded-lg shadow-lg w-full bg-indigo-100 sm:mx-0">
                        <div className="max-w-sm sm:w-12/12 lg:w-8/12 rounded overflow-hidden m-auto">
                            <img
                                className="w-3/4 mx-auto mt-8"
                                src={email}
                                alt="Sunset in the mountains"
                            />
                            <div className="px-6 py-4">
                                <div className="flex flex-col w-full p-0 py-">
                                    <div className="flex flex-col flex-1 justify-center mb-4">
                                        <h1 className="text-2xl text-center font-bold">
                                            You're Ready To Go!
                                        </h1>
                                        <h9 className="text-center">
                                            Congratulations! You are now
                                            officially registered to do
                                            business!
                                        </h9>
                                        <h9 className="text-center">
                                            Visit your product dashboard to
                                            setup services.
                                        </h9>
                                        <div className="w-full mt-0">
                                            <div
                                                id="error-message"
                                                className="alert alert-danger text-red-700"
                                            ></div>
                                            <form className="form-horizontal w-11/12 mx-auto">
                                                <div className="flex flex-col mt-4">
                                                    <button
                                                        // onClick={(event) =>
                                                        //     handleGoToDhashboardClick(
                                                        //         event,
                                                        //         false
                                                        //     )
                                                        // }
                                                        type="submit"
                                                        id="btn-go-to-login"
                                                        className="bg-blue-500 hover:bg-blue-700 text-white text-sm font-semibold py-2 px-4 rounded sm:w-1/1 lg:w-3/4 mx-auto"
                                                        style={{
                                                            backgroundColor:
                                                                '#3db5e7',
                                                        }}
                                                    >
                                                        GO TO DASHBOARD →
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AccountConfirmation;
