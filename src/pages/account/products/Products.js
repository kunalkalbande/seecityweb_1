import React, { useState, useEffect } from 'react';
import productData from '../../../data/product.json';
import Loading from '../../../components/common/Loading';
import { product } from '../../../api/graphql/product';
import { useDispatch, useSelector } from 'react-redux';
import { ProductsList,SelectedProductDetails,ActiveProductsGrid,ProductBrands,ProductTabList} from './components'
import { useHistory } from 'react-router-dom';
import { Button } from 'devextreme-react';

const ProductsPage = () => {
    const history = useHistory();
    const [isLoading, setIsLoading] = useState(false);
    const [products, setProducts] = useState('');
    const currentUser = useSelector((state) => state.user);
    const[isSelected,SetIsSelected]=useState(false);
    const[selectedProduct,SetSelectedProduct]=useState();
    const [isEditMode,setIsEditMode]=useState(false);
    const [isImageEditMode,setIsImageEditMode]=useState(false);
    const [isSetup,setIsSetup]=useState(false);
    useEffect(() => {
        const fetchData = async () => {
            await fetch();
        };
        fetchData();
    }, []);
const setSelctedValue=(value)=>{
if(value)
{
    console.log(value);
    console.log("status===",value.status)

   
    console.log("isSetup",isSetup);
    SetIsSelected(true);
    SetSelectedProduct(value);
    if(value.status==="setup")
    {
        localStorage.setItem('selectedProductId', value._id);
        console.log("product",localStorage);
        const route = '/member/pw';
        history.push(route);
    }
}
}
const newValue=()=>{
            const route = '/member/pw';
            history.push(route);
    }
    const backClick=async()=>{
        SetSelectedProduct(null);
        SetIsSelected(false);
    }
    const save = async () => {
        
        setIsLoading(true);
        await saveApi(productData.product);
        setIsLoading(false);
    };

    const fetch = async () => {
        if (
            currentUser.bag != undefined &&
            currentUser.bag.accountId != undefined
        ) {
            var accountId = currentUser.bag.accountId;
            setIsLoading(true);
            const res = await fetchApi(accountId);
            setProducts(res);
            setIsLoading(false);
        } else {
             setIsLoading(false);
             setProducts(null);
        }
    };

    const fetchApi = (accountId) => {
        return new Promise((resolve) => {
            setTimeout(async () => {
                var res = await product.getProductsByAccountId(accountId);
                console.log(res.productMany);
                resolve(res.productMany);
            }, 500);
        });
    };
const onChange=()=>{
    setIsEditMode(false);
    setIsImageEditMode(false);
}
    const columns = [
        { name: 'ProductId', title: 'ID' },
        { name: 'ProductName', title: 'ProductName' },
        { name: 'owner', title: 'Owner' },
      ];
      const rows = [
        { _id: 0, ProductName: 'DevExtreme', owner: 'DevExpress' },
        { _id: 1, ProductName: 'DevExtreme Reactive', owner: 'DevExpress' },
      ];

    const saveApi = (productObj) => {
        return new Promise((resolve, error) => {
            setTimeout(async () => {
                var res = await product.saveProduct(productObj);
                if (res.saveProduct != undefined && res.saveProduct != null) {
                    resolve(productObj);
                    alert('Product has been saved successfully.');
                } else {
                    setIsLoading(false);
                    alert('Error');
                }
                console.log('res   =' + res);
            }, 1000);
        });
    };
    return (
        <div className="mx-2 flex flex-col overflow-y-hidden">
        {!isLoading ? (!isSelected?(
            <div className="w-full sm:pr-1">
                <div>
            <div className="my-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                              
                                  
            <div className="bg-white  flex w-full px-4  py-5 border-b border-gray-200  flex-row item-right">     
                  <h3 className="text-lg leading-6 font-medium items-start text-sc-blue-500 w-4/5">
                                       Products
                                    </h3> 
                                    {/* </div>
                                    <div className="bg-white mt-2 px-2 py-4 border-b border-gray-200 sm:px-4 flex flex-col items-end"> */}
                                    <div className="flex flex-col items-end  text-sm  text-right w-1/5">
                                    {/* <a style={{marginRight:'10px',textDecoration:'underLine',color:'#337AB7'}}
                                     onClick={newValue} href="#">New</a> */}
                                     <Button
                        className="w-50 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                       onClick={newValue}
                        text="Add Product"
                    />
                                     </div></div>
                                  
                               
                                <div className="my-3 mx-3 text-sm leading-5 ">
                                <ProductsList products={products} onselect={(value)=>setSelctedValue(value)}/>
                                    {/* [Form goes here] */}
                                </div>
                                </div>
            </div>

<div className="my-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
<div className="bg-white  flex w-full px-4 py-5 border-b border-gray-200  flex-row item-right">
    <h3 className="text-lg leading-6 font-medium  text-sc-blue-500">
       Active Products
    </h3>
</div>
<div className="my-3 mx-3 text-sm leading-5 ">
<ActiveProductsGrid products={products} onselect={(value)=>setSelctedValue(value)}/>
    {/* [Form goes here] */}
</div>
</div>
</div>

             ):( !isSetup?
           (   <div className="text-right"><a style={{marginRight:'10px',textDecoration:'underLine'}} onClick={backClick} href="#">Back</a><a style={{marginRight:'10px',textDecoration:'underLine'}} href="#">Submit For Review</a>
           <div className="flex flex-wrap">
              
           <div className="w-full sm:w-1/2 sm:pr-1">
            <div className="my-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white  flex w-full px-4 py-5 border-b border-gray-200  flex-row">
                                    <h3 className="text-lg leading-6 font-medium  text-sc-blue-500">
                                       Products Detail
                                    </h3>
                                    <div className="text-right ml-auto">
                                        {!isEditMode ? (
                                            <a
                                                href="#"
                                                style={{
                                                    marginLeft:'10px',
                                                    alignContent: 'right',
                                                    textDecoration:'underLine'
                                                }}
                                                onClick={() =>
                                                    setIsEditMode(true)
                                                }
                                            >
                                                Edit
                                            </a>
                                        ) : (
                                            <div />
                                        )}
                                    </div>
                                    
                                </div>
                                <div className="my-3 mx-3 text-sm leading-5 ">
                                <SelectedProductDetails onChange={onChange} isEditMode={isEditMode} selectedProduct={selectedProduct} />
                                    {/* [Form goes here] */}
                                </div>
                                </div>
                                </div>
                                <div className="w-full sm:w-1/2 sm:pr-1">
                                <div className="my-2  flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white  flex w-full px-4 py-5 border-b border-gray-200  flex-row item-right">
                                    <h3 className="text-lg leading-6 font-medium  text-sc-blue-500">
                                       Products Brand
                                    </h3>
                                    <div className="text-right ml-auto">
                                        {!isImageEditMode ? (
                                            <a
                                                href="#"
                                                style={{
                                                    marginLeft:'10px',
                                                    alignContent: 'right',
                                                    textDecoration:'underLine'
                                                }}
                                                onClick={() =>
                                                    setIsImageEditMode(true)
                                                }
                                            >
                                                Edit
                                            </a>
                                        ) : (
                                            <div />
                                        )}
                                    </div>
                                    
                                </div>
                                <div className="my-3 mx-3 text-sm leading-5 ">
                                <ProductBrands onChange={onChange} isEditMode={isImageEditMode} selectedProduct={selectedProduct}/>
                                
                                    {/* [Form goes here] */}
                                </div>
                                </div>
                                </div>
                                <div className="w-full sm:w-full sm:pr-1">
            <div className="my-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                
                                    <ProductTabList className="w-full ml-10 mr-10" selectedProduct={selectedProduct}/>
                                </div>
                                </div>
            </div></div>):(<div/>) )): (<Loading/>)}
            </div>
    );
};

export default ProductsPage;
