/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import Loading from '../../../../components/common/Loading';
import { useSelector } from 'react-redux';
import { Button } from 'devextreme-react';

const ActiveProductGrid = ({ products,onselect }) => {
    const [activeProducts, setActiveProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [selectedProduct, setSelectedProduct] = useState();

    const currentUser = useSelector((state) => state.user);

    useEffect(() => {
        const init = async () => {
            setIsLoading(true);
            if (products.length > 0) {
                const active = products.filter((p) => p.isActive);
                setActiveProducts(active);
            }
            //setActiveProducts(products);
            setIsLoading(false);
        }
        init();
    }, []);
    useEffect(() => {
        if(selectedProduct)
        onselect(selectedProduct)
    }, [selectedProduct]);
    const viewProduct = (product) => {
        console.log("View Product -> ", product);
        setSelectedProduct(product);
       // alert("View Product");
    }

    return (
        <>
            {isLoading ? (<Loading />) : (
                <div className="mx-2 my-2 sm:px-3 h-full">
                    <div className="grid grid-cols-1 row-gap-3 col-gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3">
                        {activeProducts.map((product, index) => (
                            <div key={index}>
                                <div className="my-2 flex flex-col items-center border">
                                    <img
                                        className="h-40 w-full"
                                        //src={product._id==='5f0d52edf067de479cdbe031'?'/' + product.file[0]?.path:'https://hungry-bohr-5f31a3.netlify.app/static/media/Blue_Green_Experiences_Outdoors.5060724c.png'}
                                        src={'https://hungry-bohr-5f31a3.netlify.app/static/media/Blue_Green_Experiences_Outdoors.5060724c.png'}
                                        alt="see.city"
                                    />
                                    <div className="px-2 w-full border-b border-t">
                                        <h1 className="w-full mx-auto pt-2 h-10 text-2xl font-bold text-sc-blue-400">
                                            {/* {product.name + ' '} */}
                                            {'Attraction'}
                                        </h1>
                                    </div>
                                    <div className="px-2 w-full">
                                        <h1 className="w-full mx-auto pt-2 h-10 text-2xl font-bold text-sc-blue-400">
                                            {product.name + ' '}
                                        </h1>
                                        <div className="w-full mx-auto h-12 pt-3 text-sm text-sc-blue-400">
                                            {product.shortDescription + ' '}
                                        </div>
                                    </div>
                                    <div className="w-full p-2">
                                        <Button
                                            className="float-right border border-sc-blue-200 text-xs font-medium rounded sm:rounded text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-200 active:bg-sc-blue-200 transition duration-150 ease-in-out"
                                            text="View"
                                            type="default"
                                            //stylingMode="contained"
                                            stylingMode="outlined"
                                            onClick={() => viewProduct(product)}
                                        />
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            )}
        </>
    );
};

export { ActiveProductGrid };