import React, { useState ,useEffect} from 'react';
import { Button,TextBox, CheckBox, DateBox, FileUploader, TextArea } from 'devextreme-react';
import{product} from '../../../../api/graphql/product'
    import { from } from 'apollo-link';
export const SelectedProductDetail=({selectedProduct,isEditMode,onChange})=>{
    console.log(selectedProduct);
    const[isEdit,setIsEdit]=useState(true);
    const[name,setname]=useState(selectedProduct.name);
    const[shortDescription,setShortDesc]=useState(selectedProduct.shortDescription);
    const[longDescription,setLongDesc]=useState(selectedProduct.longDescription);
    const[termsAndConditions,setTermsAndCondition]=useState(selectedProduct.termsAndConditions);
    const[isActive,setIsActive]=useState(selectedProduct.isActive);
    const[endDate,setEndDate]=useState(selectedProduct.endDate);
    const[advanceSaleEndDate,setAdvanceSaleEndDate]=useState(selectedProduct.advanceSaleEndDate);
    const[logo,setLogo]=useState([]);
    
    const OnImageUpload= (args) => {
        document.getElementById('img1').src = URL.createObjectURL(
            args.value[0]
        );
        setLogo(args.value)
    };
   
    useEffect(()=>{
        console.log("isEdit",isEdit)
onChange();
    },[isEdit])
    const save=async()=>{
selectedProduct.name=name;
selectedProduct.shortDescription=shortDescription;
selectedProduct.longDescription=longDescription;
selectedProduct.termsAndConditions=termsAndConditions;
selectedProduct.isActive=isActive;
selectedProduct.endDate=endDate;
selectedProduct.advanceSaleEndDate=advanceSaleEndDate;
console.log("updated",selectedProduct);
console.log(logo);
var res = await product.updateProduct(selectedProduct, logo);
console.log(res.updateProduct.file[0].path);
selectedProduct.file[0].path=res.updateProduct.file[0].path;
setIsEdit(!isEdit);
    }
    console.log(selectedProduct);
    return (
        <div className="flex flex-col">
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label style={{textAlign:'left'}} class="block text-sm font-medium leading-5 text-gray-700 mt-2 sm:col-span-2">
                    Ticket Name
                </label>
                <div class="sm:mt-0 sm:col-span-3 mt-1 ml-2">
                    { isEditMode?<TextBox value={ name } 
                    onValueChanged={(e)=>{ setname(e.value) }}
                        />:
                    <TextBox value={selectedProduct.name } style={{ border: "0px" }} readOnly={true}
                        />}
                </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label style={{ textAlign:'left'}} class="block text-sm font-medium leading-5 text-gray-700 mt-2 sm:col-span-2">
                    Short Description
                </label>
                <div class="sm:mt-0 sm:col-span-3 mt-1 ml-2">
                { isEditMode?<TextBox  value={shortDescription}
                      onValueChanged={(e)=>{setShortDesc(e.value)}}
                      />:<TextBox style={{ border: "0px" }} value={selectedProduct.shortDescription} readOnly={true}
                      
                        />}
                </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label style={{ textAlign:'left'}} class="block text-sm font-medium sm:col-span-2 leading-5 text-gray-700 ">
               Long Description
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-3 ml-2">
                { isEditMode?<TextBox  value={longDescription} 
                      onValueChanged={(e)=>{setLongDesc(e.value)}}
                      />: <TextBox style={{ border: "0px" }} value={selectedProduct.longDescription} readOnly={true}
                      
                        />}
                </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label style={{ textAlign:'left'}} class="block text-sm font-medium leading-5 text-gray-700 mt-2 sm:col-span-2">
                    Terms &amp; Condition
                </label>
                <div class="sm:mt-0 sm:col-span-3 mt-1 ml-2">
                { isEditMode?<TextArea value={termsAndConditions} 
                      onValueChanged={(e)=>{setTermsAndCondition(e.value)}}
                      />
                :<TextArea style={{ border: "0px" }} value={selectedProduct.termsAndConditions} readOnly={true}
                      
                        />}
                </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label style={{ textAlign:'left'}} class="text-sm font-medium sm:col-span-2 leading-5 text-gray-700 ">
              Is Active
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-3 flex ml-2 align-start">
                    <CheckBox  value={isActive} readOnly={!isEditMode} style={{ alignSelf:'left'}}
                      onValueChanged={(e)=>{setIsActive(e.value)}}
                        />
                </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label style={{ textAlign:'left'}} class="block text-sm font-medium leading-5 text-gray-700 mt-2 sm:col-span-2">
                    Expiration Date
                </label>
                <div class="sm:mt-0 sm:col-span-3 mt-1 ml-2">
                { isEditMode?<DateBox  value={endDate}
                      onValueChanged={(e)=>{setEndDate(e.value)}}
                        />:<DateBox style={{ border: "0px" }} value={selectedProduct.endDate} readOnly={true}
                      
                        />}
                </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label style={{ textAlign:'left',alignSelf:'left'}} class="block text-sm font-medium leading-5 text-gray-700 mt-2 sm:col-span-2">
                  Advance Sale
                </label>
                <div class="sm:mt-0 sm:col-span-3 mt-1 ml-2">
                { isEditMode?<DateBox  value={advanceSaleEndDate} onValueChanged={(e)=>{setAdvanceSaleEndDate(e.value)}}
                      
                        />:<DateBox style={{ border: "0px" }} value={selectedProduct.advanceSaleEndDate} readOnly={true}
                      
                        />}
                </div>
          
            </div>
            {isEditMode?<div className="bg-white mt-2 px-2 py-4 border-t border-gray-200 sm:px-4 flex flex-col items-end">
                <div className="text-right">
                    <Button
                        className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                       onClick={save}
                        text="Save"
                    />
                    <Button
                        className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-gray-dark shadow-sm hover:bg-sc-gray-light focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                        type="normal"
                        text="Cancel"
                        onClick={() => {setIsEdit(!isEdit);}}
                    />
                </div>
            </div>:<div/>}
           </div>
    );
}