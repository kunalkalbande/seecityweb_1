/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { CheckBox, NumberBox, TagBox, Validator } from 'devextreme-react';

const ProductCommission = ({data,isEditMode,onChange}) => {
    console.log("comm",isEditMode);
    const [directCommission, setDirectCommission] = useState(data.directCommission);
    const [partnerCommission, setPartnerCommission] = useState(data.partnerCommission);
useEffect(()=>{
    data.directCommission=directCommission;
    data.partnerCommission=partnerCommission;
    onChange(data);
},[directCommission,partnerCommission])
    return (
        <div className="overflow-x-hidden m-5">
                    <div className="mt-4 sm:pt-1 sm:border-b sm:border-gray-200">
                        <h3  style={{textAlign:'left'}} className="text-lg leading-6 font-medium text-sc-blue-400 pb-1">
                            Seller's Commission
                </h3>
                    </div>
                    <div className="mt-2 sm:pt-1 ">
                        <p  style={{textAlign:'left'}} className="text-xs text-gray-400">
                            What is the percent (%) you will offer other Sellers of this
                            product when the product is sold directly to a Guest or
                            bundled wholesale via Partners? (10-20% is recommended)
                </p>
                    </div>
                    <div className="mt-4 grid grid-cols-1 row-gap-6 sm:grid-cols-4 sm:col-gap-8">
                        <div className="sm:col-span-1">
                            <label
                                htmlFor="directCommission"  style={{textAlign:'left'}}
                                className="block text-sm font-medium leading-5 text-gray-400"
                            >
                                Retail
                    </label>
                            <div className="mt-1">
                                <NumberBox
                                    id="directCommission"
                                    value={directCommission}
                                    format="#0%"
                                    readOnly={!isEditMode}
                                    step={0.01}
                                    onValueChanged={(e) => setDirectCommission(e.value)}
                                ></NumberBox>
                            </div>
                        </div>
                        <div className="sm:col-span-1">
                            <label
                                htmlFor="partnerCommission"  style={{textAlign:'left'}}
                                className="block text-sm font-medium leading-5 text-gray-400"
                            >
                                Wholesale
                    </label>
                            <div className="mt-1">
                                <NumberBox
                                    id="partnerCommission"
                                    format="#0%"
                                    readOnly={!isEditMode}
                                    step={0.01}
                                    value={partnerCommission}
                                    onValueChanged={(e) =>
                                        setPartnerCommission(e.value)
                                    }
                                ></NumberBox>
                            </div>
                        </div>
                    </div>
                </div>
    );
};

export default ProductCommission;
