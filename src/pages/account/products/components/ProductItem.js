import React, { useState, useEffect } from 'react';
import { DataGrid, CheckBox, TextBox } from 'devextreme-react';
import {
    ProductSalesChannel,
    ProductBasePrice,
    Productcomission,
    ProductDynamicPricing,
    ProductTaxGrid,
    ProductFeesGride,
} from './';
import { TaxesAndFees } from '../../../product/components'
import { Button } from 'devextreme-react';
import { product } from '../../../../api/graphql/product';
export const ProductItem = (data) => {
    const [selectedData, SetSelectedData] = useState(data.data.data);
    const [salesChannelData, setSalesChannelData] = useState(
        data.data.data.salesAccess
    );
    const [basePricingData, setBasePricingData] = useState(
        data.data.data.basePricing
    );
    const [isEditMode, setIsEditMode] = useState(false);
    const [isCancel, setIsCancel] = useState(false);
    console.log('selected', data);
    console.log('channel', salesChannelData);
    console.log('basePrice', basePricingData);
    const EditClick = (e) => {
        e.preventDefault();
        setIsEditMode(true);
        console.log(isEditMode);
    };
    const onSalesChannelDataChange = (value) => {
        console.log("ch", value);
        console.log("selected", selectedData);
        selectedData.salesAccess = value;
        console.log("updated", selectedData);
    }
    const onBasePriceChange = (value) => {
        console.log("base", value);
        selectedData.basePricing = value;
    }
    const onTaxAndFeesChange = (value) => {
        selectedData.taxes = value.taxes;
        selectedData.fees = value.fees;
    }
  const onProductUpdated=(value)=>{
    console.log(value);
    SetSelectedData(value);
    console.log(selectedData);
    //selectedData=value;
}
    const save = async () => {
        var res = await product.updateProduct(selectedData, null);
        setIsEditMode(false);
    }
    return (
        <div>
            <div className="flex flex-col items-end  text-sm  text-right w-full mr-10 mt-3">
                <a
                    style={{
                        marginRight: '10px',
                        textDecoration: 'underLine',
                        color: '#337AB7',
                    }}
                    href="#"
                    onClick={EditClick}
                    hidden={isEditMode}
                >
                    Edit
                </a>
            </div>

            {(() => {
                switch (data.data.name) {
                    case 'Sales Channel':
                        return (
                            <ProductSalesChannel
                                data={salesChannelData}
                                isEditMode={isEditMode}
                                onChange={(value) => { onSalesChannelDataChange(value) }}
                            />
                        );
                    case 'Base Pricing':
                        return <ProductBasePrice data={basePricingData} onChange={(value) => { onBasePriceChange(value) }} isEditMode={isEditMode} />;
                    case 'Commission':
                        return <Productcomission data={data.data.data} onChange={(value)=>{onProductUpdated(value)}} isEditMode={isEditMode}/>;
                    case 'Pricing Details':
                        return <ProductDynamicPricing data={data.data.data} onChange={(value)=>{onProductUpdated(value)}} isEditMode={isEditMode} />;
                    case 'Taxes & Fees':
                        return <TaxesAndFees product={data.data.data} isEditMode={isEditMode}
                            onChange={(value) => onTaxAndFeesChange(value)}
                        />;
                    default:
                        return (
                            <h1 style={{ textAlign: 'left', margin: '5px' }}>
                                {data.data.name}
                            </h1>
                        );
                        break;
                }
            })()}
            <div
                className="bg-white mt-2 px-2 py-4 border-t border-gray-200 "
                hidden={!isEditMode}
            >
                <Button
                    className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                    text="Save"
                    onClick={save}
                />
                <Button
                    className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-gray-dark shadow-sm hover:bg-sc-gray-light focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                    type="normal"
                    text="Cancel"
                    onClick={() => {
                        setIsEditMode(false);
                    }}
                />
            </div>
        </div>
    );
};
