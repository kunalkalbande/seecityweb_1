import DataGrid, { Column, Pager, Paging } from 'devextreme-react/data-grid';
import React, { useEffect, useState } from 'react';
import { Center } from 'devextreme-react/map';

export const ProductList = ({ products ,onselect}) => {
    const [selectedProduct, setSelectedProduct] = useState(products.products);
    const setImage=(data)=>{
var path= 'https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg'
//  if(data._id==='5f0d52edf067de479cdbe031')
//   path='/'+data.file[0].path;

return path;
    }
    const cellRender=(data)=> {
        return <img src={data.value} />;
      }
      const isPendingVisible=(e)=>{
          return e.row.data.status===null || e.row.data.status==="pending";
      }
      const isSetupVisible=(e)=>{
        return  e.row.data.status==="setup";
    }
    useEffect(() => {
        if(selectedProduct)
        onselect(selectedProduct)
    }, [selectedProduct]);
    const approve=(e)=>{
        console.log(e.row.data);
        setSelectedProduct(e.row.data);
        console.log("selecteddata",selectedProduct);
    }
    const columns = [
        // {
        //     name: 'ProductId',
        //     title: 'ID',
        // },
        {
            name: 'ProductName',
            title: 'ProductName',
        },
        {
            name: 'owner',
            title: 'Owner',
        },
    ];
    const rows = [
        {
            _id: 0,
            ProductName: 'DevExtreme',
            owner: 'DevExpress',
        },
        {
            _id: 1,
            ProductName: 'DevExtreme Reactive',
            owner: 'DevExpress',
        },
    ];

    return (
        <DataGrid
            dataSource={products}
            showBorders={true}
            style={{maxHeight:'260px'}}
            
        >
            <Paging defaultPageSize={10} />
            <Pager
                showPageSizeSelector={true}
                allowedPageSizes={[5, 10, 20]}
                showInfo={true}
            />
            {/* <Column dataField="_id" caption="Id" /> */}
            {/* <Column
                calculateCellValue={setImage}
                width={150}
                allowSorting={false}
                cellRender={cellRender}
                caption="Image"
            /> */}
            <Column dataField="name" caption="Product Name" />

            <Column dataField="isActive" caption="Is Active" width={100} />
           
            <Column dataField="shortDescription" caption="Description" />
            <Column dataField="status"  type="buttons"
                                            width={110}
                                            buttons={[
                                                {
                                                    hint: 'Pending',
                                                    text: 'Pending',
                                                    onClick: approve,
                                                    visible:isPendingVisible
                                                },
                                                {
                                                    hint: 'Setup',
                                                    text: 'Setup',
                                                    onClick: approve,
                                                    visible:isSetupVisible
                                                },
                                            ]} />

        </DataGrid>
    );
};
