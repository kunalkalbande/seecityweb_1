import {ProductList} from './ProductList'
import {SelectedProductDetail} from './SelectedProductDetail'
import {ActiveProductGrid} from './ActiveProductGrid'
import {ProductBrand} from './ProductBrand'
import {ProductTab} from './ProductTab';
import {ProductItem} from './ProductItem';
import {ProductChannelItem} from './productChannels';
export const ProductsList=ProductList;
export const SelectedProductDetails=SelectedProductDetail;
export const ActiveProductsGrid=ActiveProductGrid;
export const ProductBrands=ProductBrand;
export const ProductTabList=ProductTab;
export const ProductTabItem=ProductItem;
export const ProductSalesChannel=ProductChannelItem;
export {default as ProductBasePrice} from './ProductBasePrice';
export {default as Productcomission} from './ProductCommission';
export {default as ProductDynamicPricing} from './ProductDynamicPricing';
export {default as ProductTaxGrid} from './ProductTaxGrid';
export {default as ProductFeesGride} from './ProductFeesGrid';