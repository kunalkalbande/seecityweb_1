/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { CheckBox, NumberBox, TagBox, Validator } from 'devextreme-react';
import { ProductPricing } from '../../../product/components';
const ProductDynamicPricing = (data) => {
    console.log(data);
    const [priceByDay, setPriceByDay] = useState(data.data.priceByDay);
    useEffect(() => {
        const fetchData = async () => {
            //const data = await lookup.getLookupByType("Product Tags");
            //setProductTags(data);
            // setProductTags(product_tags);
        };
        fetchData();
    }, []);
 useEffect(()=>{
data.data.priceByDay=priceByDay;
data.onChange(data.data);
 },[priceByDay,data.data])
    return (
        <div className="overflow-x-hidden m-5">
            <div className="mt-4 sm:pt-1 sm:border-b sm:border-gray-200">
                <h3
                    style={{ textAlign: 'left' }}
                    className="text-lg leading-6 font-medium text-sc-blue-400 pb-1"
                >
                    Dynamic Pricing
                </h3>
            </div>
            <div className="mt-4 sm:pt-1 flex flex-1 flex-col">
                <div className="mt-2 flex flex-0 items-center sm:items-start">
                    <CheckBox
                        id="priceByPeriod"
                        value={data.data.priceByPeriod}
                        disabled={true}
                    />
                    <label
                        htmlFor="priceByPeriod"
                        className="ml-2 block text-sm font-medium leading-5 text-gray-400"
                    >
                        By Operating Period
                    </label>
                </div>
                <div className="mt-2 flex flex-0 items-center sm:items-start">
                    <CheckBox
                        id="priceByDayPart"
                        value={data.data.priceByDayPart}
                        disabled={true}
                    />
                    <label
                        htmlFor="priceByPeriodPart"
                        className="ml-2 block text-sm font-medium leading-5 text-gray-400"
                    >
                        By Day Part
                    </label>
                </div>
                <div className="mt-2 flex flex-0 items-center sm:items-start">
                    <CheckBox
                        id="priceByDay"
                        disabled={!data.isEditMode}
                        value={priceByDay}
                        onValueChanged={(e) => setPriceByDay(e.value)}
                    />
                    <label
                        htmlFor="priceByPeriod"
                        className="ml-2 block text-sm font-medium leading-5 text-gray-400"
                    >
                        By Day
                    </label>
                </div>
                <div className="mt-5">
                    <ProductPricing
                        product={data.data}
                        editMode={data.isEditMode}
                    />
                </div>
            </div>
        </div>
    );
};

export default ProductDynamicPricing;
