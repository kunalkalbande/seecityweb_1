import React, { useState, useEffect } from 'react';
import { DataGrid, CheckBox, TextBox } from 'devextreme-react';
export const ProductChannelItem = ({ data, isEditMode,onChange }) => {
    const [salesChannelData, setSalesChannelData] = useState(data);
    const [isReadMode, setIsReadMode] = useState(!isEditMode);
    const [isMySalesChannel, setIsMySalesChannel] = useState(
        salesChannelData ? salesChannelData.isMySalesChannel : false
    );
    const [isLocalPartners, setIsLocalPartners] = useState(
        salesChannelData ? salesChannelData.isLocalPartners : false
    );
    const [isLocalAndNationalSearch, setIsLocalAndNationalSearch] = useState(
        salesChannelData ? salesChannelData.isLocalAndNationalSearch : false
    );
    const [isModePrivate, setIsModePrivate] = useState(
        salesChannelData ? salesChannelData.isModePrivate : false
    );
    const [isModePublic, setIsModePublic] = useState(
        salesChannelData ? salesChannelData.isModePublic : false
    );
    const [revealCode, setRevealCode] = useState(
        salesChannelData ? salesChannelData.revealCode : ''
    );
  useEffect(() => {
      if (salesChannelData) {
          salesChannelData.isModePublic = isModePublic;
          salesChannelData.isModePrivate = isModePrivate;
          salesChannelData.isMySalesChannel = isMySalesChannel;
          salesChannelData.isLocalAndNationalSearch = setIsLocalAndNationalSearch;
          salesChannelData.isLocalPartners = isLocalPartners;
          salesChannelData.revealCode = revealCode;
      }
      onChange(salesChannelData);
  }, [
      isModePrivate,
      isModePublic,
      isMySalesChannel,
      revealCode,
      isLocalPartners,
      isLocalAndNationalSearch,
  ]);
   
    
    console.log('selected channel', data);
    console.log('channel', salesChannelData);
    console.log('ReadMode', isReadMode);
    console.log('EditMode', isEditMode);

    useEffect(() => {
        const init = () => {
        setIsReadMode(!isEditMode);
        console.log("revealcode",data.revealCode);
        setRevealCode(data.revealCode);
        console.log('effect', isReadMode);
        }
        init();
    }, []);

    return (
        <div className="flex flex-col m-10">
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label
                    style={{ textAlign: 'left' }}
                    class="block text-sm font-medium leading-5 text-gray-700 mt-1 sm:col-span-1"
                >
                    My Sales Channel
                </label>
                <div class="sm:mt-0 sm:col-span-4 mt-1 ">
                    <div class="mt-1 sm:mt-0 sm:col-span-4 flex ml-2 align-start">
                        <CheckBox
                            readOnly={!isEditMode}
                            onValueChanged={(e) => {
                                setIsMySalesChannel(e.value);
                            }}
                            value={isMySalesChannel}
                            style={{ alignSelf: 'left' }}
                        />
                    </div>
                </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label
                    style={{ textAlign: 'left' }}
                    class="block text-sm font-medium leading-5 text-gray-700 mt-1 sm:col-span-1"
                >
                    Local Partners
                </label>
                <div class="sm:mt-0 sm:col-span-4 mt-1 ">
                    <div class="mt-1 sm:mt-0 sm:col-span-4 flex ml-2 align-start">
                        <CheckBox
                            readOnly={!isEditMode}
                            value={isLocalPartners}
                            onValueChanged={(e) => {
                                setIsLocalPartners(e.value);
                            }}
                            style={{ alignSelf: 'left' }}
                        />
                    </div>
                </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label
                    style={{ textAlign: 'left' }}
                    class="block text-sm font-medium leading-5 text-gray-700 mt-1 sm:col-span-1"
                >
                    Local And National Search
                </label>
                <div class="sm:mt-0 sm:col-span-4 mt-1 ">
                    <div class="mt-1 sm:mt-0 sm:col-span-4 flex ml-2 align-start">
                        <CheckBox
                            readOnly={!isEditMode}
                            value={isLocalAndNationalSearch}
                            onValueChanged={(e) => {
                                setIsLocalAndNationalSearch(e.value);
                            }}
                            style={{ alignSelf: 'left' }}
                        />
                    </div>
                </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label
                    style={{ textAlign: 'left' }}
                    class="block text-sm font-medium leading-5 text-gray-700 mt-1 sm:col-span-1"
                >
                    Public Mode
                </label>
                <div class="sm:mt-0 sm:col-span-4 mt-1 ">
                    <div class="mt-1 sm:mt-0 sm:col-span-4 flex ml-2 align-start">
                        <CheckBox
                            readOnly={!isEditMode}
                            value={isModePublic}
                            onValueChanged={(e) => {
                                setIsModePublic(e.value);
                            }}
                            style={{ alignSelf: 'left' }}
                        />
                    </div>
                </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label
                    style={{ textAlign: 'left' }}
                    class="block text-sm font-medium leading-5 text-gray-700 mt-1 sm:col-span-1"
                >
                    Private Mode
                </label>
                <div class="sm:mt-0 sm:col-span-4 mt-1 ">
                    <div class="mt-1 sm:mt-0 sm:col-span-4 flex ml-2 align-start">
                        <CheckBox
                            readOnly={!isEditMode}
                            value={isModePrivate}
                            onValueChanged={(e) => {
                                setIsModePrivate(e.value);
                            }}
                            style={{ alignSelf: 'left' }}
                        />
                    </div>
                </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label
                    style={{ textAlign: 'left' }}
                    class="block text-sm font-medium leading-5 text-gray-700 mt-2 "
                >
                    Reveal Code
                </label>
                <div class="sm:mt-0 sm:col-span-4 mt-1 ml-2">
                    <TextBox
                        readOnly={!isEditMode}
                        value={revealCode}
                        onValueChanged={(e) => {
                            setRevealCode(e.value);
                        }}
                    />
                </div>
            </div>
        </div>
    );
};
