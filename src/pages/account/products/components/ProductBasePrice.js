/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { CheckBox, NumberBox, TagBox, Validator } from 'devextreme-react';
import { arrayUtils } from '../../../../utils';
import { BasePricing } from '../../../../pages/product/components';

const product_tags = [
    'General Admission',
    'Adult',
    'Child',
    'Senior',
    'Military',
];

const ProductBasePrices = (basePrice) => {
    console.log("from component",basePrice)
    const [basePricing, setBasePricing] = useState(basePrice.data);
    console.log("from component ==",basePricing)

    const [priceByDay, setPriceByDay] = useState(false);
    const [priceByDayPart, setPriceByDayPart] = useState(true);
    const [priceByPeriod, setPriceByPeriod] = useState(true);

    const [directCommission, setDirectCommission] = useState(0.1);
    const [partnerCommission, setPartnerCommission] = useState(0.11);
    const [productTags, setProductTags] = useState([]);
    const [basePricingArray, setBasePricingArray] = useState([]);
    const [basePricingItems, setBasePricingItems] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            //const data = await lookup.getLookupByType("Product Tags");
            //setProductTags(data);

            setBasePricing(basePrice.data);
            setProductTags(product_tags);
    console.log("from component =====",basePricing)

        };
        fetchData();
    }, []);
useEffect(()=>{
basePrice.onChange(basePricing);
},[basePricing])
   

    const onProductTagsChanged = (args) => {
        let array = [...basePricing];
        if (args.previousValue.length < args.value.length) {
            let tags = arrayUtils.except(args.value, args.previousValue);
            tags.forEach((tag) => {
                if (tag !== '') {
                    array.push({ productType: tag, retail: 0, wholesale: 0 });
                }
            });
        } else {
            let tag = arrayUtils.except(args.previousValue, args.value)[0];
            let idx = [...basePricing].map((e) => e.name).indexOf(tag);
            array.splice(idx, 1);
        }
        setBasePricing(array);
    };

    return (
        <div className="overflow-x-hidden">
            <div className="max-h-screen">
                <div className="mx-2 mt-10 sm:mx-5 flex flex-col bg-white">
                <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label style={{textAlign:'left'}} class="block text-sm font-medium leading-5 text-gray-700 mt-1 sm:col-span-1">
                            Product Types
                </label>
                <div class="sm:col-span-4">
                        <TagBox
                            items={productTags}
                            value={basePricing.map((m) => m.productType)}
                            onValueChanged={onProductTagsChanged}
                            readOnly={!basePrice.isEditMode}
                            applyValueMode="useButtons"
                            showSelectionControls={true}
                            hideSelectedItems={true}
                            showDropDownButton={true}
                            placeholder="Select..."
                        >
                       
                        </TagBox>
                </div>
                    </div>
                    <div className="mt-4 sm:pt-1 sm:border-b sm:border-gray-200 item-start">
                        <h3 style={{textAlign:'left'}}  className="text-lg leading-6 font-medium text-sc-blue-400 pb-1">
                            Base Pricing
                </h3>
                    </div>
                    <div className="mt-1 flex flex-1 flex-col mb-10">
                        <div className="invisible h-0 sm:h-2 sm:visible sm:mt-4 grid grid-cols-3 sm:grid-cols-6 col-gap-4">
                            <div style={{textAlign:'left'}} className="text-gray-400 text-xs">Product Type</div>
                            <div style={{textAlign:'left'}} className="text-gray-400 text-xs">Retail</div>
                            <div style={{textAlign:'left'}} className="text-gray-400 text-xs">Wholesale</div>
                        </div>
                        <div style={{textAlign:'left'}} className="mt-2 sm:mt-3">
                            {basePricing.map((item, index) => (
                                <BasePricing  key={index} item={item} readOnly={!basePrice.isEditMode}/>
                            ))}
                        </div>
                    </div>
                  
                        </div>
                    </div>
                </div>
    );
};

export default ProductBasePrices;
