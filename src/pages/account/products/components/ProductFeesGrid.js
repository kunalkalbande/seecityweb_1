/* eslint-disable react/prop-types */
import React, { useState, useEffect, useRef } from 'react';
import { Accordion, DataGrid, SpeedDialAction } from 'devextreme-react';
import { Editing, Column, ValidationRule, Lookup } from 'devextreme-react/data-grid';

const appliedTypes = [
    {
        value: "Exclusive",
        name: "Exclusive"
    },
    {
        value: "Inclusive",
        name: "Inclusive"
    }
]

const taxNames = [
    {
        value: "Tax - 1",
        name: "Tax - 1"
    },
    {
        value: "Tax - 2",
        name: "Tax - 2"
    },
    {
        value: "Tax - 3",
        name: "Tax - 3"
    }
]

const feeNames = [
    {
        value: "Fee-1",
        name: "Fee-1"
    },
    {
        value: "Fee-2",
        name: "Fee-2"
    },
    {
        value: "Fee-3",
        name: "Fee-3"
    }
]

const taxAndFeesData = [
      
        {
                feeName: "Fee-1",
                feeAmount: "15",
                appliedType: "Inclusive"
            
        }
];





const FeesGrid = (props) => {
    return (
        <div className='m-5'>
            <DataGrid
                id="fees_grid"
                dataSource={taxAndFeesData}
                columnAutoWidth={true}
                showBorders={true}
                showColumnLines={true}
                showRowLines={false}
                rowAlternationEnabled={true}
            >
                <Editing mode="form" allowAdding={false} allowUpdating={false} allowDeleting={false} />

                <Column
                    dataField="feeName"
                    dataType="string"
                    caption="Fee Name"
                    allowSorting={false}
                >
                    <Lookup dataSource={feeNames} displayExpr="name" valueExpr="value" />
                    <ValidationRule type="required" />
                </Column>
                <Column
                    dataField="feeAmount"
                    dataType="number"
                    alignment="left"
                    caption="Fee Amount"
                    allowSorting={false}
                >
                    <ValidationRule type="required" />
                </Column>
                <Column
                    dataField="appliedType"
                    dataType="string"
                    caption="How is it applied?"
                    allowSorting={false}
                >
                    <Lookup dataSource={appliedTypes} displayExpr="name" valueExpr="value" />
                    <ValidationRule type="required" />
                </Column>
            </DataGrid>
        </div>
    );
}



export default FeesGrid;