import React, { useState ,useEffect} from 'react';
import { TabPanel } from 'devextreme-react';
import {ProductTabItem} from './';
import { from } from 'apollo-link';
export const ProductTab=(({selectedProduct})=>{
    console.log("prd",selectedProduct);
const ProductsTabList=[{name:"Sales Channel",data:selectedProduct},
{name:"Base Pricing",data:selectedProduct},{name:"Commission",data:selectedProduct},
{name:"Pricing Details",data:selectedProduct},{name:"Inventory",data:selectedProduct},
{name:'Taxes & Fees',data:selectedProduct}];
const itemTitleRender=(setup)=>{
return <span>{setup.name}</span>
}
return( <TabPanel dataSource={ProductsTabList}  style={{minHeight:'150px',margin:'10px'}}
itemComponent={ProductTabItem} itemTitleRender={itemTitleRender}  />)
})