import React, { useState, useEffect } from 'react';
import {
    Button,
    TextBox,
    CheckBox,
    DateBox,
    FileUploader,
    TextArea,
} from 'devextreme-react';
import { product } from '../../../../api/graphql/product';
import { from } from 'apollo-link';
export const ProductBrand = ({ selectedProduct, isEditMode, onChange }) => {
    const [isEdit, setIsEdit] = useState(true);
    const [name, setname] = useState(selectedProduct.name);
    const [shortDescription, setShortDesc] = useState(
        selectedProduct.shortDescription
    );
    const [longDescription, setLongDesc] = useState(
        selectedProduct.longDescription
    );
    const [termsAndConditions, setTermsAndCondition] = useState(
        selectedProduct.termsAndConditions
    );
    const [isActive, setIsActive] = useState(selectedProduct.isActive);
    const [endDate, setEndDate] = useState(selectedProduct.endDate);
    const [advanceSaleEndDate, setAdvanceSaleEndDate] = useState(
        selectedProduct.advanceSaleEndDate
    );
    const [logo, setLogo] = useState([]);

    const OnImageUpload = (args) => {
        document.getElementById('img1').src = URL.createObjectURL(
            args.value[0]
        );
        setLogo(args.value);
    };

    useEffect(() => {
        console.log('isEdit', isEdit);
        onChange();
    }, [isEdit]);
    const save = async () => {
        // selectedProduct.name=name;
        // selectedProduct.shortDescription=shortDescription;
        // selectedProduct.longDescription=longDescription;
        // selectedProduct.termsAndConditions=termsAndConditions;
        // selectedProduct.isActive=isActive;
        // selectedProduct.endDate=endDate;
        // selectedProduct.advanceSaleEndDate=advanceSaleEndDate;
        console.log('updated', selectedProduct);
        console.log(logo);
        var res = await product.updateProduct(selectedProduct, logo);
        if (selectedProduct.file.length > 0)
          {
            selectedProduct.file[0].path = res.updateProduct.file[0].path;
          } 
        else {selectedProduct.file =[];
        selectedProduct.file.push(res.updateProduct.file[0]);}
        setIsEdit(!isEdit);
    };
    console.log(selectedProduct);
    return (
        <div>
            <div className="flex flex-wrap w-full">
                <label
                    htmlFor="about"
                    className="block text-sm font-medium leading-5 text-gray-700"
                >
                    Product Image
                </label>
                <div
                    style={{ minHeight: '273px' }}
                    className="w-full h-px-200 border-dashed border-2 mt-2 rounded "
                >
                    {isEditMode ? (
                        <FileUploader
                            id="FileUpload"
                            selectButtonText="Browse to upload (200 x 300)"
                            labelText=""
                            accept="image/*"
                            uploadMode="useForm"
                            onValueChanged={OnImageUpload}
                            multiple={false}
                            disabled={false}
                            style={{
                                height: '290px',
                                maxHeight: '290px',
                                index: 10,
                                position: 'absolute',
                            }}
                        ></FileUploader>
                    ) : (
                        <div />
                    )}
                    <div id="divimg1" className="px-2 flex justify-center">
                        <img
                            id="img1"
                            src={
                                selectedProduct.file[0]
                                    ? '/' + selectedProduct.file[0].path
                                    : 'https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg'
                            }
                            style={{
                                margin: '10px',
                                width: '70%',
                                maxHeight: '200px',
                            }}
                        />
                    </div>
                </div>
            </div>
            {isEditMode ? (
                <div className="bg-white mt-2 px-2 py-4 border-t border-gray-200 sm:px-4 flex flex-col items-end">
                    <div className="text-right">
                        <Button
                            className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                            onClick={save}
                            text="Save"
                        />
                        <Button
                            className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-gray-dark shadow-sm hover:bg-sc-gray-light focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                            type="normal"
                            text="Cancel"
                            onClick={() => {
                                setIsEdit(!isEdit);
                            }}
                        />
                    </div>
                </div>
            ) : (
                <div />
            )}
        </div>
    );
};
