import React, { useState, useEffect } from 'react';
import Loading from '../../../components/common/Loading';
import { accountSetup } from '../../../api/graphql/accountSetup';
import { changeRequest } from '../../../api/graphql/changeRequest';

import { useDispatch, useSelector } from 'react-redux';

const DashboardPage = () => {
    const [isLoading, setIsLoading] = useState(false);
    const sgMail = require('@sendgrid/mail');
    const currentUser = useSelector((state) => state.user);

    const changeName = async () => {
        setIsLoading(true);
        await submitNameApi();
        setIsLoading(false);
    };

    const changeImage = async () => {
        setIsLoading(true);
        await submitImageApi();
        setIsLoading(false);
    };

    const submitNameApi = () => {
        return new Promise((resolve) => {
            setTimeout(async () => {
                try {
                    if (
                        currentUser.bag != undefined &&
                        currentUser.bag.accountId != undefined
                    ) {
                        var changeRequestObj = {};
                        var fields = [];

                        changeRequestObj.accountId = currentUser.bag.accountId;
                        changeRequestObj.status = 'Pending';
                        changeRequestObj.type = 'Account Adjustment';

                        var changeRequestField = {};

                        changeRequestField.field = 'Account Name';
                        changeRequestField.currentValue = "Biff's Petting Zoo";
                        changeRequestField.newValue = "Mike's Beasts";
                        fields.push(changeRequestField);
                        var changeRequestField1 = {};

                        changeRequestField1.field = 'Operating Hours';
                        changeRequestField1.currentValue = 'We are open 9-5pm';
                        changeRequestField1.newValue = 'We are open 24/7';
                        fields.push(changeRequestField1);
                        changeRequestObj.fields = fields;

                        var res = await changeRequest.saveChangeRequests(
                            changeRequestObj
                        );
                        if (res) {
                            resolve(res);
                            alert(
                                'Account Adjustment has been changed and sent for review.'
                            );
                        } else {
                            setIsLoading(false);
                            alert('error');
                        }
                    }

                    //
                } catch (error) {
                    console.error(error);

                    if (error.response) {
                        console.error(error.response.body);
                    }
                }
            }, 1000);
        });
    };
    const submitImageApi = () => {
        return new Promise((resolve) => {
            setTimeout(async () => {
                try {
                    if (
                        currentUser.bag != undefined &&
                        currentUser.bag.accountId != undefined
                    ) {
                        var changeRequestObj = {};
                        var fields = [];

                        changeRequestObj.accountId = currentUser.bag.accountId;
                        changeRequestObj.status = 'Pending';
                        changeRequestObj.type = 'Account Image Adjustment';

                        var changeRequestField = {};

                        changeRequestField.field = 'Account Image';
                        changeRequestField.currentValue =
                            './images/5f02d23d93d193686cc921fa_guestx/_Gk09RWaX-NPKioskBackground_Halloween2019.png';
                        changeRequestField.newValue =
                            './images/5f02d23d93d193686cc921fa_guestx/WAnidNNWM_-NPKioskBackground.png';
                        fields.push(changeRequestField);
                        var changeRequestField1 = {};

                        changeRequestField1.field = 'Account Image';
                        changeRequestField1.currentValue =
                            './images/5f02d23d93d193686cc921fa_guestx/_Gk09RWaX-NPKioskBackground_Halloween2019.png';
                        changeRequestField1.newValue =
                            './images/5f02d23d93d193686cc921fa_guestx/WAnidNNWM_-NPKioskBackground.png';
                        fields.push(changeRequestField1);
                        changeRequestObj.fields = fields;

                        var res = await changeRequest.saveChangeRequests(
                            changeRequestObj
                        );
                        if (res) {
                            resolve(res);
                            alert(
                                'Account Image has been changed and sent for review.'
                            );
                        } else {
                            setIsLoading(false);
                            alert('error');
                        }
                    }

                    //
                } catch (error) {
                    console.error(error);

                    if (error.response) {
                        console.error(error.response.body);
                    }
                }
            }, 1000);
        });
    };
    return (
        <div>
            {!isLoading ? (
                <div
                    className="flex flex-col mt-8"
                    style={{ width: 300, margin: 10 }}
                >
                    <div className="w-full" style={{ margin: 10 }}>
                        <button
                            onClick={changeName}
                            id="btn-create-account"
                            className="bg-blue-500 hover:bg-blue-700 text-white text-sm font-semibold py-2 px-4 rounded w-full"
                            style={{
                                backgroundColor: '#3db5e7',
                            }}
                        >
                            Submit Review for Account Adjustment
                        </button>
                    </div>
                    <div className="w-full" style={{ margin: 10 }}>
                        <button
                            onClick={changeImage}
                            id="btn-create-account"
                            className="bg-blue-500 hover:bg-blue-700 text-white text-sm font-semibold py-2 px-4 rounded w-full"
                            style={{
                                backgroundColor: '#3db5e7',
                            }}
                        >
                            Submit Review for Account Image Adjustment
                        </button>
                    </div>
                </div>
            ) : (
                <Loading />
            )}
        </div>
    );
};

export default DashboardPage;
