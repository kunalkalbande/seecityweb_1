import React, { useState, useEffect } from 'react';
import Address from './Address';
import { CheckBox, TextBox, Validator, NumberBox } from 'devextreme-react';
import { RequiredRule } from 'devextreme-react/data-grid';

const PublicPhone = (props) => {
    const [isSameAddress, setIsDiffVenueAddress] = useState(true);
    const [publicPhone, setPublicPhone] = useState('');
    const [addressTwo, setAddressTwo] = useState('');
    const phoneRules = { 'X': /[02-9]/ };

    useEffect(() => {
        const fetchData = () => {
            if (props.setupDetail != null) {
                if (props.setupDetail.address != null) {
                    var addressDetail = props.setupDetail.address;
                    setPublicPhone(addressDetail.phoneNumber);
                    setIsDiffVenueAddress(addressDetail.isSameAddress);
                }
            }
        };
        fetchData();
    }, []);

    useEffect(() => {
        var phone = {};
        phone.phoneNumber = publicPhone;
        phone.isSameAddress = isSameAddress;
        phone.addressTwo = addressTwo;
        props.onChange(phone);
    }, [publicPhone, isSameAddress, addressTwo]);

    const getAddressChangeTwo = (account) => {
        if (
            account != null &&
            account != undefined &&
            account.addressOne != undefined &&
            account.addressOne.length > 0
        ) {
            var address = {};
            address.addressOne = account.addressOne;
            address.addressTwo = account.addressTwo;
            address.city = account.city;
            address.stateId = account.stateId;
            address.zip = account.zip;
            setAddressTwo(address);
            console.log("address 2" + account.addressOne);

        }
    };

    return (
        <div div id="public-phone" className="">
            <div className="flex flex-wrap -mx-3">
                <div className="w-1/2 md:w-6/12 px-3 mb-3">
                    <TextBox
                        mask="+1 (X00) 000-0000"
                        maskRules={phoneRules}
                        id="public-phone"
                        type="number"
                        name="publicPhone"
                        //showClearButton={true}
                        value={publicPhone}
                        placeholder="Public Phone"
                        onValueChanged={(e) => setPublicPhone(e.value)}
                    >
                        <Validator>
                            <RequiredRule message="Public Phone is required" />
                        </Validator>
                    </TextBox>
                </div>
            </div>
            <hr className="my-1"></hr>
            <div className="flex flex-wrap mb-2">
                <div>
                    <div className="flex items-left mt-4">
                        <CheckBox
                            text="My venue address is different than the company address"
                            value={!isSameAddress}
                            onValueChanged={(e) =>
                                setIsDiffVenueAddress(e.value)
                            }
                        />
                    </div>
                </div>
            </div>
            <fieldset disabled={isSameAddress}>

                {!isSameAddress ? (
                    <Address
                        isFromPhone={true}
                        onChange={(value) => getAddressChangeTwo(value)}
                        setupDetail={props.setupDetail}

                    />) : (

                        isSameAddress

                    )}
            </fieldset>
        </div>
    );
};

export default PublicPhone;
