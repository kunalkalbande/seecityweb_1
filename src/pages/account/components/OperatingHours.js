import React, { useState, useEffect } from 'react';
import { TextArea, Validator } from 'devextreme-react';
import { RequiredRule } from 'devextreme-react/data-grid';
const OperatingDetails = (props) => {
    const [operatingPolicy, setOperatingPolicy] = useState('');

    useEffect(() => {
        const fetchData = () => {
            if (props.setupDetail != null) {
                setOperatingPolicy(props.setupDetail.operatingPolicy);
            }
        };
        fetchData();
    }, []);

    useEffect(() => {
        props.onChange(operatingPolicy);
    }, [operatingPolicy]);

    return (
        <div className="mx-3">
            <div>
                <p className="break-words text-gray-500 ">
                    Write a general description of your operating policy. Your
                    timed
                </p>
                <p className="break-words text-gray-500 ">
                    tickets, If applicable , will be controlled separately. This
                    is for
                </p>
                <p className="text-gray-500">general information</p>
            </div>
            <div className="md:flex mb-6 ">
                <div className="md:flex-1 mt-2 mb:mt-0 ">
                    <TextArea
                        showClearButton={true}
                        className="w-full border border-gray-400"
                        placeholder=" Open Monday thru Saturdays, 10am - 6pm-round. Summers open at 8am."
                        rows="6"
                        onValueChanged={(e) => setOperatingPolicy(e.value)}
                        value={operatingPolicy}
                    >
                        <Validator>
                            <RequiredRule message="Operating Days/Hours is required" />
                        </Validator>
                    </TextArea>
                </div>
            </div>
        </div>
    );
};

export default OperatingDetails;
