import { TextArea, Validator } from 'devextreme-react';
import { RequiredRule } from 'devextreme-react/data-grid';
import React, { useState, useEffect } from 'react';

const Brand = (props) => {
    const [brandDetail, setBrandDetail] = useState('');
    useEffect(() => {
        const fetchData = () => {
            if (props.setupDetail != null) {
                setBrandDetail(props.setupDetail.brandDetail);
            }
        };
        fetchData();
    }, []);

    useEffect(() => {
        props.onChange(brandDetail);
    }, [brandDetail]);

    return (
        <div id="about" className="">
            <div className="flex flex-wrap">
                {/* <div className="w-full sm:w-4/12 px-3 my-2"> */}
                <div className="w-full px-3 my-2">
                    {/* <h1 className="text-2xl font-bold mb-3">About Us</h1> */}
                    <p className="font-normal  text-gray-500">
                        Write a brief description of your businesss and
                    </p>
                    <p className="font-normal  text-gray-500">
                        experience guests can expect to enjoy (50 words max)
                    </p>
                </div>
                {/* <div className="w-full sm:w-8/12 px-3 my-2"> */}
                <div className="w-full px-3 my-2">
                    <div className="w-full">
                        <TextArea
                            showClearButton={true}
                            className="w-full h-32 border border-gray-400"
                            placeholder=" The Oklahoma City Zoo is the oldest zoo in America, originating
                                       in the year 27 B.C. We have over 100 species of animal,
                                      featuring our real word-famous gorilaa exhibit and flight of
                                       elephents experience. Only a few miles from downtown OKC.
                                       Satisfaction guaranteed!"
                            rows="6"
                            value={brandDetail}
                            onValueChanged={(e) => setBrandDetail(e.value)}
                        >
                            <Validator>
                                <RequiredRule message="Brand details is required" />
                            </Validator>
                        </TextArea>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default Brand;
