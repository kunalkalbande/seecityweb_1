import React, { useState, useEffect } from 'react';
import { CheckBox } from 'devextreme-react';

const SellingMode = (props) => {
    const [isProviderSeller, setIsProviderSeller] = useState(false);
    const [isSeller, setIsSeller] = useState(false);

    useEffect(() => {
        const fetchData = () => {
            if (props.setupDetail != null) {
                setIsProviderSeller(props.setupDetail.isProviderSeller);
                setIsSeller(props.setupDetail.isSeller);
            }
        };
        fetchData();
    }, []);

    useEffect(() => {
        var sellingMode = {};
        sellingMode.isProviderSeller = isProviderSeller;
        sellingMode.isSeller = isSeller;
        props.onChange(sellingMode);
    }, [isProviderSeller, isSeller]);

    return (
        <div className="mx-3">
            <div className="flex items-left mt-4">
                <CheckBox
                    text=""
                    value={isProviderSeller}
                    onValueChanged={(e) => setIsProviderSeller(e.value)}
                />
                <label className="flex justify-start items-start ml-2">
                    <div className="select-none text-gray-500">
                        I am a <span className="underline">Provider</span> and a{' '}
                        <span className="underline">Seller</span> and I intend
                        to offer product for sale while also able to sell others{' '}
                    </div>
                </label>
            </div>
            <div className="flex items-left mt-4">
                <CheckBox
                    text=""
                    value={isSeller}
                    onValueChanged={(e) => setIsSeller(e.value)}
                />
                <label className="flex justify-start items-start ml-2">
                    <div className="select-none text-gray-500">
                        I am a <span className="underline">Seller</span> and
                        only I intend to sell other.{' '}
                    </div>
                </label>
            </div>
        </div>
    );
};
export default SellingMode;
