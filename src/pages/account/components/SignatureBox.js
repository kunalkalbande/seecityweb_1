import React, { useState, useEffect } from 'react';
import SignaturePad from 'react-signature-canvas'

const SignatureBox = () => {
	const [trimmedDataURL, setTrimmedDataURL] = useState(null);
	const [showModal, setShowModal] = useState(false);
	var sigPad = {}
	const clear = () => {
		sigPad.clear()
	}
	const trim = () => {
		setTrimmedDataURL(sigPad.getTrimmedCanvas().toDataURL('image.png'))
		setShowModal(false)
	}
	return (
		<>
			{trimmedDataURL
				?
				<div
					id="signature-box"
					onClick={() => setShowModal(true)}
					className="block appearance-none w-full h-10 bg-transparent border-b mb-3 px-3 py-1 focus:outline-none focus:border-blue-500"
				>
					<img className="h-10 mx-auto"
						src={trimmedDataURL} />
				</div>
				:
				<div
					id="signature-box" type="text" placeholder=""
					onClick={() => setShowModal(true)}
					className="block appearance-none w-full h-10 bg-transparent border-b mb-3 px-3 py-1 focus:outline-none focus:border-blue-500"
				/>
			}

			{showModal ? (
				<>
					<div
						className="justify-center items-center flex overflow-x-hidden overflow-y-auto m-5 fixed inset-0 z-50 outline-none focus:outline-none"
					>
						<div className="relative w-auto my-6 mx-auto max-w-3xl">
							{/*content*/}
							<div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
								{/*header*/}
								<div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t">
									<h3 className="text-3xl font-semibold text-center">
										Sign
                  </h3>
									<button
										className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
										onClick={() => setShowModal(false)}
									>
										<span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
											×
                    </span>
									</button>
								</div>
								{/*body*/}
								<div className="relative flex-auto h-48">

									<SignaturePad canvasProps={{ className: "w-full h-full" }}
										ref={(ref) => { sigPad = ref }} />

								</div>
								{/*footer*/}
								<div className="flex items-center justify-end p-6 border-t border-solid border-gray-300 rounded-b">
									<button
										className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1"
										type="button"
										style={{ transition: "all .15s ease" }}
										onClick={() => setShowModal(false)}
									>
										Close
                  </button>
									<button
										className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1"
										type="button"
										style={{ transition: "all .15s ease" }}
										onClick={() => clear(false)}
									>
										Clear
                  </button>
									<button
										className="bg-green-500 text-white active:bg-green-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
										type="button"
										style={{ transition: "all .15s ease" }}
										onClick={() => trim()}
									>
										Save Changes
                  </button>
								</div>
							</div>
						</div>
					</div>
					<div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
				</>
			) : null}

		</>
	);
}

export default SignatureBox;