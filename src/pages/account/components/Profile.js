import React, { useState, useEffect } from 'react';
import { SelectBox, TextBox, Validator } from 'devextreme-react';
//import accountTypes from '../../../data/account-types';
import { RequiredRule } from 'devextreme-react/data-grid';
import { findByKey } from '../../../utils/index';
import moment from 'moment';
import { lookup } from '../../../api/graphql/lookup';

const ProfileDetails = (props) => {
    const [accountTypes, setAccountTypes] = useState([]);

    const [companyName, setCompanyName] = useState('');
    const [venueName, setVenueName] = useState('');
    const [accountType, setAccountType] = useState('');
    console.log(props);
    useEffect(() => {
        const init = async () => {
            const data = await lookup.getLookupByType("Account Tags");
            setAccountTypes(data);
            await fetchData(data);
        }
        const fetchData = async (accountTypes) => {
            if (props.setupDetail != null) {
                setCompanyName(props.setupDetail.companyName);
                setVenueName(props.setupDetail.venueName);
                setAccountType(
                    findByKey(props.setupDetail.accountTypeId, accountTypes)
                );
            }
        };
        init();
        //fetchData();
    }, []);

    useEffect(() => {
        var profile = {};
        profile.companyName = companyName;
        profile.venueName = venueName;
        profile.accountTypeId = accountType;
        props.onChange(profile);
    }, [companyName, venueName, accountType]);

    return (
        <div id="profile" className="">
            <div className="flex flex-wrap ">
                <div className="w-full">
                    <SelectBox
                        dataSource={accountTypes}
                        placeholder="Choose One Account Type"
                        displayExpr="name"
                        valueExpr="value"
                        value={accountType.value}
                        showClearButton={true}
                        onValueChanged={(e) =>
                            setAccountType(e.value ? e.value : '')
                        }
                    >
                        <Validator>
                            <RequiredRule message="Account Type is required" />
                        </Validator>
                    </SelectBox>
                </div>

                <div className="w-full mt-3 mb-3">
                    <TextBox
                        id="companyName"
                        type="text"
                        name="companyName"
                        placeholder="Legal Company Name"
                        showClearButton={true}
                        value={companyName}
                        onValueChanged={(e) => setCompanyName(e.value)}
                    >
                        <Validator>
                            <RequiredRule message="Legal Company Name is required" />
                        </Validator>
                    </TextBox>
                </div>
                <div className="w-full mb-3">
                    <TextBox
                        id="venueName"
                        type="text"
                        name="venueName"
                        showClearButton={true}
                        value={venueName}
                        placeholder="Venue Name"
                        onValueChanged={(e) => setVenueName(e.value)}
                    >
                        <Validator>
                            <RequiredRule message="Venue Name is required" />
                        </Validator>
                    </TextBox>
                </div>
            </div>
        </div>
    );
};

export default ProfileDetails;
