import React, { useState, useEffect } from 'react';
import { FileUploader, ColorBox, Validator, Gallery } from 'devextreme-react';
import { RequiredRule } from 'devextreme-react/data-grid';
const LogoDetails = (setup) => {
    const [logo, setlogo] = useState([]);
    const [filePath, setPath] = useState(
        'http://www.myiconfinder.com/uploads/iconsets/256-256-37736f35fd7d98b10b35287679b223b8.png'
    );
    const [filePath1, setPath1] = useState(
        'http://www.myiconfinder.com/uploads/iconsets/256-256-37736f35fd7d98b10b35287679b223b8.png'
    );
    const [filePath2, setPath2] = useState(
        'http://www.myiconfinder.com/uploads/iconsets/256-256-37736f35fd7d98b10b35287679b223b8.png'
    );
    const [filePath3, setPath3] = useState(
        'http://www.myiconfinder.com/uploads/iconsets/256-256-37736f35fd7d98b10b35287679b223b8.png'
    );
    const [filePath4, setPath4] = useState(
        'http://www.myiconfinder.com/uploads/iconsets/256-256-37736f35fd7d98b10b35287679b223b8.png'
    );
    const [filePath5, setPath5] = useState(
        'http://www.myiconfinder.com/uploads/iconsets/256-256-37736f35fd7d98b10b35287679b223b8.png'
    );

    const [fileArray, setFileArray] = useState([]);
    const [isBrandImage, setIsBrandImage] = useState(true);
    const [isGallaryImage, setIsGallaryImage] = useState(true);

    useEffect(() => {
        if (setup.setupDetail != null) {
            if (setup.setupDetail.file != null && setup.setupDetail.file.length > 0) {
                const images = setup.setupDetail.file;
                //setlogo(images);
                if (images[0]) setPath1('/' + images[0].path);
                if (images[1]) setPath2('/' + images[1].path);
                if (images[2]) setPath3('/' + images[2].path);
                if (images[3]) setPath4('/' + images[3].path);
                if (images[4]) setPath5('/' + images[4].path);
            }
        }
    }, [])

    useEffect(() => {
        setup.onChange(logo);
    }, [logo]);
    // useEffect(() => {
    //     const init = () => {
    //         if (setup.setupDetail != null) {
    //             if (
    //                 setup.setupDetail.file != null &&
    //                 setup.setupDetail.file.length > 0
    //             ) {
    //                 setPath('/' + setup.setupDetail.file[0].path);
    //                 if (setup.setupDetail.file.length > 1) {
    //                     //document.getElementById("img1").style.marginLeft="-120px";
    //                     document
    //                         .getElementById('divimg1')
    //                         .classList.remove('w-1');
    //                     document
    //                         .getElementById('divimg2')
    //                         .classList.remove('w-0');
    //                     document
    //                         .getElementById('divimg1')
    //                         .classList.add('w-1/2');
    //                     document
    //                         .getElementById('divimg2')
    //                         .classList.add('w-1/2');
    //                     setPath1('/' + setup.setupDetail.file[1].path);
    //                 } else {
    //                     document.getElementById('divimg1').classList.add('w-1');
    //                     document.getElementById('divimg2').classList.add('w-0');
    //                     document
    //                         .getElementById('divimg1')
    //                         .classList.remove('w-1/2');
    //                     document
    //                         .getElementById('divimg2')
    //                         .classList.remove('w-1/2');
    //                 }
    //             } else {
    //                 document.getElementById('divimg1').classList.add('w-1');
    //                 document.getElementById('divimg2').classList.add('w-0');
    //                 document
    //                     .getElementById('divimg1')
    //                     .classList.remove('w-1/2');
    //                 document
    //                     .getElementById('divimg2')
    //                     .classList.remove('w-1/2');
    //             }
    //         } else {
    //             document.getElementById('divimg1').classList.add('w-1');
    //             document.getElementById('divimg2').classList.add('w-0');
    //             document.getElementById('divimg1').classList.remove('w-1/2');
    //             document.getElementById('divimg2').classList.remove('w-1/2');

    //             // document.getElementById("img2").style.width="20%";
    //             // document.getElementById("img1").style.marginLeft="12%";
    //             document.getElementById('img1').style.marginTop = '0%';
    //             // document.getElementById("img2").style.width="0%";
    //         }
    //     };
    //     init();
    // }, []);

    const setLogo = (index, file) => {
        console.log("image -> " + index + " " + file)
        let obj = logo;
        obj[index] = file;
        setlogo(obj);
    }

    const onBrandImgUploaded = (args) => {
        setLogo(0, args.target.files[0]);
        console.log(URL.createObjectURL(args.target.files[0]));
        setIsBrandImage(true);
        setPath1(URL.createObjectURL(args.target.files[0]));
    };
    const onGallaryImgUploaded = (args) => {
        setLogo(1, args.target.files[0]);
        console.log(URL.createObjectURL(args.target.files[0]));
        setIsGallaryImage(true);
        setPath2(URL.createObjectURL(args.target.files[0]));
    };
    const onGallaryImgUploaded3 = (args) => {
        setLogo(2, args.target.files[0]);
        console.log(URL.createObjectURL(args.target.files[0]));
        setIsGallaryImage(true);
        setPath3(URL.createObjectURL(args.target.files[0]));
    };
    const onGallaryImgUploaded4 = (args) => {
        setLogo(3, args.target.files[0]);
        console.log(URL.createObjectURL(args.target.files[0]));
        setIsGallaryImage(true);
        setPath4(URL.createObjectURL(args.target.files[0]));
    };
    const onGallaryImgUploaded5 = (args) => {
        setLogo(4, args.target.files[0]);
        console.log(URL.createObjectURL(args.target.files[0]));
        setIsGallaryImage(true);
        setPath5(URL.createObjectURL(args.target.files[0]));
    };

    return (
        <div id="logo-details" className="">
            {/* <div className="flex flex-wrap">
				<div className="w-full border p-2 text-gray-700 font-bold">
					OKCZoo.OKC.see.city
				</div>
			</div> */}
            <div className="w-full border-dashed border-2 my-3 rounded  ">
                <div style={{ margin: 20 }} className="">
                    {isBrandImage ? (
                        <div htmlFor="fileUpload" className="btn">
                            <div>Brand Image</div>
                            <img
                                style={{ margin: 5 }}
                                width={200}
                                src={filePath1}
                                alt="..."
                            />
                        </div>
                    ) : (
                            <div></div>
                        )}

                    <div className="form-group" style={{ height: 10 }}>
                        <label htmlFor="fileUpload" className="btn">
                            Select Brand Image
                        </label>
                        <input
                            id="fileUpload"
                            style={{ visibility: 'hidden' }}
                            type="file"
                            className="form-control"
                            onChange={onBrandImgUploaded}
                        />
                    </div>
                </div>
                <div className="flex flex-row">
                    <div style={{ margin: 20 }}>
                        {isGallaryImage ? (
                            <div>
                                <div>Gallary Image1</div>
                                <img
                                    style={{ margin: 5 }}
                                    width={100}
                                    src={filePath2}
                                    alt="..."
                                />
                            </div>
                        ) : (
                                <div></div>
                            )}

                        <div className="form-group" style={{ height: 10 }}>
                            <label htmlFor="fileUpload1" className="btn">
                                Select Gallary Image
                            </label>
                            <input
                                id="fileUpload1"
                                style={{ visibility: 'hidden' }}
                                type="file"
                                className="form-control"
                                onChange={onGallaryImgUploaded}
                            />
                        </div>
                    </div>
                    <div style={{ margin: 20 }}>
                        {isGallaryImage ? (
                            <div>
                                <div>Gallary Image2</div>
                                <img
                                    style={{ margin: 5 }}
                                    width={100}
                                    src={filePath3}
                                    alt="..."
                                />
                            </div>
                        ) : (
                                <div></div>
                            )}

                        <div className="form-group" style={{ height: 10 }}>
                            <label htmlFor="fileUpload3" className="btn">
                                Select Gallary Image
                            </label>
                            <input
                                id="fileUpload3"
                                style={{ visibility: 'hidden' }}
                                type="file"
                                className="form-control"
                                onChange={onGallaryImgUploaded3}
                            />
                        </div>
                    </div>
                </div>
                <div className="flex flex-row">
                    <div style={{ margin: 20 }}>
                        {isGallaryImage ? (
                            <div>
                                <div>Gallary Image3</div>
                                <img
                                    style={{ margin: 5 }}
                                    width={100}
                                    src={filePath4}
                                    alt="..."
                                />
                            </div>
                        ) : (
                                <div></div>
                            )}

                        <div className="form-group" style={{ height: 10 }}>
                            <label htmlFor="fileUpload4" className="btn">
                                Select Gallary Image
                            </label>
                            <input
                                id="fileUpload4"
                                style={{ visibility: 'hidden' }}
                                type="file"
                                className="form-control"
                                onChange={onGallaryImgUploaded4}
                            />
                        </div>
                    </div>
                    <div style={{ margin: 20 }}>
                        {isGallaryImage ? (
                            <div>
                                <div>Gallary Image4</div>
                                <img
                                    style={{ margin: 5 }}
                                    width={100}
                                    src={filePath5}
                                    alt="..."
                                />
                            </div>
                        ) : (
                                <div></div>
                            )}

                        <div className="form-group" style={{ height: 10 }}>
                            <label htmlFor="fileUpload5" className="btn">
                                Select Gallary Image
                            </label>
                            <input
                                id="fileUpload5"
                                style={{ visibility: 'hidden' }}
                                type="file"
                                className="form-control"
                                onChange={onGallaryImgUploaded5}
                            />
                        </div>
                    </div>
                </div>
            </div>

            <div className="flex flex-wrap -mx-2">
                <div className="w-1/2 px-2">
                    <div className="w-full border p-2">
                        <label className="w-full">Primary color</label>
                        <ColorBox defaultValue="#fdc109">
                            <Validator>
                                <RequiredRule message="Primary color is required" />
                            </Validator>
                        </ColorBox>
                    </div>
                </div>
                <div className="w-1/2 px-2">
                    <div className="w-full border p-2">
                        <label className="w-full">Secondary color</label>
                        <ColorBox defaultValue="#00bcd5">
                            <Validator>
                                <RequiredRule message="Secondary color is required" />
                            </Validator>
                        </ColorBox>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LogoDetails;
