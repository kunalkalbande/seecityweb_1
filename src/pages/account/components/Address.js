import React, { useState, useEffect } from 'react';
import { SelectBox, TextBox, Validator, NumberBox } from 'devextreme-react';
//import states from '../../../data/states';
import { RequiredRule } from 'devextreme-react/data-grid';
import { findByKey } from '../../../utils/index';
import { lookup } from '../../../api/graphql/lookup';

const Address = (props) => {
    const [states, setStates] = useState([]);

    const [addressLine1, setAddressLine1] = useState('');
    const [addressLine2, setAddressLine2] = useState('');
    const [state, setState] = useState('');
    const [city, setCity] = useState('');
    const [zip, setZip] = useState('');

    useEffect(() => {
        const init = async () => {
            const data = await lookup.getLookupByType("States");
            setStates(data);
            await fetchData(data);
        }
        const fetchData = async (states) => {
            if (props.setupDetail != null) {
                if (props.setupDetail.address != null) {
                    var addressDetail = {};
                    if (props.isFromPhone) {
                        addressDetail = props.setupDetail.addressTwo;
                    } else {
                        addressDetail = props.setupDetail.address;
                    }
                    if (addressDetail != null) {
                        setAddressLine1(addressDetail.addressOne);
                        setAddressLine2(addressDetail.addressTwo);
                        setState(findByKey(addressDetail.stateId, states));
                        setCity(addressDetail.city);
                        setZip(addressDetail.zip);
                    }
                }
            }
        };
        init();
        //fetchData();
    }, []);

    useEffect(() => {
        var address = {};
        address.addressOne = addressLine1;
        address.addressTwo = addressLine2;
        address.city = city;
        address.stateId = state;
        address.zip = zip;
        props.onChange(address);
    }, [addressLine1, addressLine2, city, state, zip]);

    return (
        <div div id="address" className="">
            <div className="flex flex-wrap">
                <div className="w-full mb-3">
                    <TextBox
                        id="address-line-1"
                        type="text"
                        name="addressLine1"
                        placeholder="Address Line 1"
                        showClearButton={true}
                        value={addressLine1}
                        onValueChanged={(e) => setAddressLine1(e.value)}
                    >
                        <Validator>
                            <RequiredRule message="Address Line 1 is required" />
                        </Validator>
                    </TextBox>
                </div>
                <div className="w-full mb-3">
                    <TextBox
                        id="address-line-2"
                        type="text"
                        name="addressLine2"
                        showClearButton={true}
                        value={addressLine2}
                        placeholder="Address Line 2"
                        onValueChanged={(e) => setAddressLine2(e.value)}
                    >
                        <Validator>
                            <RequiredRule message="Address Line 2 is required" />
                        </Validator>
                    </TextBox>
                </div>
            </div>
            <div className="flex flex-wrap -mx-3 mb-3">
                <div className="w-4/12 px-3">
                    <TextBox
                        id="city"
                        type="text"
                        name="city"
                        showClearButton={true}
                        value={city}
                        placeholder="City"
                        onValueChanged={(e) => setCity(e.value)}
                    >
                        <Validator>
                            <RequiredRule message="City is required" />
                        </Validator>
                    </TextBox>
                </div>
                <div className="w-4/12 pr-3">
                    <div className="w-full">
                        <SelectBox
                            dataSource={states}
                            placeholder="State"
                            displayExpr="name"
                            valueExpr="value"
                            value={state.value}
                            showClearButton={true}
                            onValueChanged={(e) =>
                                setState(e.value ? e.value : '')
                            }
                        >
                            <Validator>
                                <RequiredRule message="State is required" />
                            </Validator>
                        </SelectBox>
                    </div>
                </div>
                <div className="w-4/12 pr-3">
                    <NumberBox
                        id="zip"
                        type="number"
                        name="zip"
                        showClearButton={true}
                        value={zip}
                        placeholder="Zip"
                        onValueChanged={(e) => setZip(e.value)}
                    >
                        <Validator>
                            <RequiredRule message="Zip is required" />
                        </Validator>
                    </NumberBox>
                </div>
            </div>
        </div>
    );
};

export default Address;
