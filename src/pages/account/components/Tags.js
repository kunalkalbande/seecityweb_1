import React, { useState, useEffect } from 'react';
import { TagBox } from 'devextreme-react/tag-box';
//import tagList from '../../../data/tagsList';
import { Validator } from 'devextreme-react';
import { RequiredRule } from 'devextreme-react/data-grid';
import { lookup } from '../../../api/graphql/lookup';
const names = ['Outdoor', 'Animals', 'Zoo', 'Family', 'Kids', 'Downtown'];

const Tags = (props) => {
    const [tagList, setTagList] = useState([]);

    const [isAddTags, setIsAddTags] = useState(false);
    const [tags, setTags] = useState('');

    useEffect(() => {
        const fetchData = async () => {
            const data = await lookup.getLookupByType("Business Tags")
            setTagList(data);

            if (props.setupDetail != null) {
                setTags(props.setupDetail.tags);
            }
        };
        fetchData();
    }, []);

    useEffect(() => {
        props.onChange(tags);
    }, [tags]);

    return (
        <div id="tags" className="p-3">
            <div className="flex flex-wrap -mx-3">
                <div className="w-full px-3 my-2">
                    <div className="text-gray-500 ">
                        Pick the tags that best describes your business
                    </div>
                </div>
                <div className="w-full px-3 my-2">
                    <div className="w-full">
                        <div
                            className={
                                isAddTags
                                    ? 'justify-center items-center -mt-48 flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none'
                                    : ''
                            }
                        >
                            <div className={isAddTags ? 'w-full max-w-xs' : ''}>
                                <div
                                    onClick={() => setIsAddTags(false)}
                                    className={
                                        isAddTags
                                            ? 'block cursor-pointer'
                                            : 'hidden'
                                    }
                                >
                                    <div
                                        className="w-full p-2 text-center items-center bg-blue-500 rounded text-white text-bold"
                                        style={{ backgroundColor: '#3db5e7' }}
                                    >
                                        Close
                                    </div>
                                </div>
                                <TagBox
                                    items={tagList}
                                    value={tags}
                                    displayExpr="name"
                                    valueExpr="value"
                                    onValueChanged={(e) => setTags(e.value)}
                                    applyValueMode="useButtons"
                                    //tagRender={renderTag}
                                    showClearButton={true}
                                    showSelectionControls={true}
                                    hideSelectedItems={true}
                                    showDropDownButton={true}
                                >
                                    <Validator>
                                        <RequiredRule message="Business Tag is required" />
                                    </Validator>
                                </TagBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    className={
                        isAddTags
                            ? 'opacity-25 fixed inset-0 z-40 bg-black'
                            : 'hidden'
                    }
                />
            </div>
        </div>
    );
};

export default Tags;
