import React, { useState, useEffect } from 'react';
import { ColorBox, RadioGroup, NumberBox, Validator } from 'devextreme-react';
import SignatureBox from './SignatureBox';
import { RequiredRule } from 'devextreme-react/data-grid';

const BankingDetails = (props) => {
    const [routingNumber, setRoutingNumber] = useState('');
    const [accountNumber, setAccountNumber] = useState('');
    const [nameOnCheck, setNameOnCheck] = useState('');
    const [checkTextOne, setCheckTextOne] = useState('');
    const [checkTextTwo, setCheckTextTwo] = useState('');
    const [checkAmount, setCheckAmount] = useState('');
    const [bankAccountType, setBankAccountType] = useState('Savings');

    useEffect(() => {
        const fetchData = () => {
            if (props.setupDetail != null) {
                if (props.setupDetail.bankdetail != null) {
                    var bankDetail = props.setupDetail.bankdetail;
                    setRoutingNumber(bankDetail.routingNumber);
                    setAccountNumber(bankDetail.accountNumber);
                    setNameOnCheck(bankDetail.nameOnCheck);
                    setCheckTextOne(bankDetail.checkTextOne);
                    setCheckTextTwo(bankDetail.checkTextTwo);
                    setCheckAmount(bankDetail.checkAmount);
                    setBankAccountType(bankDetail.bankAccountType);
                }
            }
        };
        fetchData();
    }, []);

    useEffect(() => {
        var bankDetail = {};
        bankDetail.routingNumber = routingNumber;
        bankDetail.accountNumber = accountNumber;
        bankDetail.bankAccountType = bankAccountType;
        props.onChange(bankDetail);
    }, [routingNumber, accountNumber, bankAccountType]);

    return (
        <div id="banking-details" className="p-3 bg-white sm:max-w-md">
            <div className="text-center">
                {/* <div className="text-2xl text-center font-bold mb-7">
					Link Bank Account
				</div> */}
                <div className="text-sm m-3 mt-0">
                    Provide your bank account details where you would like your
                    earning to be deposited
                </div>
            </div>
            <div className="py-5">
                <RadioGroup
                    items={['Checking', 'Savings']}
                    value={bankAccountType}
                    onValueChanged={(e) => setBankAccountType(e.value)}
                    layout="horizontal"
                />
            </div>
            <div className="flex flex-wrap">
                <div className="w-full mb-3">
                    <NumberBox
                        id="routingNumber"
                        type="number"
                        name="routingNumber"
                        showClearButton={true}
                        value={routingNumber}
                        placeholder="Routing Number"
                        onValueChanged={(e) => setRoutingNumber(e.value)}
                    >
                        <Validator>
                            <RequiredRule message="Routing Number is required" />
                        </Validator>
                    </NumberBox>
                </div>
                <div className="w-full mb-3">
                    <NumberBox
                        id="accountNumber"
                        type="number"
                        name="accountNumber"
                        showClearButton={true}
                        value={accountNumber}
                        placeholder="Account Number"
                        onValueChanged={(e) => setAccountNumber(e.value)}
                    >
                        <Validator>
                            <RequiredRule message="Account Number is required" />
                        </Validator>
                    </NumberBox>
                </div>
                <div className="w-full flex-col flex">
                    <div className="text-gray-400 w-full text-xs py-1 text-center">
                        By completing signing up,you agree to the{' '}
                        <a href="#" style={{ color: '#5cb2cc' }}>
                            Privacy Policy
                        </a>{' '}
                        and the{' '}
                        <a href="#" style={{ color: '#5cb2cc' }}>
                            FAQ
                        </a>
                    </div>
                </div>
            </div>
            {/* <div className="flex items-center justify-center pt-4">
				<div className="">
					<button
						type="submit"
						id="btn-create-account"
						className="bg-blue-500 hover:bg-blue-700 text-white text-sm font-semibold py-2 px-4 rounded w-full"
						style={{
							backgroundColor: "#3db5e7",
						}}
					>
						CREATE ACCOUNT →
            </button>
				</div>
			</div> */}
        </div>
    );
};

export default BankingDetails;
