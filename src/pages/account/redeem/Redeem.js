import React, { useEffect, useRef, useState } from 'react';
import {
	Button,
	TextBox,
	ValidationSummary,
	SelectBox,
	Switch,
	Slider,
	TextArea
} from 'devextreme-react';
import {
	RequiredRule,
	ValidationRule,
	Validator,
} from 'devextreme-react/validator';

const RedeemPage = () => {
	const [minutesBefore, setMinutesBefore] = useState(0);
	const [minutesAfter, setMinutesAfter] = useState(0);
	const [product, setProduct] = useState();
	const [manualBarcode, setManualBarcode] = useState('');
	const [scanAllTicket, setScanAllTicket] = useState('')

	const handleSubmit = (e) => {
		e.preventDefault();
	}

	const resetForm = () => {
		setMinutesBefore(0);
		setMinutesAfter(0);
		setManualBarcode('');
		setScanAllTicket('')
		setProduct();
	}

	return (
		<div className="mx-2 flex flex-col overflow-y-hidden">
			<div className="mb-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
				<div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6">
					<h3 className="text-lg leading-6 font-medium text-sc-blue-500">
						Redeem
                    </h3>
				</div>

				<form
					onSubmit={handleSubmit}
					className="w-full h-full"
				>
					<div className="h-full flex flex-1 flex-col">
						<div className="h-full">
							<div className="mt-4 mx-2 sm:mx-5 grid grid-cols-1 row-gap-6 col-gap-4 sm:grid-cols-2">
								<div className="text-sm text-teal-600 font-semibold">
									Accept and Redeem Active Products
								</div>

								<div className="sm:col-span-2">
									<label
										htmlFor="name"
										className="block text-sm font-medium leading-5 text-gray-400"
									>
										Redeem Products
									</label>
									<div className="mt-1 sm:w-1/2">
										<SelectBox
											dataSource={null}
											placeholder="Choose products"
											displayExpr="name"
											valueExpr="value"
											showClearButton={true}
										>
										</SelectBox>
									</div>
								</div>

								<div className="sm:col-span-2">
									<label
										htmlFor="name"
										className="block text-sm font-medium leading-5 text-gray-400"
									>
										Redeem Ticket?
									</label>
									<div className="mt-1 sm:w-1/2">
										<Switch
											width="80px"
											defaultValue={false}
											switchedOffText="No"
											switchedOnText="Yes"
										/>
									</div>
								</div>

								<div className="sm:col-span-2">
									<label
										htmlFor="name"
										className="block text-sm font-medium leading-5 text-gray-400"
									>
										Minutes Before
									</label>
									<div className="mt-1 sm:w-1/2">
										<div className="flex flex-wrap w-full">
											<div className="w-9/12 sm:w-10/12">
												<Slider
													min={0}
													max={100}
													defaultValue={0}
													step={1} tooltip={{ enabled: true }}
													value={minutesBefore}
													onValueChanged={(e) => setMinutesBefore(e.value)}
												/>
											</div>
											<div className="w-3/12 sm:w-2/12 px-2 text-sm font-medium leading-5 text-gray-400">{minutesBefore} minute</div>
										</div>
									</div>
								</div>

								<div className="sm:col-span-2">
									<label
										htmlFor="name"
										className="block text-sm font-medium leading-5 text-gray-400"
									>
										Minutes After
									</label>
									<div className="mt-1 sm:w-1/2">
										<div className="flex flex-wrap w-full">
											<div className="w-9/12 sm:w-10/12">
												<Slider
													min={0}
													max={100}
													defaultValue={0}
													step={1} tooltip={{ enabled: true }}
													value={minutesAfter}
													onValueChanged={(e) => setMinutesAfter(e.value)}
												/>
											</div>
											<div className="w-3/12 sm:w-2/12 px-2 text-sm font-medium leading-5 text-gray-400">{minutesAfter} minute</div>
										</div>
									</div>
								</div>

								<div className="sm:col-span-2">
									<label
										htmlFor="name"
										className="block text-sm font-medium leading-5 text-gray-400"
									>
										Manual Barcode
									</label>
									<div className="mt-1 sm:w-1/2">
										<TextBox
											id="manualBarcode"
											type="text"
											name="manualBarcode"
											value={manualBarcode}
											onValueChanged={(e) => setManualBarcode(e.value)}
										/>
									</div>
								</div>

								<div className="sm:col-span-2">
									<label
										htmlFor="name"
										className="block text-sm font-medium leading-5 text-gray-400"
									>
										Scan All Tickets
									</label>
									<div className="mt-1 sm:w-1/2">
										<TextArea
											className="h-32 border"
											placeholder=""
											rows="6"
											value={scanAllTicket}
											onValueChanged={(e) => setScanAllTicket(e.value)}
										></TextArea>
									</div>
								</div>

								<div className="sm:col-span-2">
									<ValidationSummary id="summary"></ValidationSummary>
								</div>
							</div>
						</div>
						<div className="px-2 py-4 border-t border-gray-200 sm:px-4 flex flex-col items-end">
							<div className="text-right">
								<Button
									className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
									useSubmitBehavior={true}
									text="SUBMIT"
								/>
								<Button
									className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-gray-dark shadow-sm hover:bg-sc-gray-light focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
									type="normal"
									text="RESET"
									onClick={resetForm}
								/>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	);
}

export default RedeemPage;