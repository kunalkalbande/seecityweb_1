import React, { useState ,useEffect} from 'react';
import { TagBox, TextBox,RadioGroup,CheckBox, TextArea, SelectBox,Button } from 'devextreme-react';
import { useDispatch, useSelector } from 'react-redux';
import {findByKey} from '../../../../utils'
import { changeRequest } from '../../../../api/graphql/changeRequest';
import { AccountList } from '../../../account-list/components';


export const EditAccountDetails=({setupDetail,tagsList,statesList,accountTagsList,onEdit})=>{
    const [setup, setSetup] = useState(setupDetail);
    const [accountType,setAccountType]=useState('Attraction/Experience')
    const [companyName,setCompanyName]=useState(setupDetail.companyName)
    const [venueName,setVenueName]=useState('')
    const [address1,setAddress1]=useState('')
    const [address2,setAddress2]=useState('')
    const [phoneNumber,setPhoneNumber]=useState('')
    const [tagList, setTagList] = useState(tagsList);
    const [tags, setTags] = useState(setup.tags);
    const[operatingPolicy,setOperatingPolicy]=useState('');
    const [accountTypes, setAccountTypes] = useState([]);
    const [bankAccountType, setBankAccountType] = useState('');
    const [accountNumber, setAccountNumber] = useState('');
    const [routingNumber, setRoutingNumber] = useState('');
    const [brandDetail, setBrandDetail] = useState('');
    const [isProviderSeller, setIsProviderSeller] = useState(false);
    const [isSeller, setIsSeller] = useState(false);
    const [state, setState] = useState('');
    const [city, setCity] = useState('');
    const [zip, setZip] = useState('');
    const [states, setStates] = useState([]);
    const [editMode, setEditMode] = useState(true);
    const currentUser = useSelector((state) => state.user);
    const [fields,setFields]=useState([]);
   
    const phoneRules = { 'X': /[02-9]/ };
    const changeCompanyName=(value)=>{
        setCompanyName(value);
        addField("Legal Company Name",value,setup.companyName)
        console.log(fields);

    };
    const changeAddressOne=(value)=>{
        setAddress1(value);
        console.log(setup.address);
        addField("Address Line One",value,setup.address.addressOne)
        console.log(fields);

    };
    const changeAddressTwo=(value)=>{
        setAddress2(value);
        addField("Address Line Two",value,setup.address.addressTwo)
        console.log(fields);

    };
    const changeCity=(value)=>{
        setCity(value);
        addField("City",value,setup.address.city)
        console.log(fields);

    };
    const changeState=(value)=>{
        setState(value);
        addField("State",value,setup.address.stateId)
        console.log(fields);

    };
    const changeZip=(value)=>{
        setZip(value);
        addField("Zip",value,setup.address.zip)
        console.log(fields);

    };
    const changeTag=(value)=>{
        console.log(value);
        setTags(value);
        addField("Business Tags",value,setup.tags)
        console.log(fields);

    };
    const changeAccountType=(e)=>{
        console.log(e);
        setAccountType( e.value?findByKey(e.value,accountTypes):'');
        addField("Account Type",e.value?e.value:setup.accountTypeId,setup.accountTypeId)
        console.log(fields);

    };
    const changeVenueName=(value)=>{
        setVenueName(value);
        addField("VenueName",value,setup.venueName)
        console.log(fields);

    }
    const changePhoneNumber=(value)=>{
        setPhoneNumber(value);
        addField("Phone Number",value,setup.address.phoneNumber)
        console.log(fields);

    }
    const changeOperatingPeriod=(value)=>{
        setOperatingPolicy(value);
        addField("Operating Period Days/Hour",value,setup.operatingPolicy)
        console.log(fields);

    }
    const changeBrandDetails=(value)=>{
        setBrandDetail(value);
        addField("Brand Details",value,setup.brandDetail)
        console.log(fields);

    }
    const changeSeller=(value)=>{
        setIsSeller(value);
        addField("Seller",value?value:'false',setup.isSeller?setup.isSeller:'false')
        console.log(fields);

    }
    const changeSellerProvider=(value)=>{
        setIsProviderSeller(value);
        addField("Provider/Seller",value?value:'false',setup.isProviderSeller?setup.isProviderSeller:'false')
        console.log(fields);

    }
    const addField = (name, arg1, arg2) => {
        var existing = fields.filter((f) => f.field === name);
        if (existing && existing.length>0) {
            console.log(existing)
            fields.splice(existing);
        }
        if (arg1 !== arg2) {
            var changeRequestField = {};
            changeRequestField.field = name;
            changeRequestField.currentValue = arg2?arg2.toString():'';
            changeRequestField.newValue = arg1?arg1.toString():'';
            
            console.log("beforePush",fields.length)
            fields.push(changeRequestField);
        }
    };
    const save=async()=>{
        console.log(setup);
        var changeRequestObj = {};

        changeRequestObj.accountId = currentUser.bag.accountId;
        changeRequestObj.status = 'Pending';
        changeRequestObj.type = 'Account Adjustment';
        changeRequestObj.fields = fields;
        console.log(changeRequestObj);
        var res = await changeRequest.saveChangeRequests(
            changeRequestObj
        );
        if (res) {
            alert(
                'Account Adjustment has been changed and sent for review.'
            );
        } else {
            alert('error');
        }
        setEditMode(false);
    }
    useEffect(()=>{
        onEdit(editMode);
    },[editMode])
    useEffect(() => {
        const fetchData =  () => {
            console.log("detail :",setupDetail);
            if (setupDetail != null) {
                setTags(setup.tags);
                setStates(statesList);
                setBrandDetail(setup.brandDetail);
                setIsProviderSeller(setup.isProviderSeller);
                setIsSeller(setup.isSeller);
                console.log(accountTagsList);
                setAccountTypes(accountTagsList);
                console.log(setup.accountTypeId);
                console.log( findByKey(setup.accountTypeId, accountTagsList));
                setAccountType(
                    findByKey(setup.accountTypeId, accountTagsList)
                );
                //setCompanyName(setup.companyName);
                setVenueName(setup.venueName);
                setOperatingPolicy(setup.operatingPolicy);
                
                if(setup.address)
                {
                setAddress1(setup.address.addressOne);
                setAddress2(setup.address.addressTwo);
                setState(setup.address.stateId);
                setCity(setup.address.city);
                setZip(setup.address.zip);
                setPhoneNumber(setup.address.phoneNumber);
                }
              
            }

        };
        fetchData();
    }, []);
    return( <div className="mx-2 flex flex-col overflow-y-hidden">
              <div class="sm:grid sm:grid-cols-5 mb-2 sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700 ">
                Account Type
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
              <div className="w-full">
                    <SelectBox
                        dataSource={accountTypes}
                        placeholder="Choose One Account Type"
                        displayExpr="name"
                        valueExpr="value"
                        value={accountType.value}
                        showClearButton={true}
                        onValueChanged={(e) =>
                            changeAccountType(e)
                        }
                    >
                      
                    </SelectBox>
                </div>
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700">
                Legal Company Name
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
                <TextBox value={companyName} onValueChanged={(e)=>changeCompanyName(e.value)}/>
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2 sm:items-start">
              <label class="block text-sm font-medium leading-5  text-gray-700 ">
                    VenueName
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
              <TextBox value={venueName} onValueChanged={(e)=>changeVenueName(e.value)}/>  
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700 ">
                    Address
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
                <TextBox value={address1} onValueChanged={(e)=>changeAddressOne(e.value)}/>
                <TextBox value={address2} className="mt-1" onValueChanged={(e)=>changeAddressTwo(e.value)}/>
                <div class="sm:grid sm:grid-cols-3  mt-1 sm:items-start">
                    <TextBox value={city} className="mr-5" onValueChanged={(e)=>changeCity(e.value)}/>
                    <SelectBox
                            dataSource={states}
                            placeholder="State"
                            displayExpr="name"
                            valueExpr="value"
                            value={state}
                            showClearButton={true}
                            onValueChanged={(e) =>
                                changeState(e.value ? e.value : '')
                            }
                        />
                    <TextBox value={zip} onValueChanged={(e)=>changeZip(e.value)}/>
                </div>
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700 ">
                    Phone Number
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
                <TextBox  mask="+1 (X00) 000-0000"  onValueChanged={(e)=>changePhoneNumber(e.value)}
                        maskRules={phoneRules} value={phoneNumber}/>
                   
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700 ">
              Operating Days/Hours
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
                <TextArea value={operatingPolicy} onValueChanged={(e)=>changeOperatingPeriod(e.value)}/>
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700 ">
              My Business Tags
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
              <TagBox 
                                    items={tagList}
                                    value={tags}
                                    displayExpr="name"
                                    valueExpr="value"
                                    hideSelectedItems={true}
                                    onValueChanged={(e)=>changeTag(e.value)}
                                  //  onValueChanged={(e)=>{setTags(e.value)}}
                                />
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700 ">
                   Brand Details
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
                <TextArea value={brandDetail} onValueChanged={(e)=>changeBrandDetails(e.value)}/>
                   
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700 ">
                   Selling Mode
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
              <div className="flex items-left">
                <CheckBox
                    text=""
                    value={isProviderSeller}
                    onValueChanged={(e)=>changeSellerProvider(e.value)}
                />
                <label className="flex justify-start items-start ml-2">
                    <div className="select-none text-gray-500">
                        I am a <span className="underline">Provider</span> and a{' '}
                        <span className="underline">Seller</span> and I intend
                        to offer product for sale while also able to sell others{' '}
                    </div>
                </label>
            </div>
            <div className="flex items-left mt-2">
                <CheckBox
                    text=""
                    value={isSeller}
                    onValueChanged={(e)=>changeSeller(e.value)}
                />
                <label className="flex justify-start items-start ml-2">
                    <div className="select-none text-gray-500">
                        I am a <span className="underline">Seller</span> and
                        only I intend to sell other.{' '}
                    </div>
                </label>
            </div>
                   
              </div>
            </div>
            <div className="bg-white px-2 py-4 border-t border-gray-200 sm:px-4 flex flex-col items-end">
                                <div className="text-right">
                                    <Button
                                        className="w-50 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                                       onClick={save}
                                        text="Submit For Review"
                                    />
                                    <Button
                                        className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-gray-dark shadow-sm hover:bg-sc-gray-light focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                                        type="normal"
                                        text="Cancel"
                                        onClick={()=>setEditMode(false)}
                                    />
                                </div>
                            </div>
    </div>);
}