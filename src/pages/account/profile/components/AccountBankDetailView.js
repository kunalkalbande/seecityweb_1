import React, { useState, useEffect } from 'react';
import { RadioGroup, TextBox } from 'devextreme-react';

export const AccountBankDetailsView = (setupDetail) => {

  const [bankAccountType, setBankAccountType] = useState('');
  const [accountNumber, setAccountNumber] = useState('');
  const [routingNumber, setRoutingNumber] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      console.log(setupDetail);
      if (setupDetail.setupDetail != null) {

        if (setupDetail.setupDetail.bankdetail) {
          setBankAccountType(setupDetail.setupDetail.bankdetail.bankAccountType)
          setAccountNumber(setupDetail.setupDetail.bankdetail.accountNumber)
          setRoutingNumber(setupDetail.setupDetail.bankdetail.routingNumber)
        }

      }

    };
    fetchData();
  }, []);

  const accountMask = (num) => {
    num = num + '';
    var res = '';
    for (let i = 0; i < num.length - 4; i++) {
      res = res + '🞶';
    }
    var temp = num.slice(num.length - 4, num.length);
    res = res + temp;
    return res;
  }

  return (<div className="mx-2 flex flex-col overflow-y-hidden">
    <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
      <label class="block text-sm font-medium leading-5 text-gray-700 ">
        Bank Account Type
              </label>
      <div class="mt-1 sm:mt-0 sm:col-span-4">
        <RadioGroup
          style={{ opacity: 1 }}
          disabled={true}
          items={['Checking', 'Savings']}
          value={bankAccountType}
          layout="horizontal"
        />
      </div>
    </div>
    <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
      <label class="block text-sm font-medium mt-2 leading-5 text-gray-700 ">
        Account Number
              </label>
      <div class="mt-1 sm:mt-0 sm:col-span-4">
        <TextBox stylingMode="underlined" style={{ border: "0px" }} readOnly={true} value={accountMask(accountNumber)} />

      </div>
    </div>
    <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
      <label class="block text-sm font-medium mt-2 leading-5 text-gray-700 ">
        Routing Number
              </label>
      <div class="mt-1 sm:mt-0 sm:col-span-4">
        <TextBox stylingMode="underlined" readOnly={true} style={{ border: "0px" }} value={accountMask(routingNumber)} />

      </div>
    </div>
  </div>);
}