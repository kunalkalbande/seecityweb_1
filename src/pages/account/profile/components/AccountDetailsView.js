import React, { useState ,useEffect} from 'react';
import { TagBox, TextBox,RadioGroup,CheckBox } from 'devextreme-react';
import { lookup } from '../../../../api/graphql/lookup';
import {findByKey} from '../../../../utils'

export const AccountDetailsView=(setupDetail)=>{
    const [accountType,setAccountType]=useState('Attraction/Experience')
    const [companyName,setCompanyName]=useState('sp')
    const [venueName,setVenueName]=useState('')
    const [address1,setAddress1]=useState('')
    const [address2,setAddress2]=useState('')
    const [phoneNumber,setPhoneNumber]=useState('')
    const [tagList, setTagList] = useState(['Outdoor', 'Animals', 'Zoo', 'Family', 'Kids', 'Downtown']);
    const [tags, setTags] = useState('');
    const[operatingPolicy,setOperatingPolicy]=useState('');
    const [bankAccountType, setBankAccountType] = useState('');
    const [accountNumber, setAccountNumber] = useState('');
    const [routingNumber, setRoutingNumber] = useState('');
    const [brandDetail, setBrandDetail] = useState('');
    const [isProviderSeller, setIsProviderSeller] = useState(false);
    const [isSeller, setIsSeller] = useState(false);
    const phoneRules = { 'X': /[02-9]/ };

    useEffect(() => {
        const fetchData = async () => {
            console.log(setupDetail);
            setTagList(setupDetail.tagsList);
            if (setupDetail.setupDetail != null) {
                setTags(setupDetail.setupDetail.tags);
                setBrandDetail(setupDetail.setupDetail.brandDetail);
                setIsProviderSeller(setupDetail.setupDetail.isProviderSeller);
                setIsSeller(setupDetail.setupDetail.isSeller);
                console.log( findByKey(setupDetail.setupDetail.accountTypeId, setupDetail.accountTagsList));
                setAccountType(
                    findByKey(setupDetail.setupDetail.accountTypeId, setupDetail.accountTagsList)
                );
                setCompanyName(setupDetail.setupDetail.companyName);
                setVenueName(setupDetail.setupDetail.venueName);
                setOperatingPolicy(setupDetail.setupDetail.operatingPolicy);
                
                if(setupDetail.setupDetail.address)
                {
                setAddress1(setupDetail.setupDetail.address.addressOne);
                setAddress2(setupDetail.setupDetail.address.addressTwo);
                setPhoneNumber(setupDetail.setupDetail.address.phoneNumber);
                }
                if(setupDetail.setupDetail.bankdetail)
                {
                  setBankAccountType(setupDetail.setupDetail.bankdetail.bankAccountType)
                  setAccountNumber(setupDetail.setupDetail.bankdetail.accountNumber)
                  setRoutingNumber(setupDetail.setupDetail.bankdetail.routingNumber)
                }
                
            }

        };
        fetchData();
    }, []);
    return( <div className="mx-2 flex flex-col overflow-y-hidden">
              <div class="sm:grid sm:grid-cols-5 mb-2 sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700 ">
                Account Type
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
                <label>{accountType.name}</label> 
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700">
                Legal Company Name
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
                <label>{companyName}</label>
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2 sm:items-start">
              <label class="block text-sm font-medium leading-5  text-gray-700 ">
                    Venue Name
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
              <label>{venueName}</label>  
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5  sm:items-start">
              <label class="block text-sm font-medium leading-5  text-gray-700 ">
                    Address
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
                <label>{address1}<br></br>
                        {address2}
                    </label> 
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5   sm:items-start">
              <label class="block text-sm font-medium mt-2 leading-5 text-gray-700 ">
                    Phone Number
              </label>
              <div class="mt-0  sm:mt-0 sm:col-span-4">
                <TextBox stylingMode="underlined" style={{border:"0px",padding:"0px"}} readOnly={true}  maskRules={phoneRules} mask="+1 (X00) 000-0000" value={phoneNumber}/>
                   
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700 ">
              Operating Days/Hours
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
                <label>{operatingPolicy}
                    </label> 
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700 ">
               Business Tags
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
              <TagBox style={{opacity: 1}}
                                    items={tagList}
                                    value={tags}
                                    displayExpr="name"
                                    valueExpr="value"
                                    disabled={true}
                                    hideSelectedItems={true}
                                />
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700 ">
                   Brand Details
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
                <label>{brandDetail}</label>
                   
              </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
              <label class="block text-sm font-medium leading-5 text-gray-700 ">
                   Selling Mode
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-4">
              <div className="flex items-left">
                <CheckBox
                    text=""
                    style={{opacity: 1}}
                    value={isProviderSeller}
                   disabled={true}
                />
                <label className="flex justify-start items-start ml-2">
                    <div className="select-none text-gray-500">
                        I am a <span className="underline">Provider</span> and a{' '}
                        <span className="underline">Seller</span> and I intend
                        to offer product for sale while also able to sell others{' '}
                    </div>
                </label>
            </div>
            <div className="flex items-left mt-2">
                <CheckBox
                    text=""
                    style={{opacity: 1}}
                    value={isSeller}
                    disabled={true}
                />
                <label className="flex justify-start items-start ml-2">
                    <div className="select-none text-gray-500">
                        I am a <span className="underline">Seller</span> and
                        only I intend to sell other.{' '}
                    </div>
                </label>
            </div>
                   
              </div>
            </div>
    </div>);
}