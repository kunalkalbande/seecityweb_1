import React, { useState ,useEffect} from 'react';
import { TagBox, TextBox,RadioGroup,CheckBox, TextArea, SelectBox,Button } from 'devextreme-react';
import {findByKey} from '../../../../utils'
import { useSelector } from 'react-redux';
import { changeRequest } from '../../../../api/graphql/changeRequest';

export const EditBankAccountDetails=({setupDetail,tagsList,statesList,accountTagsList,onEdit})=>{
    const [setup, setSetup] = useState(setupDetail);
    const [bankAccountType, setBankAccountType] = useState('');
    const [accountNumber, setAccountNumber] = useState('');
    const [editMode, setEditMode] = useState(true);
    const [routingNumber, setRoutingNumber] = useState('');
    const [fields,setFields]=useState([]);
    const currentUser = useSelector((state) => state.user);


    const changeAccountType=(value)=>{
        setBankAccountType(value);
        addField("Bank Account Type",value,setup.bankdetail.bankAccountType)
        console.log(fields);

    }
    const changeAccountNumber=(value)=>{
        setAccountNumber(value);
        addField("Account Number",value,setup.bankdetail.accountNumber)
        console.log(fields);

    }
    const changeRoutingNumber=(value)=>{
        setRoutingNumber(value);
        addField("Routing Number",value,setup.bankdetail.routingNumber)
        console.log(fields);

    }
    const addField = (name, arg1, arg2) => {
        var existing = fields.filter((f) => f.field === name);
        if (existing && existing.length>0) {
            console.log(existing)
            fields.splice(existing);
        }
        if (arg1 !== arg2) {
            var changeRequestField = {};
            changeRequestField.field = name;
            changeRequestField.currentValue = arg2?arg2.toString():'';
            changeRequestField.newValue = arg1?arg1.toString():'';
            
            console.log("beforePush",fields.length)
            fields.push(changeRequestField);
        }
    };
    const save=async()=>{
        console.log(setup);
        console.log(setupDetail);
        var changeRequestObj = {};

        changeRequestObj.accountId = currentUser.bag.accountId;
        changeRequestObj.status = 'Pending';
        changeRequestObj.type = 'BankAccount Adjustment';
        changeRequestObj.fields = fields;
        console.log(changeRequestObj);
        var res = await changeRequest.saveChangeRequests(
            changeRequestObj
        );
        if (res) {
            alert(
                'Account Adjustment has been changed and sent for review.'
            );
        } else {
            alert('error');
        }
        setEditMode(false);
    }
    useEffect(()=>{
        onEdit(editMode);
    },[editMode])
    useEffect(() => {
        const fetchData =  () => {
            console.log("detail :",setupDetail);
            if (setupDetail != null) {
                if(setup.bankdetail)
                {
                  setBankAccountType(setup.bankdetail.bankAccountType)
                  setAccountNumber(setup.bankdetail.accountNumber)
                  setRoutingNumber(setup.bankdetail.routingNumber)
                }
                
            }

        };
        fetchData();
    }, []);
    return (
        <div className="mx-2 flex flex-col overflow-y-hidden">
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label class="block text-sm font-medium leading-5 text-gray-700 ">
                    Bank Account Type
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-4">
                    <RadioGroup
                        items={['Checking', 'Savings']}
                        value={bankAccountType}
                        layout="horizontal"
                        onValueChanged={(e)=>changeAccountType(e.value)}
                    />
                </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label class="block text-sm font-medium leading-5 text-gray-700 ">
                    Account Number
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-4">
                    <TextBox value={accountNumber} 
                        onValueChanged={(e)=>changeAccountNumber(e.value)}
                        />
                </div>
            </div>
            <div class="sm:grid sm:grid-cols-5 mb-2  sm:items-start">
                <label class="block text-sm font-medium leading-5 text-gray-700 ">
                    Routing Number
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-4">
                    <TextBox value={routingNumber} 
                        onValueChanged={(e)=>changeRoutingNumber(e.value)}
                        />
                </div>
            </div>
            <div className="bg-white px-2 py-4 border-t border-gray-200 sm:px-4 flex flex-col items-end">
                <div className="text-right">
                    <Button
                        className="w-50 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                        onClick={save}
                        text="Submit For Review"
                    />
                    <Button
                        className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-gray-dark shadow-sm hover:bg-sc-gray-light focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                        type="normal"
                        text="Cancel"
                        onClick={() => setEditMode(false)}
                    />
                </div>
            </div>
        </div>
    );
}