import React, { useState, useEffect } from 'react';
import { FileUploader, ColorBox, Validator, Button } from 'devextreme-react';
import { RequiredRule } from 'devextreme-react/data-grid';
import { useDispatch, useSelector } from 'react-redux';
import { accountSetup } from '../../../../api/graphql/accountSetup';
import { from } from 'apollo-link';
import { changeRequest } from '../../../../api/graphql/changeRequest';

export const EditAccountBranding = (setupDetail) => {
    const [logo, setlogo] = useState([]);
    const [newValue, setNewValue] = useState('');
    const [filePath, setPath] = useState(
        'http://www.myiconfinder.com/uploads/iconsets/256-256-37736f35fd7d98b10b35287679b223b8.png'
    );
    const [filePath1, setPath1] = useState('');
    const [editMode, setEditMode] = useState(true);
    const currentUser = useSelector((state) => state.user);

    const save = async() => {
        console.log(setupDetail);
        var filedata=await accountSetup.uploadLogos(currentUser.bag.accountId,logo);
        console.log(filedata.uploadFile.file[0].path);
        setNewValue(filedata.uploadFile.file[0].path);
        console.log(newValue);
        await submitImageApi("/"+filedata.uploadFile.file[0].path);
        setEditMode(false);
    };
    const submitImageApi = (newFilePath) => {
        return new Promise((resolve) => {
            setTimeout(async () => {
                try {
                    if (
                        currentUser.bag != undefined &&
                        currentUser.bag.accountId != undefined
                    ) {
                        var changeRequestObj = {};
                        var fields = [];

                        changeRequestObj.accountId = currentUser.bag.accountId;
                        changeRequestObj.status = 'Pending';
                        changeRequestObj.type = 'Account Image Adjustment';

                        var changeRequestField = {};

                        changeRequestField.field = 'Account Image';
                        changeRequestField.currentValue =
                           filePath
                        changeRequestField.newValue =
                           newFilePath
                        fields.push(changeRequestField);
                        // var changeRequestField1 = {};

                        // changeRequestField1.field = 'Account Image';
                        // changeRequestField1.currentValue =
                        //     './images/5f02d23d93d193686cc921fa_guestx/_Gk09RWaX-NPKioskBackground_Halloween2019.png';
                        // changeRequestField1.newValue =
                        //     './images/5f02d23d93d193686cc921fa_guestx/WAnidNNWM_-NPKioskBackground.png';
                        // fields.push(changeRequestField1);
                        changeRequestObj.fields = fields;

                        var res = await changeRequest.saveChangeRequests(
                            changeRequestObj
                        );
                        if (res) {
                            resolve(res);
                            alert(
                                'Account Image has been changed and sent for review.'
                            );
                        } else {
                           
                            alert('error');
                        }
                    }

                    //
                } catch (error) {
                    console.error(error);

                    if (error.response) {
                        console.error(error.response.body);
                    }
                }
            }, 1000);
        });
    };
    useEffect(() => {
        setupDetail.onEdit(editMode);
    }, [editMode]);
    const onFileUploaded = async (args) => {
        document.getElementById('img1').src = URL.createObjectURL(
            args.value[0]
        );
        if (args.value.length > 1) {
            document.getElementById('divimg2').classList.remove('w-0');
            document.getElementById('divimg1').classList.add('w-1/2');
            document.getElementById('divimg2').classList.add('w-1/2');
            document.getElementById('img1').style.width = '70%';
            document.getElementById('img2').src = URL.createObjectURL(
                args.value[1]
            );
        } else {
            // document.getElementById('divimg1').classList.add('w-4/5');
            document.getElementById('divimg2').classList.add('w-0');
            document.getElementById('divimg1').classList.remove('w-1/2');
            document.getElementById('divimg2').classList.remove('w-1/2');
            document.getElementById('img1').style.width = '35%';
        }
        console.log(args.value);
        setlogo(args.value);
        //var res =await accountSetup.uploadLogos(args.value[0]);
        //console.log(res);
    };

    useEffect(() => {
        const init = () => {
            console.log(setupDetail);
            if (setupDetail.setupDetail != null) {
                if (
                    setupDetail.setupDetail.file != null &&
                    setupDetail.setupDetail.file.length > 0
                ) {
                    setPath('/' + setupDetail.setupDetail.file[0].path);
                    if (setupDetail.setupDetail.file.length > 1) {
                        //document.getElementById("img1").style.marginLeft="-120px";
                        document
                            .getElementById('divimg2')
                            .classList.remove('w-0');
                        document
                            .getElementById('divimg1')
                            .classList.add('w-1/2');
                        document
                            .getElementById('divimg2')
                            .classList.add('w-1/2');
                        document.getElementById('img1').style.width = '70%';
                        setPath1('/' + setupDetail.setupDetail.file[1].path);
                    } else {
                        document.getElementById('divimg2').classList.add('w-0');
                        document
                            .getElementById('divimg1')
                            .classList.remove('w-1/2');
                        document
                            .getElementById('divimg2')
                            .classList.remove('w-1/2');
                        document.getElementById('img1').style.width = '35%';
                    }
                } else {
                    document.getElementById('divimg2').classList.add('w-0');
                    document
                        .getElementById('divimg1')
                        .classList.remove('w-1/2');
                    document
                        .getElementById('divimg2')
                        .classList.remove('w-1/2');
                    document.getElementById('img1').style.width = '35%';
                }
            } else {
                document.getElementById('divimg2').classList.add('w-0');
                document.getElementById('divimg1').classList.remove('w-1/2');
                document.getElementById('divimg2').classList.remove('w-1/2');
                document.getElementById('img1').style.width = '35%';
            }
        };
        init();
    }, []);

    return (
        <div id="logo-details" className="">
            <div
                style={{ height: '300px' }}
                className="w-full border-2 my-3 rounded flex justify-center flex-col"
            >
                <div className="flex flex-wrap -mx-2 justify-center">
                    <div
                        id="divimg1"
                        className="w-1/2 px-2 flex justify-center"
                    >
                        <img
                            id="img1"
                            src={filePath}
                            alt={filePath}
                            style={{
                                margin: '10px',
                                width: '70%',
                                maxHeight:'250px'
                            }}
                        />
                    </div>
                    <div
                        id="divimg2"
                        className="w-1/2 px-2 flex justify-center "
                    >
                        <img
                            id="img2"
                            src={filePath1}
                            style={{
                                margin: '10px',
                                width: '70%',
                                maxHeight:'250px'

                            }}
                        />
                    </div>
                </div>
                <FileUploader
                    id="FileUpload"
                    selectButtonText="Drag your logo here or browse to upload"
                    labelText=""
                    accept="image/*"
                    uploadMode="useForm"
                    multiple={true}
                    onValueChanged={onFileUploaded}
                    style={{
                        height: '300px',
                        maxHeight: '300px',
                        width: '90%',
                        index: 1,
                        position: 'absolute',
                    }}
                ></FileUploader>
            </div>

            <div className="flex flex-wrap -mx-2">
                <div className="w-1/2 px-2">
                    <div className="w-full border p-2">
                        <label className="w-full">Primary color</label>
                        <ColorBox defaultValue="#fdc109">
                            <Validator>
                                <RequiredRule message="Primary color is required" />
                            </Validator>
                        </ColorBox>
                    </div>
                </div>
                <div className="w-1/2 px-2">
                    <div className="w-full border p-2">
                        <label className="w-full">Secondary color</label>
                        <ColorBox defaultValue="#00bcd5">
                            <Validator>
                                <RequiredRule message="Secondary color is required" />
                            </Validator>
                        </ColorBox>
                    </div>
                </div>
            </div>
            <div className="bg-white px-2 py-4 border-t border-gray-200 sm:px-4 flex flex-col items-end">
                <div className="text-right">
                    <Button
                        className="w-50 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                        onClick={save}
                        text="Submit For Review"
                    />
                    <Button
                        className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-gray-dark shadow-sm hover:bg-sc-gray-light focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                        type="normal"
                        text="Cancel"
                        onClick={() => setEditMode(false)}
                    />
                </div>
            </div>
        </div>
    );
};
