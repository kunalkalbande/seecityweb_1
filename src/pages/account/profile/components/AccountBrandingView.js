import React, { useState, useEffect } from 'react';
import { FileUploader, ColorBox, Validator } from 'devextreme-react';
import { RequiredRule } from 'devextreme-react/data-grid';
export const AccountBrandingView = (setupDetail) => {
    const [logo, setlogo] = useState([]);
    const [filePath, setPath] = useState(
        'http://www.myiconfinder.com/uploads/iconsets/256-256-37736f35fd7d98b10b35287679b223b8.png'
    );
    const [filePath1, setPath1] = useState('');

    useEffect(() => {
        const init = () => {
            console.log(setupDetail);
            if (setupDetail.setupDetail != null) {
                if (
                    setupDetail.setupDetail.file != null &&
                    setupDetail.setupDetail.file.length > 0
                ) {
                    setPath('/' + setupDetail.setupDetail.file[0].path);
                    if (setupDetail.setupDetail.file.length > 1) {
                        //document.getElementById("img1").style.marginLeft="-120px";
                        document
                            .getElementById('divimg2')
                            .classList.remove('w-0');
                        document
                            .getElementById('divimg1')
                            .classList.add('w-1/2');
                        document
                            .getElementById('divimg2')
                            .classList.add('w-1/2');
                        setPath1('/' + setupDetail.setupDetail.file[1].path);
                    } else {
                        document.getElementById('divimg2').classList.add('w-0');
                        document
                            .getElementById('divimg1')
                            .classList.remove('w-1/2');
                        document
                            .getElementById('divimg2')
                            .classList.remove('w-1/2');
                        document.getElementById('img1').style.width = '40%';
                    }
                } else {
                    document.getElementById('divimg2').classList.add('w-0');
                    document
                        .getElementById('divimg1')
                        .classList.remove('w-1/2');
                    document
                        .getElementById('divimg2')
                        .classList.remove('w-1/2');
                    document.getElementById('img1').style.width = '40%';
                }
            } else {
                document.getElementById('divimg2').classList.add('w-0');
                document.getElementById('divimg1').classList.remove('w-1/2');
                document.getElementById('divimg2').classList.remove('w-1/2');
                document.getElementById('img1').style.width = '40%';
            }
        };
        init();
    }, []);

    return (
        <div id="logo-details" className="">
            {/* <div className="flex flex-wrap">
				<div className="w-full border p-2 text-gray-700 font-bold">
					OKCZoo.OKC.see.city
				</div>
			</div> */}
            <div className="w-full  border-2 my-3 rounded flex justify-center flex-col">
                <div className="flex flex-wrap -mx-2 justify-center">
                    <div
                        id="divimg1"
                        className="w-1/2 px-2 flex justify-center"
                    >
                        <img
                            id="img1"
                            src={filePath}
                            alt={filePath}
                            style={{
                                margin: '10px',
                                width: '96%',
                            }}
                        />
                    </div>
                    <div
                        id="divimg2"
                        className="w-1/2 px-2 flex justify-center "
                    >
                        <img
                            id="img2"
                            src={filePath1}
                            style={{
                                margin: '10px',
                                width: '96%',
                            }}
                        />
                    </div>
                </div>
            </div>

            <div className="flex flex-wrap -mx-2">
                <div className="w-1/2 px-2">
                    <div className="w-full border p-2">
                        <label className="w-full">Primary color</label>
                        <ColorBox
                            defaultValue="#fdc109"
                            disabled={true}
                            style={{ opacity: 1 }}
                        >
                            <Validator>
                                <RequiredRule message="Primary color is required" />
                            </Validator>
                        </ColorBox>
                    </div>
                </div>
                <div className="w-1/2 px-2">
                    <div className="w-full border p-2">
                        <label className="w-full">Secondary color</label>
                        <ColorBox
                            defaultValue="#00bcd5"
                            disabled={true}
                            style={{ opacity: 1 }}
                        >
                            <Validator>
                                <RequiredRule message="Secondary color is required" />
                            </Validator>
                        </ColorBox>
                    </div>
                </div>
            </div>
        </div>
    );
};
