import React, { useState, useEffect } from 'react';
import { Loading } from '../../../components/common';
import { AccountDetailsView } from './components/AccountDetailsView';
import { EditAccountDetails } from './components/EditAccountDetails';
import { AccountBrandingView } from './components/AccountBrandingView';
import { EditAccountBranding } from './components/EditAccountBranding';
import { AccountBankDetailsView } from './components/AccountBankDetailView';
import { EditBankAccountDetails } from './components/EditBankDetailsView';
import { accountSetup } from '../../../api/graphql/accountSetup';
import { useDispatch, useSelector } from 'react-redux';
import { lookup } from '../../../api/graphql/lookup';
const ProfilePage = () => {
    const [isLoading, setIsLoading] = useState(false);
    const currentUser = useSelector((state) => state.user);
    const [setupDetail, setDetail] = useState('');
    const [tags, setTags] = useState('');
    const [accountTags, setAccountTags] = useState('');
    const [isEditMode, setIsEditMode] = useState(false);
    const [isImageEditMode, setIsImageEditMode] = useState(false);
    const [isBankingEditMode, setIsBankingEditMode] = useState(false);
    const [states, setStates] = useState('');
    const editModeChange = (editMode) => {
        console.log('edit mode');
        setIsEditMode(editMode);
    };
    const imageEditModeChange = (editMode) => {
        console.log('edit mode');
        setIsImageEditMode(editMode);
    };
    useEffect(() => {
        const fetchData = async () => {
            var accountId = currentUser.bag.accountId;
            setIsLoading(true);
            const res = await fetchApi(accountId);
            const data = await lookup.getLookupByType('Business Tags');
            setTags(data);
            const data1 = await lookup.getLookupByType('Account Tags');
            setAccountTags(data1);
            const data2 = await lookup.getLookupByType('States');
            setStates(data2);
            console.log(res);
            setDetail(res.accountById);
            setIsLoading(false);
        };
        fetchData();
    }, []);
    const fetchApi = (accountId) => {
        return new Promise((resolve) => {
            setTimeout(async () => {
                var res = await accountSetup.getAccountSetupDetails(accountId);
                console.log(res);
                resolve(res);
            }, 500);
        });
    };
    return (
        <div className="mx-2 flex flex-col overflow-y-hidden">
            {!isLoading ? (
                <form>
                    <div className="w-full sm:pr-1">
                        {!isImageEditMode && !isBankingEditMode ? (
                            <div className="my-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white  flex w-full px-4 py-5 border-b border-gray-200  flex-row item-right">
                                    <h3 className="text-lg leading-6 font-medium  text-sc-blue-500">
                                        Account Details
                                    </h3>

                                    <div className="text-right ml-auto">
                                        {!isEditMode ? (
                                            <a
                                                href="#"
                                                style={{
                                                    alignContent: 'right',
                                                }}
                                                onClick={() =>
                                                    setIsEditMode(true)
                                                }
                                            >
                                                Edit
                                            </a>
                                        ) : (
                                            <div />
                                        )}
                                    </div>
                                </div>
                                <div className="my-3 mx-3 text-sm leading-5 ">
                                    {!isEditMode ? (
                                        <AccountDetailsView
                                            setupDetail={setupDetail}
                                            tagsList={tags}
                                            accountTagsList={accountTags}
                                            states={states}
                                        />
                                    ) : (
                                        <EditAccountDetails
                                            setupDetail={setupDetail}
                                            tagsList={tags}
                                            accountTagsList={accountTags}
                                            statesList={states}
                                            onEdit={(value) =>
                                                editModeChange(value)
                                            }
                                        />
                                    )}

                                    {/* [Form goes here] */}
                                </div>
                            </div>
                        ) : (
                            <div />
                        )}
                        {!isEditMode && !isBankingEditMode ? (
                            <div className="my-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white  flex w-full px-4 py-5 border-b border-gray-200  flex-row item-right">
                                    <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                        Account Branding
                                    </h3>
                                    <div className="text-right ml-auto">
                                        {!isImageEditMode ? (
                                            <a
                                                href="#"
                                                style={{
                                                    alignContent: 'right',
                                                }}
                                                onClick={() =>
                                                    setIsImageEditMode(true)
                                                }
                                            >
                                                Edit
                                            </a>
                                        ) : (
                                            <div />
                                        )}
                                    </div>
                                </div>
                                <div className="my-3 mx-3  text-sm leading-5">
                                    {/* [Form goes here] */}
                                    {!isImageEditMode ? (
                                        <AccountBrandingView
                                            setupDetail={setupDetail}
                                        />
                                    ) : (
                                        <EditAccountBranding
                                            setupDetail={setupDetail}
                                            onEdit={(value) =>
                                                imageEditModeChange(value)
                                            }
                                        />
                                    )}
                                </div>
                            </div>
                        ) : (
                            <div />
                        )}
                        {!isImageEditMode && !isEditMode ? (
                            <div className="my-2 flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white  flex w-full px-4 py-5 border-b border-gray-200  flex-row item-right">
                                    <h3 className="text-lg leading-6 font-medium  text-sc-blue-500">
                                        Banking Details
                                    </h3>

                                    <div className="text-right ml-auto">
                                        {!isBankingEditMode ? (
                                            <a
                                                href="#"
                                                style={{
                                                    alignContent: 'right',
                                                }}
                                                onClick={() =>
                                                    setIsBankingEditMode(true)
                                                }
                                            >
                                                Edit
                                            </a>
                                        ) : (
                                            <div />
                                        )}
                                    </div>
                                </div>
                                <div className="my-3 mx-3 text-sm leading-5 ">
                                    {!isBankingEditMode ? (
                                        <AccountBankDetailsView
                                            setupDetail={setupDetail}
                                            tagsList={tags}
                                            accountTagsList={accountTags}
                                            states={states}
                                        />
                                    ) : (
                                        <EditBankAccountDetails
                                            setupDetail={setupDetail}
                                            onEdit={(value) =>
                                                setIsBankingEditMode(value)
                                            }
                                        />
                                    )}

                                    {/* [Form goes here] */}
                                </div>
                            </div>
                        ) : (
                            <div />
                        )}
                    </div>
                </form>
            ) : (
                <Loading />
            )}
        </div>
    );
};

export default ProfilePage;
