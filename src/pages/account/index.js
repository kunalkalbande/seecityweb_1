import React, { useContext, useState } from 'react';
import { Route, Switch, NavLink } from 'react-router-dom';
import { ReactReduxContext, useDispatch, useSelector } from 'react-redux';
import { DateBox, TextBox } from 'devextreme-react';
import TagBox from 'devextreme-react/tag-box';
import Validator from 'devextreme-react/validator';

const TaxesPage = () => {
    return <div> Taxes</div>;
};

const BasePage = ({ title }) => {
    const [now, setNow] = useState(new Date());

    const [tags, setTags] = useState([
        'Up Early',
        'Morning',
        'Afternoon',
        'Evening',
        'Up Late',
    ]);

    return (
        <div className="px-2 sm:px-3 h-full">
            <div className="min-h-full grid grid-cols-1 sm:grid-cols-1 col-gap-4">
                <div className="flex flex-col bg-white shadow overflow-hidden sm:rounded-b-md">
                    {title}
                </div>
                <br />

                <br />

                <TextBox className="text-blue-700" placeholder="Subject">
                    <Validator validationRules={[{ type: 'required' }]} />
                </TextBox>

                <br />
                <TagBox items={tags} />

                <br />

                <div className="mt-10 sm:mt-0">
                    <div className="md:grid md:grid-cols-3 md:gap-6">
                        <div className="md:col-span-1">
                            <div className="px-4 sm:px-0">
                                <h3 className="text-lg font-medium leading-6 text-gray-900">
                                    Personal Information
                                </h3>
                                <p className="mt-1 text-sm leading-5 text-gray-600">
                                    Use a permanent address where you can
                                    receive mail.
                                </p>
                            </div>
                        </div>
                        <div className="mt-5 md:mt-0 md:col-span-2">
                            <form action="#" method="POST">
                                <div className="shadow overflow-hidden sm:rounded-md">
                                    <div className="px-4 py-5 bg-white sm:p-6">
                                        <div className="grid grid-cols-6 gap-6">
                                            <div className="col-span-6 sm:col-span-3">
                                                <label
                                                    htmlFor="first_name"
                                                    className="block text-sm font-medium leading-5 text-gray-700"
                                                >
                                                    First name
                                                </label>
                                                <DateBox
                                                    className="mt-1"
                                                    defaultValue={now}
                                                    type="date"
                                                    onValueChanged={() =>
                                                        console.log('biff')
                                                    }
                                                />
                                                {/* <input
                                                    id="first_name"
                                                    className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                                /> */}
                                            </div>

                                            <div className="col-span-6 sm:col-span-3">
                                                <label
                                                    htmlFor="last_name"
                                                    className="block text-sm font-medium leading-5 text-gray-700"
                                                >
                                                    Last name
                                                </label>
                                                <TextBox
                                                    id="last_name"
                                                    className="mt-1"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-4">
                                                <label
                                                    htmlFor="email_address"
                                                    className="block text-sm font-medium leading-5 text-gray-700"
                                                >
                                                    Email address
                                                </label>
                                                <input
                                                    id="email_address"
                                                    className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-3">
                                                <label
                                                    htmlFor="country"
                                                    className="block text-sm font-medium leading-5 text-gray-700"
                                                >
                                                    Country / Region
                                                </label>
                                                <select
                                                    id="country"
                                                    className="mt-1 block form-select w-full py-2 px-3 py-0 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                                >
                                                    <option>
                                                        United States
                                                    </option>
                                                    <option>Canada</option>
                                                    <option>Mexico</option>
                                                </select>
                                            </div>

                                            <div className="col-span-6">
                                                <label
                                                    htmlFor="street_address"
                                                    className="block text-sm font-medium leading-5 text-gray-700"
                                                >
                                                    Street address
                                                </label>
                                                <input
                                                    id="street_address"
                                                    className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-6 lg:col-span-2">
                                                <label
                                                    htmlFor="city"
                                                    className="block text-sm font-medium leading-5 text-gray-700"
                                                >
                                                    City
                                                </label>
                                                <input
                                                    id="city"
                                                    className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-3 lg:col-span-2">
                                                <label
                                                    htmlFor="state"
                                                    className="block text-sm font-medium leading-5 text-gray-700"
                                                >
                                                    State / Province
                                                </label>
                                                <input
                                                    id="state"
                                                    className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-3 lg:col-span-2">
                                                <label
                                                    htmlFor="postal_code"
                                                    className="block text-sm font-medium leading-5 text-gray-700"
                                                >
                                                    ZIP / Postal
                                                </label>
                                                <input
                                                    id="postal_code"
                                                    className="mt-1 form-input block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                        <button className="py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 shadow-sm hover:bg-indigo-500 focus:outline-none focus:shadow-outline-blue active:bg-indigo-600 transition duration-150 ease-in-out">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div className="hidden sm:block">
                    <div className="py-5">
                        <div className="border-t border-gray-200"></div>
                    </div>
                </div>
            </div>

            <NavLink exact={true} to="/accounts/1/taxes">
                Taxes
            </NavLink>
        </div>
    );
};

export const AccountPage = () => {
    const { store } = useContext(ReactReduxContext);
    const { getState } = store;
    const account = useSelector((state) => state.selectedAccount);
    console.log(getState());

    return (
        <div>
            <Switch>
                <Route exact path="/accounts/:id">
                    <BasePage title={`${account.name} General Information`} />
                </Route>
                <Route path="/accounts/:id/taxes">
                    <TaxesPage />
                </Route>
                <Route path="/accounts/:id/products">
                    <BasePage title="Products" />
                </Route>
            </Switch>
        </div>
    );
};
