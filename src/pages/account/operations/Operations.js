/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { Button } from 'devextreme-react';
import Loading from '../../../components/common/Loading';
import { OperatingPeriodList} from './components';
import operatingPeriods from '../../../data/operating-periods';
import {operatingPeriodapi} from '../../../api/graphql/operatingPeriod';
import { useSelector } from 'react-redux';

const defaultPeriod = operatingPeriods.complex.filter(
    (el) => el.year === '2020' && el.name === 'Calendar Year' 
);
// const myPeriods = operatingPeriods.complex.filter((el) => el.year === '2020');

const toOperatingPeriods = (array) => {
    return array.map((m) => {
        // Note: both startTime & endTime use startDate
        let startTime = moment(m.start_date);
        startTime.hours(9);
        startTime.minutes(0);

        let endTime = moment(m.start_date);
        endTime.hours(17);
        endTime.minutes(0);

        return {
            name: m.name,
            startDate: m.start_date,
            startTime: startTime,
            endDate: m.end_date,
            endTime: endTime,
            dailySchedule: [],
            dayParts: [],
        };
    });
};

const fetchApi = (accountId) => {
    return new Promise((resolve) => {
        setTimeout(async() => {
            var data=await operatingPeriodapi.getAccountOperatingPeriodsQuery(accountId);
            console.log(data);
            if(data.accountById && data.accountById.operatingPeriods && data.accountById.operatingPeriods.length>0)
            {
                resolve(data.accountById.operatingPeriods)
            }
            else
            {
                resolve(toOperatingPeriods(defaultPeriod));
            }
        }, 500);
    });
};

const saveApi = (accountId,operatingPeriods) => {
    return new Promise((resolve) => {
        setTimeout(async() => {
            console.log({"_Id ":accountId,"operatingPeriods": JSON.stringify(operatingPeriods)});
            var res = await operatingPeriodapi.updateOperatingPeriods(accountId, operatingPeriods);
         
             resolve(operatingPeriods);
            
            //resolve(toOperatingPeriods(defaultPeriod));
        }, 800);
    });
};

const OperationsPage = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [showControls, setShowControls] = useState(true);
    const [operatingPeriods, setOperatingPeriods] = useState([]);
    const currentUser = useSelector((state) => state.user);
console.log(useSelector((state) => state));

       
    console.log(currentUser.bag.accountId);
    var accId=currentUser.bag.accountId;
   
    useEffect(() => {
        const init = async () => {
            await fetch();
        };
        init();
    }, []);

    const fetch = async () => {
        setIsLoading(true);
        const results = await fetchApi(accId);
        setOperatingPeriods(results);
        setIsLoading(false);
    };

    const save = async () => {
        console.log(JSON.stringify(operatingPeriods));
        setIsLoading(true);

        await saveApi(accId,operatingPeriods);
        // setOperatingPeriods(results);
        setIsLoading(false);
    };

    const cancel = async () => {
          await fetch();
    };

    const getOperatingPeriodList=(value)=>{
        setOperatingPeriods(value);
    }
    return (
        <div className="px-2 sm:px-3 h-full">
            <div className="min-h-full grid grid-cols-1 sm:grid-cols-1 col-gap-4">
                <div className="mb-2 flex flex-col overflow-y-hidden">
                    <div className="flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                        <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6 flex items-center justify-between flex-wrap sm:flex-no-wrap">
                            <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                Operating Periods
                            </h3>
                        </div>
                        <div className="h-full">
                            {!isLoading ? (
                                <OperatingPeriodList
                                onChange={(value) =>
                                    getOperatingPeriodList(value)
                                }
                                    canSave={setShowControls}
                                    periods={operatingPeriods}
                                />
                            ) : (
                                <Loading />
                            )}
                        </div>
                        {showControls && (
                            <div className="bg-white px-2 py-4 border-t border-gray-200 sm:px-4 flex flex-col items-end">
                                <div className="text-right">
                                    <Button
                                        className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                                        onClick={save}
                                        text="Save"
                                    />
                                    <Button
                                        className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-gray-dark shadow-sm hover:bg-sc-gray-light focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                                        type="normal"
                                        text="Cancel"
                                        onClick={cancel}
                                    />
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default OperationsPage;
