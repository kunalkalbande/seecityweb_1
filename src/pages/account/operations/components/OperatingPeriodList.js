/* eslint-disable react/prop-types */
import { useDispatch } from 'react-redux';
import React, { useEffect, useRef, useState } from 'react';
import moment from 'moment';
import { TagBox } from 'devextreme-react';
import { OperatingPeriodListItem, OperatingPeriodForm } from './';
import { arrayUtils } from '../../../../utils';
import operatingPeriods from '../../../../data/operating-periods';

const tagTemplate = (tag) => {
    const showRemoveButton = tag !== 'Calendar Year';
    return (
        <React.Fragment>
            <div className="dx-tag-content">
                {tag}
                {showRemoveButton && (
                    <div className="dx-tag-remove-button"></div>
                )}
            </div>
        </React.Fragment>
    );
};

const OperationsList = ({ onChange, periods, canSave }) => {
    const [items, setItems] = useState([]);
    const [selectedPeriods, setSelectedPeriods] = useState([]);
    const [selectedPeriod, setSelectedPeriod] = useState(null);
    const tagRef = useRef(null);
    const dispatch = useDispatch();

    const loadTags = (array) => {
        let itemsArray = [
            ...array
                .filter((f) => f.name !== 'Calendar Year')
                .map((m) => m.name),
            ...operatingPeriods.tags,
        ];
        itemsArray = itemsArray.filter(arrayUtils.distinct);
        setItems(itemsArray);
    };

    useEffect(() => {
        loadTags(periods);
        setSelectedPeriods(periods);
    }, []);

    useEffect(() => {
        let customTag = selectedPeriods.filter((f) => f.name === 'Custom');
        if (customTag.length > 0) setSelectedPeriod(customTag[0]);
    }, [selectedPeriods]);

    useEffect(() => {
        canSave(selectedPeriod === null);
    }, [selectedPeriod]);
    useEffect(() => {
        onChange(selectedPeriods);
    }, [selectedPeriods]);
    // Handle changes to tag box
    const onValueChanged = (args) => {
        let array = [...selectedPeriods];
        if (args.previousValue.length < args.value.length) {
            let tags = arrayUtils.except(args.value, args.previousValue);
            tags.forEach((tag) => {
                //TODO: make smarter based on year
                let period = operatingPeriods.complex.find(
                    (x) => x.name === tag && x.year === '2020'
                );
                if (tag === 'Custom') {
                    let now = moment();
                    now.minutes(0);
                    array.push({
                        name: 'Custom',
                        startDate: now,
                        endDate: now.clone().add(1, 'day'),
                        startTime: now.clone().hours(8),
                        endTime: now.clone().hours(17),
                        dailySchedule: [],
                        dayParts: [],
                    });
                } else {
                    // Set default start/end time
                    let startTime = moment(period.startTime);
                    startTime.hours(9);
                    startTime.minutes(0);
                    period.startTime = startTime;

                    let endTime = moment(period.startTime);
                    endTime.hours(17);
                    endTime.minutes(0);
                    period.endTime = endTime;
                    array.push({
                        name: period.name,
                        startDate: period.start_date,
                        startTime:period.startTime.format(),
                        endDate: period.end_date,
                        endTime:period.endTime.format(),
                        dailySchedule: [],
                        dayParts: [],
                    });
                }
            });
            setSelectedPeriods(array);
        } else {
            let tag = arrayUtils.except(args.previousValue, args.value)[0];
            let idx = [...selectedPeriods].map((e) => e.name).indexOf(tag);
            array.splice(idx, 1);
            setSelectedPeriods(array);
        }
        localStorage.setItem('selectedPeriods', selectedPeriods);
    };

    return (
        <React.Fragment>
            {!selectedPeriod ? (
                <div>
                    <div className="border-b border-gray-200">
                        <div className="my-3 mx-2 sm:mx-5 text-sm leading-5 text-gray-500 ">
                            <div className="mb-1 text-sm text-gray-400">
                                Select your operating periods
                            </div>
                            <TagBox
                                deferRendering={true}
                                ref={tagRef}
                                items={items}
                                value={selectedPeriods.map((m) => m.name)}
                                onValueChanged={onValueChanged}
                                applyValueMode="useButtons"
                                tagRender={tagTemplate}
                                showSelectionControls={true}
                                hideSelectedItems={true}
                                showDropDownButton={true}
                            ></TagBox>
                        </div>
                    </div>

                    <ul className="pt-2 pb-2 overflow-auto">
                        {selectedPeriods.map((item, index) => (
                            <li key={index}>
                                <OperatingPeriodListItem
                                    item={item}
                                    onSelect={setSelectedPeriod}
                                />
                            </li>
                        ))}
                    </ul>
                </div>
            ) : (
                <OperatingPeriodForm
                    item={selectedPeriod}
                    onCancel={() => setSelectedPeriod(null)}
                    onOk={() => {
                        loadTags(selectedPeriods);
                        setSelectedPeriod(null);
                    }}
                />
            )}
        </React.Fragment>
    );
};
export default OperationsList;
