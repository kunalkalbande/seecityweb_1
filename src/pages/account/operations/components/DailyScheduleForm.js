/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { DateBox, CheckBox } from 'devextreme-react';
import {
    RequiredRule,
    Validator,
    ValidationRule,
} from 'devextreme-react/validator';
import moment from 'moment';

const WeekDaySchedule = ({ item }) => {
    const [isClosed, setIsClosed] = useState(item.isClosed);
    const [startTime, setStartTime] = useState(moment(item.startTime));
    const [endTime, setEndTime] = useState(moment(item.endTime));

    const isTimeValid = () => {
        return moment(startTime).isBefore(moment(endTime));
    };

    useEffect(() => {
        item.startTime = startTime;
        item.endTime = endTime;
        item.isClosed = isClosed;
    }, [isClosed, startTime, endTime]);

    return (
        <div className="mt-2 mb-2 sm:mb-1 grid grid-cols-2 sm:grid-cols-4 row-gap-2 col-gap-4">
            <div className="col-span-1">
                <label
                    htmlFor="name"
                    className="sm:invisible sm:h-0 block text-sm font-medium leading-5 text-gray-400"
                >
                    Day
                </label>
                <div className="mt-1 text-gray-500 text-sm">{item.name}</div>
            </div>
            <div className="flex flex-0 items-center sm:items-start">
                <div className="mt-1 sm:pl-2">
                    <CheckBox
                        id="is_closed"
                        value={isClosed}
                        onValueChanged={(e) => setIsClosed(e.value)}
                    />
                </div>
                <label
                    htmlFor="is_closed"
                    className="ml-2 block text-sm font-medium leading-5 text-gray-400 sm:invisible"
                >
                    Closed
                </label>
            </div>
            <div>
                <label
                    htmlFor="start_time"
                    className="sm:invisible sm:h-0 block text-sm font-medium leading-5 text-gray-400"
                >
                    Start Time
                </label>
                <div className="mt-1">
                    <DateBox
                        id="start_time"
                        type="time"
                        disabled={isClosed}
                        useMaskBehavior={true}
                        defaultValue={startTime}
                        onValueChanged={(e) => setStartTime(e.value)}
                    >
                        <Validator>
                            <RequiredRule
                                message={`Open time for "${item.name}" required`}
                            />
                            <ValidationRule
                                type="custom"
                                message={`"${item.name}" start time must be < end time`}
                                validationCallback={isTimeValid}
                            />
                        </Validator>
                    </DateBox>
                </div>
            </div>
            <div>
                <label
                    htmlFor="end_time"
                    className="sm:invisible sm:h-0 block text-sm font-medium leading-5 text-gray-400"
                >
                    Close Time
                </label>
                <div className="mt-1">
                    <DateBox
                        id="end_time"
                        type="time"
                        disabled={isClosed}
                        useMaskBehavior={true}
                        defaultValue={endTime}
                        onValueChanged={(e) => setEndTime(e.value)}
                    >
                        <Validator>
                            <RequiredRule
                                message={`Close time for "${item.name}" required`}
                            />
                            <ValidationRule
                                type="custom"
                                message={`"${item.name}" end time must be > start time`}
                                validationCallback={isTimeValid}
                            />
                        </Validator>
                    </DateBox>
                </div>
            </div>
        </div>
    );
};

export default WeekDaySchedule;
