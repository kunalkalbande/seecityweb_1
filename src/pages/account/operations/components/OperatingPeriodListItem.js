/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import moment from 'moment';
import { dateUtils } from '../../../../utils';
import { daysOfTheWeek } from '../../../../data/days';

const OperatingPeriodListItem = ({ item, onSelect }) => {
    const [range] = useState(() => {
        let start = moment(item.startDate);
        let end = moment(item.endDate);
        let diff = end.diff(start, 'days');
        let format = diff > 7 ? 'MMM D yyyy' : 'dddd, MMM D yyyy';
        let range = `${start.format(format)} - ${end.format(format)}`;
        return range;
    });

    const [days] = useState(() => {
        let days = [];
        if (item.dailySchedule && item.dailySchedule.length > 0) {
            days = item.dailySchedule.filter((f) => !f.isClosed);
            days = days.map((m) => m.name);
        } else {
            let start = moment(item.startDate);
            let end = moment(item.endDate);
            let diff = end.diff(start, 'days');
            if (diff) {
                if (diff > 7) {
                    days = daysOfTheWeek;
                } else {
                    days = dateUtils.between(start, end).map((d) => {
                        return moment(d).format('ddd');
                    });
                }
            }
        }
        days = days
            .map((m) => m.substring(0, 3))
            .toString()
            .replace(/,/g, ', ');
        return days;
    });

    return (
        <a
            href="#"
            className="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"
        >
            <div className="px-2 py-1 sm:px-6">
                <div className="flex items-center justify-between">
                    <div className="text-sm leading-5 font-medium text-sc-blue-400 truncate">
                        {item.name}
                    </div>

                    <div className="ml-2 flex-shrink-0 flex">
                        <button
                            className="text-xs text-sc-blue-200 underline"
                            onClick={() => onSelect(item)}
                        >
                            Edit
                        </button>
                    </div>
                </div>
                <div className="mt-1 sm:flex sm:justify-between">
                    <div className="sm:flex">
                        <div className="mr-6 flex items-center text-xs leading-5 text-gray-400">
                            {range}
                        </div>
                    </div>
                    <div className="sm:flex">
                        <div className="flex items-center text-xs leading-5 text-gray-400">
                            {days}
                        </div>
                    </div>
                </div>
            </div>
        </a>
    );
};

export default OperatingPeriodListItem;
