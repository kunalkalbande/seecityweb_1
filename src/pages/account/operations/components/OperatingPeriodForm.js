/* eslint-disable react/prop-types */
import React, { Fragment, useEffect, useRef, useState } from 'react';
import {
    Button,
    CheckBox,
    DateBox,
    TagBox,
    TextBox,
    ValidationSummary,
} from 'devextreme-react';
import {
    RequiredRule,
    ValidationRule,
    Validator,
} from 'devextreme-react/validator';

import moment from 'moment';
import { DailyScheduleForm, DayPartForm } from './';
import { arrayUtils, dateUtils } from '../../../../utils';
import { daysOfTheWeek, dayParts } from '../../../../data/days';

import { lookup } from '../../../../api/graphql/lookup';

const OperatingPeriodForm = ({ item, onOk, onCancel }) => {
    const [dayParts, setDayParts] = useState([]);
    const [name, setName] = useState(item.name);
    const [startDate, setStartDate] = useState(moment(item.startDate));
    const [endDate, setEndDate] = useState(moment(item.endDate));
    const [startTime, setStartTime] = useState(item.startTime);
    const [endTime, setEndTime] = useState(item.endTime);
    const [hasDailySchedule, setHasDailySchedule] = useState(
        item.dailySchedule && item.dailySchedule.length > 0
    );
    const [dailySchedule, setDailySchedule] = useState(
        item.dailySchedule || []
    );
    const [hasDayParts, setHasDayParts] = useState(
        item.dayParts && item.dayParts.length > 0
    );
    const [selectedDayParts, setSelectedDayParts] = useState(
        item.dayParts || []
    );
    const nameTextBox = useRef(null);

    useEffect(() => {
        const init = async () => {
            const data = await lookup.getLookupByType("Day Parts");
            setDayParts(data);
        }
        init();
    }, [])

    useEffect(() => {
        if (name !== 'Calendar Year') nameTextBox.current.instance.focus();
    }, []);

    useEffect(() => {
        const diff = moment(endDate).diff(moment(startDate), 'days');
        if (hasDailySchedule) {
            if (diff < 7) {
                let schedule = dateUtils
                    .between(
                        moment(startDate).format(),
                        moment(endDate).format()
                    )
                    .map((d) => {
                        return {
                            name: moment(d).format('dddd'),
                            isClosed: false,
                            startTime: startTime,
                            endTime: endTime,
                        };
                    });
                setDailySchedule(schedule);
            } else {
                // if we already have a schedule no reason to re-create
                if (
                    dailySchedule.length > 0 &&
                    dailySchedule[0].name === 'Monday'
                )
                    return;

                let schedule = daysOfTheWeek.map((day) => {
                    return {
                        name: day,
                        isClosed: false,
                        startTime: startTime,
                        endTime: endTime,
                    };
                });
                setDailySchedule(schedule);
            }
        }
    }, [hasDailySchedule, startDate, endDate]);

    useEffect(() => {
        if (!hasDailySchedule) setDailySchedule([]);
    }, [hasDailySchedule]);

    useEffect(() => {
        if (!hasDayParts) setSelectedDayParts([]);
    }, [hasDayParts]);

    const handleSubmit = (e) => {
        e.preventDefault();
        item.name = name;
        item.startDate = moment(startDate).format();
        item.endDate = moment(endDate).format();
        item.startTime = moment(startTime).format();
        item.endTime = moment(endTime).format();

        if (hasDailySchedule) {
            item.dailySchedule = dailySchedule.map((m) => {
                return {
                    name: m.name,
                    isClosed: m.isClosed,
                    startTime: moment(m.startTime).format(),
                    endTime: moment(m.endTime).format(),
                };
            });
        } else {
            item.dailySchedule = null;
        }

        if (hasDayParts) {
            item.dayParts = selectedDayParts.map((m) => {
                return {
                    name: m.name,
                    startTime: moment(m.startTime).format(),
                    endTime: moment(m.endTime).format(),
                };
            });
        } else {
            item.dayParts = null;
        }

        onOk(item);
    };

    const isDateRangeValid = () => {
        return moment(startDate).isBefore(moment(endDate));
    };

    const isTimeRangeValid = () => {
        return moment(startTime).isBefore(moment(endTime));
    };

    const onDatePartsChanged = (args) => {
        let array = [...selectedDayParts];
        if (args.previousValue.length < args.value.length) {
            let tags = arrayUtils.except(args.value, args.previousValue);
            tags.forEach((tag) => {
                if (tag !== '') {
                    const dp = dayParts.find((x) => x.name === tag);
                    const startTime = moment(startDate).hour(dp.start);
                    const endTime = moment(startDate).hour(dp.end);
                    array.push({ ...dp, startTime, endTime });
                }
            });
        } else {
            let tag = arrayUtils.except(args.previousValue, args.value)[0];
            let idx = [...selectedDayParts].map((e) => e.name).indexOf(tag);
            array.splice(idx, 1);
        }
        setSelectedDayParts(array);
    };

    const validateDayPartName = ({ name }) => {
        //TODO: look for duplicate
        return true;
    };

    const validateDayPartRange = (time) => {
        if (hasDailySchedule) return true;

        const dayPartTime = moment(time);
        const result = dayPartTime.isBetween(
            moment(startTime),
            moment(endTime)
        );
        return result;
    };

    const validateDayPartRanges = ({ name, startTime, endTime }) => {
        if (selectedDayParts.length === 1) return true;

        let params = [
            moment(startTime).format('HH:mm'),
            moment(endTime).format('HH:mm'),
        ];

        const copy = [...selectedDayParts]
            .filter((f) => f.name !== name)
            .map((dp) => [
                moment(dp.startTime).format('HH:mm'),
                moment(dp.endTime).format('HH:mm'),
            ]);

        copy.push(params);

        const sorted = copy.sort((start, end) => {
            if (start < end) return -1;
            if (start === end) return 0;
            return 1;
        });

        for (let i = 0; i < sorted.length - 1; i++) {
            const currentEndTime = sorted[i][1];
            const nextStartTime = sorted[i + 1][0];
            if (currentEndTime > nextStartTime) {
                return false;
            }
        }
        return true;
    };

    return (
        <form
            onSubmit={handleSubmit}
            onReset={() => onCancel()}
            className="w-full h-full"
        >
            <div className="h-full flex flex-1 flex-col">
                <div className="h-full">
                    <div className="mt-4 mx-2 sm:mx-5 grid grid-cols-1 row-gap-6 col-gap-4 sm:grid-cols-2">
                        <div className="sm:col-span-2">
                            <label
                                htmlFor="name"
                                className="block text-sm font-medium leading-5 text-gray-400"
                            >
                                Name
                            </label>
                            <div className="mt-1 sm:w-1/2">
                                <TextBox
                                    id="name"
                                    ref={nameTextBox}
                                    type="email"
                                    value={name}
                                    name="name"
                                    disabled={name === 'Calendar Year'}
                                    onValueChanged={(e) => setName(e.value)}
                                >
                                    <Validator>
                                        <RequiredRule message="Name is required" />
                                    </Validator>
                                </TextBox>
                            </div>
                        </div>
                        <div>
                            <label
                                htmlFor="start_date"
                                className="block text-sm font-medium leading-5 text-gray-400"
                            >
                                Start Date
                            </label>
                            <div className="mt-1">
                                <DateBox
                                    id="start_date"
                                    type="date"
                                    disabled={name === 'Calendar Year'}
                                    useMaskBehavior={true}
                                    defaultValue={startDate}
                                    onValueChanged={(e) =>
                                        setStartDate(e.value)
                                    }
                                >
                                    <Validator>
                                        <RequiredRule message="Start date required" />
                                        <ValidationRule
                                            type="custom"
                                            message="Start date must be < end date"
                                            validationCallback={
                                                isDateRangeValid
                                            }
                                        />
                                    </Validator>
                                </DateBox>
                            </div>
                        </div>
                        <div>
                            <label
                                htmlFor="end_date"
                                className="block text-sm font-medium leading-5 text-gray-400"
                            >
                                End Date
                            </label>
                            <div className="mt-1">
                                <DateBox
                                    id="end_date"
                                    type="date"
                                    disabled={name === 'Calendar Year'}
                                    useMaskBehavior={true}
                                    defaultValue={endDate}
                                    onValueChanged={(e) => setEndDate(e.value)}
                                >
                                    <Validator>
                                        <RequiredRule message="End date required" />
                                        <ValidationRule
                                            type="custom"
                                            message="End date must be > start date"
                                            validationCallback={
                                                isDateRangeValid
                                            }
                                        />
                                    </Validator>
                                </DateBox>
                            </div>
                        </div>

                        <div className="sm:col-span-2 sm:pt-1 sm:border-b sm:border-gray-200">
                            <h3 className="text-lg leading-6 font-medium text-sc-blue-400 pb-1">
                                Hours of Operation
                            </h3>
                        </div>

                        <div className="sm:col-span-2 flex flex-0 items-center sm:items-start">
                            <CheckBox
                                id="specify_days"
                                value={hasDailySchedule}
                                onValueChanged={(e) =>
                                    setHasDailySchedule(e.value)
                                }
                            />
                            <label
                                htmlFor="specify_days"
                                className="ml-2 block text-sm font-medium leading-5 text-gray-400"
                            >
                                Specify hours by day
                            </label>
                        </div>

                        {!hasDailySchedule ? (
                            <Fragment>
                                <div>
                                    <label
                                        htmlFor="start_time"
                                        className="block text-sm font-medium leading-5 text-gray-400"
                                    >
                                        Open Time
                                    </label>
                                    <div className="mt-1">
                                        <DateBox
                                            id="start_time"
                                            type="time"
                                            useMaskBehavior={true}
                                            defaultValue={startTime}
                                            onValueChanged={(e) =>
                                                setStartTime(e.value)
                                            }
                                        >
                                            <Validator>
                                                <RequiredRule message="Start time required" />
                                                <ValidationRule
                                                    type="custom"
                                                    message="Start time must be < end time"
                                                    validationCallback={
                                                        isTimeRangeValid
                                                    }
                                                />
                                            </Validator>
                                        </DateBox>
                                    </div>
                                </div>

                                <div>
                                    <label
                                        htmlFor="end_time"
                                        className="block text-sm font-medium leading-5 text-gray-400"
                                    >
                                        Close Time
                                    </label>
                                    <div className="mt-1">
                                        <DateBox
                                            id="end_time"
                                            type="time"
                                            useMaskBehavior={true}
                                            defaultValue={endTime}
                                            onValueChanged={(e) =>
                                                setEndTime(e.value)
                                            }
                                        >
                                            <Validator>
                                                <RequiredRule message="End time required" />
                                                <ValidationRule
                                                    type="custom"
                                                    message="End time must be > start time"
                                                    validationCallback={
                                                        isTimeRangeValid
                                                    }
                                                />
                                            </Validator>
                                        </DateBox>
                                    </div>
                                </div>
                            </Fragment>
                        ) : (
                                <div className="sm:col-span-2">
                                    <div className="invisible h-0 sm:h-2 sm:visible sm:mt-4 grid grid-cols-2 grid-cols-4 col-gap-4">
                                        <div className="text-gray-400 text-xs ">
                                            Day
                                    </div>
                                        <div className="text-gray-400 text-xs">
                                            Closed
                                    </div>
                                        <div className="text-gray-400 text-xs">
                                            Open
                                    </div>
                                        <div className="text-gray-400 text-xs">
                                            Close
                                    </div>
                                    </div>
                                    <div className="mt-2">
                                        {dailySchedule.map((item, index) => (
                                            <DailyScheduleForm
                                                key={index}
                                                item={item}
                                            />
                                        ))}
                                    </div>
                                </div>
                            )}

                        <div className="sm:col-span-2 sm:pt-1 sm:border-b sm:border-gray-200">
                            <h3 className="text-lg leading-6 font-medium text-sc-blue-400 pb-1">
                                Day Parts
                            </h3>
                        </div>

                        <div className="sm:col-span-2 flex flex-0 items-center sm:items-start">
                            <CheckBox
                                id="has_day_parts"
                                value={hasDayParts}
                                onValueChanged={(e) => setHasDayParts(e.value)}
                            />
                            <label
                                htmlFor="has_day_parts"
                                className="ml-2 block text-sm font-medium leading-5 text-gray-400"
                            >
                                Select and customize day parts
                            </label>
                        </div>

                        {hasDayParts === true && (
                            <div className="sm:col-span-2">
                                <div className="sm:col-span-2">
                                    <TagBox
                                        disabled={!hasDayParts}
                                        items={dayParts.map((dp) => dp.name)}
                                        value={selectedDayParts.map(
                                            (m) => m.name
                                        )}
                                        onValueChanged={onDatePartsChanged}
                                        applyValueMode="useButtons"
                                        showSelectionControls={true}
                                        hideSelectedItems={true}
                                        showDropDownButton={true}
                                        placeholder="Select day parts"
                                    ></TagBox>
                                </div>

                                <div className="sm:col-span-2">
                                    <div className="invisible h-0 sm:h-2 sm:visible sm:mt-4 grid grid-cols-2 sm:grid-cols-3 col-gap-4">
                                        <div className="text-gray-400 text-xs">
                                            Name
                                        </div>
                                        <div className="text-gray-400 text-xs">
                                            Start Time
                                        </div>
                                        <div className="text-gray-400 text-xs">
                                            End Time
                                        </div>
                                    </div>
                                    <div className="mt-1">
                                        {selectedDayParts.map((item, index) => (
                                            <DayPartForm
                                                key={index}
                                                item={item}
                                                onValidateName={
                                                    validateDayPartName
                                                }
                                                onValidateRange={
                                                    validateDayPartRange
                                                }
                                                onValidateRanges={
                                                    validateDayPartRanges
                                                }
                                            />
                                        ))}
                                    </div>
                                </div>
                            </div>
                        )}

                        <div className="sm:col-span-2">
                            <ValidationSummary id="summary"></ValidationSummary>
                        </div>
                    </div>
                </div>
                <div className="px-2 py-4 border-t border-gray-200 sm:px-4 flex flex-col items-end">
                    <div className="text-right">
                        <Button
                            className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                            useSubmitBehavior={true}
                            text="OK"
                        />
                        <Button
                            className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-gray-dark shadow-sm hover:bg-sc-gray-light focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                            type="normal"
                            text="Cancel"
                            onClick={onCancel}
                        />
                    </div>
                </div>
            </div>
        </form>
    );
};

export default OperatingPeriodForm;
