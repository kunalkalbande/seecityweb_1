/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { DateBox, TextBox } from 'devextreme-react';
import {
    RequiredRule,
    Validator,
    ValidationRule,
} from 'devextreme-react/validator';
import moment from 'moment';

const DayPartForm = ({
    item,
    onValidateName,
    onValidateRange,
    onValidateRanges,
}) => {
    const [name, setName] = useState(item.name);
    const [startTime, setStartTime] = useState(moment(item.startTime));
    const [endTime, setEndTime] = useState(moment(item.endTime));

    const isTimeValid = () => {
        return moment(startTime).isBefore(moment(endTime));
    };

    return (
        <div className="mt-2 mb-2 sm:mb-1 grid grid-cols-2 sm:grid-cols-3 row-gap-2 col-gap-4">
            <div className="col-span-2 sm:col-span-1">
                <label
                    htmlFor="name"
                    className="sm:invisible sm:h-0 block text-sm font-medium leading-5 text-gray-400"
                >
                    Name
                </label>
                <div className="mt-1">
                    <TextBox
                        id="name"
                        type="email"
                        value={name}
                        name="name"
                        onValueChanged={(e) => setName(e.value)}
                    >
                        <Validator>
                            <RequiredRule message="Date part name required" />
                        </Validator>
                    </TextBox>
                </div>
            </div>
            <div>
                <label
                    htmlFor="start_time"
                    className="sm:invisible sm:h-0 block text-sm font-medium leading-5 text-gray-400"
                >
                    Start Time
                </label>
                <div className="mt-1">
                    <DateBox
                        id="start_time"
                        type="time"
                        useMaskBehavior={true}
                        defaultValue={startTime}
                        onValueChanged={(e) => setStartTime(e.value)}
                    >
                        <Validator>
                            <RequiredRule
                                message={`Start time for "${name}" required`}
                            />
                            <ValidationRule
                                type="custom"
                                message={`"${name}" start time must be < end time`}
                                validationCallback={isTimeValid}
                            />
                            <ValidationRule
                                type="custom"
                                message={`"${name}" range overlaps with another day part`}
                                validationCallback={() => {
                                    return onValidateRanges({
                                        name,
                                        startTime,
                                        endTime,
                                    });
                                }}
                            />
                            <ValidationRule
                                type="custom"
                                message={`"${name}" start time cannot be < than open time`}
                                validationCallback={() => {
                                    return onValidateRange(endTime);
                                }}
                            />
                        </Validator>
                    </DateBox>
                </div>
            </div>
            <div>
                <label
                    htmlFor="end_time"
                    className="sm:invisible sm:h-0 block text-sm font-medium leading-5 text-gray-400"
                >
                    End Time
                </label>
                <div className="mt-1">
                    <DateBox
                        id="end_time"
                        type="time"
                        useMaskBehavior={true}
                        defaultValue={endTime}
                        onValueChanged={(e) => setEndTime(e.value)}
                    >
                        <Validator>
                            <RequiredRule
                                message={`End time for "${name}" required`}
                            />
                            <ValidationRule
                                type="custom"
                                message={`"${name}" end time must be > start time`}
                                validationCallback={isTimeValid}
                            />
                            <ValidationRule
                                type="custom"
                                message={`"${name}" range overlaps with another day part`}
                                validationCallback={() => {
                                    return onValidateRanges({
                                        name,
                                        startTime,
                                        endTime,
                                    });
                                }}
                            />
                            <ValidationRule
                                type="custom"
                                message={`"${name}" end time cannot be > than close time`}
                                validationCallback={() => {
                                    return onValidateRange(endTime);
                                }}
                            />
                        </Validator>
                    </DateBox>
                </div>
            </div>
        </div>
    );
};

export default DayPartForm;
