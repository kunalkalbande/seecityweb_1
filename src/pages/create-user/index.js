import React, { useState, useEffect, useRef } from 'react';
import {
    RequiredRule,
    FormItem,
    Paging,
    Popup,
    Position,
    Button,
    ValidationRule,
    AsyncRule,
} from 'devextreme-react/data-grid';
import { Validator, TagBox } from 'devextreme-react';
import roles from '../../data/roles';
import { authenticationService } from '../../services/authentication.service';
import { apolloClient } from '../../api/graphql/apolloClient';
import DataGrid, { Column, Editing } from 'devextreme-react/data-grid';
import { SpeedDialAction } from 'devextreme-react/speed-dial-action';
import { addUsers, updateUser, deleteUser } from '../../api/graphql/mutations';
import { useSelector } from 'react-redux';
import { usersApi } from '../../api/graphql/users';
import { Loading } from '../../components/common';

const tagTemplate = (tag) => {
    var showRemoveButton = true;
    if (tag.name === 'Admin' || tag.name === 'Member') {
        showRemoveButton = false;
    }
    return (
        <React.Fragment>
            <div className="dx-tag-content">
                {tag.name}
                {showRemoveButton && (
                    <div className="dx-tag-remove-button"></div>
                )}
            </div>
        </React.Fragment>
    );
};

const fetchApi = () => {
    return new Promise((resolve) => {
        setTimeout(async () => {
            var data = await usersApi.getAllUsers();
            //console.log(data);
            resolve(data.users);
        }, 500);
    });
};

const CreateUser = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('seecity@123');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [role, setRole] = useState(['rol_orjOTB4fi8GB9MBt']);
    const [roleName, setRolesName] = useState(['Member']);
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState('');
    const [userdata, setUserData] = useState([]);
    const [selectedRowIndex, setselectedRowIndex] = useState(-1);
    const [selectedRowRoles, setSelectedRowRoles] = useState([
        'rol_orjOTB4fi8GB9MBt',
    ]);
    const [selectedValues, setselectedValues] = useState('');
    const grid = useRef(null);
    const currentUser = useSelector((state) => state.user);
    const [cuurentRole, setcuurentRole] = useState([]);
    //const [cuurentRole, setcuurentRole] = useState([]);

    useEffect(() => {
        const init = async () => {
            setIsLoading(true);
            await authenticationService.getManagementAccessToken();
            await rolesFromAuth0();
            await fetchUser();
            setIsLoading(false);
        };
        init();
    }, []);

    const rolesFromAuth0 = async () => {
        //await authenticationService.getManagementAccessToken();
        var token = sessionStorage.getItem('Token');
        const requestOptions2 = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            },
        };
        await fetch('https://seecity.auth0.com/api/v2/roles', requestOptions2)
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                setcuurentRole(data.filter((x) => x.name !== 'Admin'));
                if (currentUser.role === 'Admin') {
                    setcuurentRole(data.filter((x) => x.name === 'Admin'));
                }
            });

        //  https://seecity.auth0.com/api/v2/roles
    };

    const fetchUser = async () => {
        const results = await fetchApi();

        const filterUser = [];
        const cuurentRole = [currentUser];
        //setcuurentRole(currentUser.role);

        results.forEach((rt) => {
            if (rt.roles[0] === currentUser.role) filterUser.push(rt);
        });
        let unique = [...new Set(filterUser)];
        setUserData(unique);
    };

    const saveUser = async (data) => {
        // alert('in save user');
        console.log(data.Tags);
        //setIsLoading(true);
        await authenticationService.getManagementAccessToken();
        var token = sessionStorage.getItem('Token');
        const requestOptions2 = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            },
            body: JSON.stringify({
                connection: 'Username-Password-Authentication',
                email: data.email,
                password: password,
                email_verified: true,
                user_metadata: {
                    forceReset: true,
                    email: data.email,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    phone: data.phone,
                    password: password,
                },
            }),
        };
        await fetch('https://seecity.auth0.com/api/v2/users', requestOptions2)
            .then((response) => {
                if (!response.ok) {
                    if (response.status == 409) {
                        var errorMessage = document.getElementById(
                            'error-message'
                        );
                        errorMessage.innerHTML = 'User already exist';
                        errorMessage.style.display = 'block';
                        throw response;
                    }
                }
                return response.json(); //we only get here if there is no error
            })
            .then(async (data) => {
                sessionStorage.setItem('id', data.user_id);
                const roleNames = [];

                role.forEach((rt) => {
                    let r1 = cuurentRole.find((r) => r.id === rt);
                    roleNames.push(r1.name);
                });

                var res = await apolloClient.performMutation(addUsers, {
                    user: {
                        name:
                            data.user_metadata.firstName +
                            ' ' +
                            data.user_metadata.lastName,
                        userId: data.user_id,
                        firstName: data.user_metadata.firstName,
                        lastName: data.user_metadata.lastName,
                        email: data.user_metadata.email,
                        phone: data.user_metadata.phone,
                        roles: roleNames,
                        createdBy: currentUser.firstName,
                    },
                });
            });

        var id = sessionStorage.getItem('id');
        var token = sessionStorage.getItem('Token');

        const requestOptions3 = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                Authorization: 'Bearer ' + token,
            },
            body: JSON.stringify({
                roles: role,
            }),
        };
        await fetch(
            `https://seecity.auth0.com/api/v2/users/${id}/roles`,
            requestOptions3
        ).then((data) => {
            //alert('success');
        });
        // sessionStorage.removeItem('id');
        sessionStorage.removeItem('Token');
        // setIsLoading(false);
        authenticationService.findAccount(data.email);
    };

    const selectedChanged = (e) => {
        setselectedValues(e);

        setselectedRowIndex(e);
        setselectedRowIndex(e.component.getRowIndexByKey(e.selectedRowKeys[0]));
        console.log(selectedRowIndex);
        console.log(e);
        if (e.selectedRowsData.length > 0) {
            console.log(e.selectedRowsData);
            setRole([
                cuurentRole.find(
                    (x) => x.name === e.selectedRowsData[0].roles[0]
                ).id,
            ]);
            console.log(
                cuurentRole.find(
                    (x) => x.name === e.selectedRowsData[0].roles[0]
                ).id
            );
        }
    };
    const updateAddrow = async (e) => {
        // const roleNames = [];
        var data = e.data;

        await saveUser(data);
    };

    const deleteRowAuth0 = async (e) => {
        var id = e.data.userId;
        await authenticationService.getManagementAccessToken();
        var token = sessionStorage.getItem('Token');
        const requestOptions = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            },
        };
        await fetch(
            `https://seecity.auth0.com/api/v2/users/${id}`,
            requestOptions
        ).then(async (data) => {
            var user = await apolloClient.performMutation(deleteUser, {
                userId: id,
            });
        });
        //.then((response) => response.json())
        //.then((data) => {});
        sessionStorage.removeItem('id');
        sessionStorage.removeItem('Token');
    };
    const addRow = () => {
        grid.current.instance.addRow();
        grid.current.instance.deselectAll();
    };
    const deleteRow = (e) => {
        grid.current.instance.deleteRow(selectedRowIndex);
        grid.current.instance.deselectAll();
    };
    const editRow = (e) => {
        grid.current.instance.editRow(selectedRowIndex);
        grid.current.instance.deselectAll();
    };
    const resendMail = async () => {
        var email = selectedValues.selectedRowsData[0].email;
        var id = selectedValues.selectedRowKeys[0].userId;
        await authenticationService.getManagementAccessToken();
        var token = sessionStorage.getItem('Token');
        const requestOptions = {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            },
            body: JSON.stringify({
                user_metadata: { forceReset: true },
                password: 'seecity@123',
            }),
        };
        await fetch(
            `https://seecity.auth0.com/api/v2/users/${id}`,
            requestOptions
        )
            .then((response) => response.json())
            .then((data) => {});
        sessionStorage.removeItem('id');
        sessionStorage.removeItem('Token');
        authenticationService.findAccount(email);
    };
    const editedRow = async (e) => {
        console.log(e);
        var modifiedData = e.data;
        var id = modifiedData.userId;
        await authenticationService.getManagementAccessToken();
        var token = sessionStorage.getItem('Token');
        const requestOptions = {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            },
            body: JSON.stringify({
                connection: 'Username-Password-Authentication',
                email: modifiedData.email,
                user_metadata: {
                    email: modifiedData.email,
                    firstName: modifiedData.firstName,
                    lastName: modifiedData.lastName,
                    phone: modifiedData.phone,
                },
            }),
        };
        await fetch(
            `https://seecity.auth0.com/api/v2/users/${id}`,
            requestOptions
        )
            .then((response) => response.json())
            .then(async (data) => {
                const user = await apolloClient.performMutation(updateUser, {
                    user: {
                        name: modifiedData.firstName,
                        userId: modifiedData.userId,
                        firstName: modifiedData.firstName,
                        lastName: modifiedData.lastName,
                        email: modifiedData.email,
                        phone: modifiedData.phone,
                        roles: modifiedData.roles,
                        createdBy: currentUser.firstName,
                    },
                });
            });
    };
    const sendRequest = async (value) => {
        return new Promise((resolve) => {
            setTimeout(async () => {
                await authenticationService.getManagementAccessToken();
                var token = sessionStorage.getItem('Token');
                const requestOptions2 = {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + token,
                    },
                };
                await fetch(
                    `https://seecity.auth0.com/api/v2/users-by-email?email=${value}`,
                    requestOptions2
                )
                    .then((response) => response.json())
                    .then((data) => {
                        if (data.length === 0) {
                            resolve(true);
                        } else {
                            resolve(false);
                        }
                    })
                    .catch((error) => {
                        resolve(false);
                    });
            }, 1000);
        });
    };

    const asyncValidation = (params) => {
        return sendRequest(params.value);
    };

    return (
        <div className="px-2 sm:px-3 h-full">
            {!isLoading ? (
                <div className="min-h-full grid grid-cols-1 sm:grid-cols-1 col-gap-4">
                    <div className="mb-2 flex flex-col overflow-y-hidden">
                        <div className="flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                            <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6 flex items-center justify-between flex-wrap sm:flex-no-wrap">
                                <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                    Users
                                </h3>
                            </div>
                            <div
                                id="error-message"
                                className="alert alert-danger text-red-700"
                            ></div>
                            <div>
                                <DataGrid
                                    id="grid"
                                    className="p-2"
                                    dataSource={userdata}
                                    ref={grid}
                                    selection={{ mode: 'single' }}
                                    showBorders={true}
                                    onSelectionChanged={selectedChanged}
                                    onRowInserted={updateAddrow}
                                    onRowRemoved={deleteRowAuth0}
                                    onRowUpdated={editedRow}
                                >
                                    <Paging enabled={false} />
                                    {/* <Editing mode="popup" allowUpdating={true} /> */}
                                    <Editing
                                        mode="form"
                                        // allowUpdating={true}
                                        // allowDeleting={true}
                                    >
                                        {/* <Popup
                                        title="User Info"
                                        showTitle={true}
                                        width={700}
                                        height={525}
                                    >
                                        <Position
                                            my="top"
                                            at="top"
                                            of={window}
                                        />
                                    </Popup> */}
                                    </Editing>
                                    <Column dataField="firstName">
                                        <ValidationRule type="required" />
                                    </Column>
                                    <Column dataField="lastName">
                                        <ValidationRule type="required" />
                                    </Column>
                                    <Column
                                        dataField="email"
                                        // width={350}
                                        alignment="left"
                                    >
                                        <ValidationRule type="required" />
                                        <AsyncRule
                                            message="Email is already registered"
                                            validationCallback={asyncValidation}
                                        />
                                    </Column>
                                    <Column
                                        dataField="phone"
                                        // width={350}
                                        alignment="left"
                                    >
                                        <ValidationRule type="required" />
                                    </Column>
                                    <Column dataField="roles">
                                        <FormItem>
                                            <TagBox
                                                //defaultValue={[0]}
                                                //applyValueMode="useButtons"
                                                tagRender={tagTemplate}
                                                showSelectionControls={true}
                                                hideSelectedItems={true}
                                                showDropDownButton={true}
                                                dataSource={cuurentRole}
                                                placeholder="Choose Role"
                                                displayExpr="name"
                                                valueExpr="id"
                                                //showClearButton={true}
                                                value={role}
                                                onValueChanged={(e) =>
                                                    setRole(e.value)
                                                }
                                            >
                                                <Validator>
                                                    <RequiredRule message="Role is required" />
                                                </Validator>
                                            </TagBox>
                                        </FormItem>
                                    </Column>
                                </DataGrid>

                                <SpeedDialAction
                                    icon="add"
                                    label="Create User"
                                    index={1}
                                    onClick={addRow}
                                    // width={100}
                                />
                                <SpeedDialAction
                                    icon="trash"
                                    label="Delete row"
                                    index={2}
                                    visible={
                                        selectedRowIndex !== undefined &&
                                        selectedRowIndex !== -1
                                    }
                                    onClick={deleteRow}
                                />
                                <SpeedDialAction
                                    icon="edit"
                                    label="Edit row"
                                    index={3}
                                    visible={
                                        selectedRowIndex !== undefined &&
                                        selectedRowIndex !== -1
                                    }
                                    onClick={editRow}
                                />
                                <SpeedDialAction
                                    icon="repeat"
                                    label="Resend Invitation"
                                    index={4}
                                    onClick={resendMail}
                                    visible={
                                        selectedRowIndex !== undefined &&
                                        selectedRowIndex !== -1
                                    }
                                />
                            </div>
                        </div>
                    </div>
                </div>
            ) : (
                <Loading></Loading>
            )}
        </div>
    );
};
export default CreateUser;
