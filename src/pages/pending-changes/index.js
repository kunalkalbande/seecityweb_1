import React, { useState, useEffect, useRef } from 'react';
import { Paging, Label } from 'devextreme-react/data-grid';
import { apolloClient } from '../../api/graphql/apolloClient';
import DataGrid, {
    Column,
    Editing,
    MasterDetail,
} from 'devextreme-react/data-grid';
import { useSelector } from 'react-redux';
import { lookup } from '../../api/graphql/lookup';
import { accountSetup } from '../../api/graphql/accountSetup';
import Loading from '../../components/common/Loading';
import { SpeedDialAction } from 'devextreme-react/speed-dial-action';
import { Popup } from 'devextreme-react/popup';
import { TextArea, Button, List } from 'devextreme-react';
import { changeRequest } from '../../api/graphql/changeRequest';
import { usersApi } from '../../api/graphql/users';
import { RadioGroup } from 'devextreme-react';
import { render } from '@testing-library/react';
import moment from 'moment';
import { products } from '../../data/products';
import { Background } from 'devextreme-react/range-selector';
import { findByKey } from '../../utils';
import LoadingV2 from '../../components/common/LoadingV2';

const PendingChanges = () => {
    const [data, setData] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [isLoadingV2, setIsLoadingV2] = useState(false);
    const currentUser = useSelector((state) => state.user);
    const [popupVisible, setPopupVisible] = useState(false);
    const [accountTags, setAccountTags] = useState('');
    const [accountTypes, setAccountTypes] = useState('');
    const [states, setStates] = useState('');
    const [users, setUsers] = useState('');
    const [fields, setFields] = useState('');
    const [isImageChange, setIsImageChange] = useState(false);

    const [pendingAccounts, setPendingAccounts] = useState([]);
    const [selectedRowIndex, setselectedRowIndex] = useState(-1);
    const [selectedAccount, setSelectedAccount] = useState({});
    const [selectedChild, setSelectedChild] = useState({});

    const [comment, setComment] = useState('');
    const [title, setTitle] = useState('Approve');
    const [popupTitle, setPopupTitle] = useState('');
    const [imgOne, setImageOne] = useState('');
    const [imgTwo, setImageTwo] = useState('');
    const [accepted, setAccepted] = useState('Accepted');
    const [imgOnedimensions, setImgOneDimensions] = useState({});
    const [imgTwodimensions, setImgTwoDimensions] = useState({});
    const [value, setValue] = useState([]);
    const selectedItemKeys = [];
    const [img1, setImage1] = useState('');
    const [img2, setImage2] = useState('');
    const [images, setImages] = useState([]);
    useEffect(() => {
        const init = async () => {
            setIsLoading(true);

            var acctagdata = await lookup.getLookupByType('Business Tags');
            if (acctagdata) {
                setAccountTags(acctagdata);
            }
            const data1 = await lookup.getLookupByType('Account Tags');
            setAccountTypes(data1);
            const data2 = await lookup.getLookupByType('States');
            setStates(data2)
            var userData = await usersApi.getAllUsers();
            if (userData.users) {
                setUsers(userData.users);
            }
            var pendingAccounts = await changeRequest.getChangeRequests();

            setPendingAccounts(pendingAccounts.changeRequestMany);
            setIsLoading(false);
        };
        init();
    }, []);
    const okClick = async () => {
        if (accepted === 'Not Accepted') {
            setTitle(`${selectedAccount.account.venueName} is Not Accepted`);

            if (selectedAccount.type === 'Account Adjustment') {
                var msg =
                    '<b>Filed Name: </b>  ' +
                    selectedChild.field +
                    ',' +
                    ' <b>Current Value:</b> ' +
                    selectedChild.currentValue +
                    ',' +
                    ' <b>New Value:</b> ' +
                    selectedChild.newValue;
                accountSetup.sendEmails({
                    to: 'tanvi.kansara@guestx.com',
                    text: `${title}`,
                    html: `<h4>your request for ${selectedChild.field} is Not Accepted</h4><p> ${msg} </p>  <p>${comment}</p>`,
                });
            } else {
                accountSetup.sendEmails({
                    // to: 'dnyaneshwar.wadghane@guestx.com',
                    to: 'tanvi.kansara@guestx.com',
                    text: `${title}`,
                    html: `<h4>your request for  Account Image Change is Not Accepted</h4> 
                   
                        <div class="flex flex-wrap -mx-2 justify-center" style="
                        display: flex;
                        ">
                        <div class="w-1/2 flex flex-col" style="
                        width: 50%;
                        ">
                        <div class="text-center" style="
                        ">Current Value</div>
                        <div id="divimg1" class="w-full px-2 flex justify-center"><img id="img1" alt="img one" 
                        src=${img1} style="height: 160px; width: 160px; margin: 10px 0px;"></div>

                        </div>
                        <div class="w-1/2 flex flex-col" style="
                        width: 50%;
                        ">
                        <div class="text-center">New Value</div>
                        <div id="divimg2" class="w-full px-2 flex justify-center"><img id="img2" 
                        src=${img2} style="height: 160px; width: 160px; margin: 10px 0px;"></div>

                        </div>
                        </div>
                       
                    <p>${comment}</p>`,
                });
            }
        }
        else if (accepted === "Accepted") {
            setIsLoadingV2(true);
            const accountObj = await fetchAccount(selectedAccount.accountId);
            var accountNew = rebuildAccountObj(accountObj);
            accountNew._id = selectedAccount.accountId;
            const res = await updateAccount(accountNew);
            console.log("is updated -> ", res);
            setIsLoadingV2(false);
        }

        setPopupVisible(false);
        setComment('');
    };

    const fetchAccount = async (accountId) => {
        return new Promise((resolve) => {
            setTimeout(async () => {
                var res = await accountSetup.getAccountSetupDetails(accountId);
                resolve(res.accountById);
            }, 1000);
        });
    };

    const updateAccount = async (accountObj) => {
        return new Promise((resolve) => {
            setTimeout(async () => {
                var res = await accountSetup.updateAccountById(accountObj);
                resolve(res.updateAccountById);
            }, 1000);
        });
    };

    const rebuildAccountObj = (accountObj) => {
        if (selectedAccount.type === 'Account Adjustment') {
            switch (selectedChild.field) {
                case 'Legal Company Name': accountObj.companyName = selectedChild.newValue;
                    break;
                case 'Address Line One': accountObj.address.addressOne = selectedChild.newValue;
                    break;
                case 'Address Line Two': accountObj.address.addressTwo = selectedChild.newValue;
                    break;
                case 'City': accountObj.address.city = selectedChild.newValue;
                    break;
                case 'State': accountObj.address.stateId = selectedChild.newValue;
                    break;
                case 'Zip': accountObj.address.zip = selectedChild.newValue;
                    break;
                case 'Business Tags': accountObj.tags = selectedChild.newValue;
                    break;
                case 'Account Type': accountObj.accountTypeId = selectedChild.newValue;
                    break;
                case 'VenueName': accountObj.venueName = selectedChild.newValue;
                    break;
                case 'Phone Number': accountObj.address.phoneNumber = selectedChild.newValue;
                    break;
                case 'Operating Period Days/Hour': accountObj.operatingPolicy = selectedChild.newValue;
                    break;
                case 'Brand Details': accountObj.brandDetail = selectedChild.newValue;
                    break;
                case 'Seller': accountObj.isSeller = selectedChild.newValue;
                    break;
                case 'Provider/Seller': accountObj.isProviderSeller = selectedChild.newValue;
                    break;
            }
        }
        else if (selectedAccount.type === 'BankAccount Adjustment') {
            switch (selectedChild.field) {
                case 'Bank Account Type': accountObj.bankdetail.bankAccountType = selectedChild.newValue;
                    break;
                case 'Account Number': accountObj.bankdetail.accountNumber = parseInt(selectedChild.newValue);
                    break;
                case 'Routing Number': accountObj.bankdetail.routingNumber = parseInt(selectedChild.newValue);
                    break;
            }
        }
        else if (selectedAccount.type === 'Account Image Adjustment') {
            accountObj.file[0].path = selectedChild.newValue;
        }

        return accountObj;
    }

    const selectedChanged = (e) => {
        setselectedRowIndex(e);

        console.log(e.component.getRowIndexByKey(e.selectedRowKeys[0]));
    };

    const selectedChildChanged = (e) => {
        var date = moment(e.selectedRowsData[0].createdAt).format('MM/DD/YYYY');
        e.selectedRowsData[0].createdAt = date;
        e.selectedRowsData[0].comment = 'Tulsa, Ok';
        setSelectedChild(e.selectedRowsData[0]);
    };

    const approve = (e) => {
        setSelectedAccount(e.row.data);
        console.log(e.row.data);
        setImages(e.row.data.fields)
        if (e.row.data.type === 'Account Image Adjustment') {
            setIsImageChange(true);
        } else {
            setIsImageChange(false);
        }
        setTitle(`${e.row.data.account.venueName} is Not Accepted`);

        setPopupTitle(e.row.data.type + ' Details');
        setFields(e.row.data.fields);
        setPopupVisible(true);
    };

    const onTextAreaValueChanged = (e) => {
        setComment(e.value);
    };
    const handleSizeImgOne = ({ target: img }) => {
        var dimensions = {};
        dimensions.height = img.naturalHeight;
        dimensions.width = img.naturalWidth;
        setImgOneDimensions(dimensions);
        console.log('height' + img.naturalWidth, img.naturalHeight);
    };
    const handleSizeImgTwo = ({ target: img }) => {
        var dimensions = {};
        dimensions.height = img.naturalHeight;
        dimensions.width = img.naturalWidth;
        setImgTwoDimensions(dimensions);
        console.log('height' + img.naturalWidth, img.naturalHeight);
    };
    const bgStyle = { background: '#337ab7' };

    const ProductInfo = (item) => {
        return (
            <div
                className={
                    img1 === item.currentImage && img2 === item.newImage
                        ? 'bgSelect'
                        : null
                }
            >
                <div
                    className="flex flex-wrap -mx-2 justify-center"
                // style={{ img1 === item.currentImage ? bgStyle : ''}}
                >
                    <div className="w-1/2 flex flex-col">
                        <div className="text-center">Current Value</div>
                        <div
                            id="divimg1"
                            className="w-full px-2 flex justify-center"
                        >
                            <img
                                onLoad={handleSizeImgOne}
                                id="img1"
                                alt="img one"
                                src={item.currentValue}
                                style={{
                                    height: '160px',
                                    width: '160px',
                                    margin: '10px 0',
                                    index: 1,
                                }}
                            />
                        </div>
                        <div className="text-center">
                            Dimensions: {imgOnedimensions.width} x{' '}
                            {imgOnedimensions.height}
                        </div>
                    </div>
                    <div className="w-1/2 flex flex-col">
                        <div className="text-center">New Value</div>
                        <div
                            id="divimg2"
                            className="w-full px-2 flex justify-center"
                        >
                            <img
                                onLoad={handleSizeImgTwo}
                                id="img2"
                                src={item.newValue}
                                style={{
                                    height: '160px',
                                    width: '160px',
                                    margin: '10px 0',
                                    index: 1,
                                }}
                            />
                        </div>
                        <div className="text-center">
                            Dimensions: {imgTwodimensions.width} x{' '}
                            {imgTwodimensions.height}
                        </div>
                    </div>
                </div>
            </div>
        );
    };
    const onSelectedItemKeysChange = (e) => {
        console.log(e);
        setSelectedChild(e.itemData);
        setImage1("https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg");
        setImage2("https://image.shutterstock.com/image-photo/autumn-forest-nature-vivid-morning-600w-766886038.jpg");
        //setImage1(e.itemData.currentValue);
        //setImage2(e.itemData.newValue);
    };
    const calculateCurrentCellValue = (data) => {
        switch (data.field) {
            case 'Business Tags': {
                var currValue = [];
                data.currentValue.split(',').forEach((element) => {
                    currValue.push(findByKey(element, accountTags).name);
                });
                return currValue.toString();
            }

            case 'Account Type': {
                return findByKey(data.currentValue, accountTypes).name;
            }
            case 'State':
                return findByKey(data.currentValue, states).name;
            default: {
                return data.currentValue;
            }
        }
    };
    const calculateNewCellValue = (data) => {
        console.log("data", data);
        switch (data.field) {
            case 'Business Tags':
                var newValue = [];
                data.newValue.split(',').forEach((element) => {
                    newValue.push(findByKey(element, accountTags).name);
                });
                return newValue.toString();
            case 'Account Type':
                return findByKey(data.newValue, accountTypes).name;
            case 'State':
                return findByKey(data.newValue, states).name;
            default:
                return data.newValue;
        }
    }
    return (
        <div className="px-2 sm:px-3 h-full">
            {!isLoading ? (
                <div>
                    <div className="min-h-full grid grid-cols-1 sm:grid-cols-1 col-gap-4">
                        <div className="mb-2 flex flex-col overflow-y-hidden">
                            <div className="flex flex-1 flex-col bg-white shadow-sm sm:rounded-md">
                                <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6 flex items-center justify-between flex-wrap sm:flex-no-wrap">
                                    <h3 className="text-lg leading-6 font-medium text-sc-blue-500">
                                        Change Requests
                                    </h3>
                                </div>
                                <div
                                    id="error-message"
                                    className="alert alert-danger text-red-700"
                                ></div>

                                <React.Fragment>
                                    <DataGrid
                                        id="gridContainer"
                                        keyExpr="_id"
                                        className="p-2"
                                        dataSource={pendingAccounts}
                                        showRowLines={true}
                                        allowColumnReordering={true}
                                        //selection={{ mode: 'single' }}
                                        onSelectionChanged={selectedChanged}
                                        showBorders={true}
                                    >
                                        <Paging enabled={true} />

                                        <Column
                                            dataField="type"
                                            caption="Change"
                                        />

                                        <Column
                                            dataField="account.venueName"
                                            caption="Account"
                                        />
                                        {/* <Column
                                            dataField="market"
                                            caption="Market"
                                        /> */}
                                        <Column
                                            dataField="account.address.city"
                                            caption="City"
                                        />
                                        <Column
                                            dataField="account.userId"
                                            caption="User"
                                            lookup={{
                                                dataSource: users,
                                                displayExpr: 'name',
                                                valueExpr: 'userId',
                                            }}
                                        />

                                        <Column
                                            dataType="date"
                                            dataField="account.createdAt"
                                            caption="Date"
                                        />
                                        <Column
                                            type="buttons"
                                            width={110}
                                            buttons={[
                                                {
                                                    hint: 'Details',
                                                    text: 'Details',
                                                    onClick: approve,
                                                },
                                            ]}
                                        />
                                    </DataGrid>
                                    {/* <SpeedDialAction
                                        icon="repeat"
                                        label="Details"
                                        index={0}
                                        visible={
                                            selectedRowIndex !== undefined &&
                                            selectedRowIndex !== -1
                                        }
                                        onClick={approve}
                                    /> */}
                                </React.Fragment>
                            </div>
                        </div>
                    </div>
                    <Popup
                        visible={popupVisible}
                        onHiding={() => setPopupVisible(false)}
                        dragEnabled={false}
                        closeOnOutsideClick={true}
                        showTitle={true}
                        title={popupTitle}
                        width={1050}
                        height={650}
                    >
                        {isImageChange ? (
                            <div className="w-full my-3 rounded flex justify-center flex-col">
                                {isLoadingV2 ? (<LoadingV2 />) : (<></>)}
                                <List
                                    height="250px"
                                    width="960px"
                                    margin="10px "
                                    dataSource={images}
                                    itemRender={ProductInfo}
                                    showSelectionControls={true}
                                    selectedItemKeys={selectedItemKeys}
                                    onItemClick={onSelectedItemKeysChange}
                                // selectionMode="single"
                                />
                            </div>
                        ) : (
                                <div style={{ marginLeft: 10, marginRight: 10 }}>
                                    {isLoadingV2 ? (<LoadingV2 />) : (<></>)}
                                    <div className="text-sm mb-3">
                                        {selectedChild.currentValue}
                                        <div className="text-sm  float-right">
                                            {selectedChild.createdAt}
                                        </div>
                                    </div>
                                    <div className="text-sm mb-3">
                                        {' '}
                                        {selectedChild.comment}
                                    </div>
                                    <DataGrid
                                        id="gridContainer"
                                        wordWrapEnabled="true"
                                        keyExpr="_id"
                                        style={{ maxHeight: "200px" }}
                                        dataSource={fields}
                                        allowColumnReordering={true}
                                        selection={{ mode: 'single' }}
                                        onSelectionChanged={selectedChildChanged}
                                        showRowLines={true}
                                        showBorders={true}
                                    >
                                        <Paging enabled={true} />
                                        <Column
                                            dataField="field"
                                            caption="Field Name"
                                        />
                                        <Column
                                            calculateCellValue={calculateCurrentCellValue}
                                            dataField="currentValue"
                                            caption="Current Value"
                                        />
                                        <Column
                                            calculateCellValue={calculateNewCellValue}
                                            dataField="newValue"
                                            caption="New Value"
                                        />
                                    </DataGrid>
                                </div>
                            )}
                        <div
                            style={{ margin: 10 }}
                            className="text-sm m-3 mt-0"
                        >
                            Comment:
                            <TextArea
                                style={{ marginTop: 10 }}
                                value={comment}
                                onValueChanged={onTextAreaValueChanged}
                                hint="comment"
                                height={105}
                                maxLength={105}
                            />
                        </div>

                        <div className="py-5">
                            <RadioGroup
                                items={['Accepted', 'Not Accepted']}
                                value={accepted}
                                layout="horizontal"
                                onValueChanged={(e) => setAccepted(e.value)}
                            />
                        </div>
                        <div className="py-4 border-t border-gray-200 flex flex-col items-end">
                            <div className="text-right">
                                <Button
                                    className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-blue-200 shadow-sm hover:bg-sc-blue-300 focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                                    onClick={okClick}
                                    text="OK"
                                />
                                <Button
                                    className="w-24 mx-1 border border-transparent text-xs font-medium rounded-none sm:rounded-sm text-white bg-sc-gray-dark shadow-sm hover:bg-sc-gray-light focus:outline-none focus:shadow-outline-blue focus:bg-sc-blue-300 active:bg-sc-blue-300 transition duration-150 ease-in-out"
                                    type="normal"
                                    text="Cancel"
                                    onClick={() => {
                                        setComment('');
                                        setPopupVisible(false);
                                    }}
                                />
                            </div>
                        </div>
                    </Popup>
                </div>
            ) : (
                    <Loading />
                )}
        </div>
    );
};
export default PendingChanges;
