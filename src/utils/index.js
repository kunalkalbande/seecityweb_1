import { useEffect, useRef } from 'react';
import { createBrowserHistory } from 'history';
import moment from 'moment';

export const history = createBrowserHistory();

export const classNames = (...classes) => {
    return classes.filter(Boolean).join(' ');
};

export const usePrevious = (value) => {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
};

const except = (array1, array2) => {
    var temp = [];
    array1 = array1.toString().split(',').map(String);
    array2 = array2.toString().split(',').map(String);

    for (var i in array1) {
        if (array2.indexOf(array1[i]) === -1) temp.push(array1[i]);
    }
    for (i in array2) {
        if (array1.indexOf(array2[i]) === -1) temp.push(array2[i]);
    }
    return temp.sort((a, b) => a - b);
};

const distinct = (value, index, self) => {
    return self.indexOf(value) === index;
};

export const arrayUtils = {
    except,
    distinct,
};

const between = (startDate, endDate, inclusive = true) => {
    var dates = [];

    var currDate = moment(startDate).startOf('day');
    var lastDate = moment(endDate).startOf('day');

    if (inclusive) dates.push(currDate.toDate());
    while (currDate.add(1, 'days').diff(lastDate) < 0) {
        dates.push(currDate.clone().toDate());
    }
    if (inclusive) dates.push(lastDate.toDate());

    return dates;
};

export const dateUtils = {
    between,
};

export const findByKey =(nameKey, arrayList)=>{
    for (var i=0; i < arrayList.length; i++) {
        if (arrayList[i].value === nameKey) {
            return arrayList[i];
        }
    }
}

  