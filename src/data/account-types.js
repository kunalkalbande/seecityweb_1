export default [
    { value: 'AE', name: 'Attraction/Experience' },
    { value: 'ES', name: 'Event Space' },
    { value: 'PE', name: 'Pop-Up Event' },
    { value: 'TO', name: 'Tour Operator' },
    { value: 'CT', name: 'Convention/Tourism Agency' },
    { value: 'HR', name: 'Hostel/Resort' },
    { value: 'GR', name: 'General Local Reseller' },
];
