export default [
    { value: '1', name: 'Outdoor' },
    { value: '2', name: 'Animals' },
    { value: '3', name: 'Zoo' },
    { value: '4', name: 'Family' },
    { value: '5', name: 'Kids' },
    { value: '6', name: 'Downtown' },
];
