export default [
    {
        id: 'auth0|5ed8dff7det85d0befbc75a3',
        firstName: 'mock1',
        lastName: 'test',
        email: 'mock11@test.com',
        phone: '1234567890',
    },
    {
        id: 'auth0|5ed8dff7def75d0befbc75a3',
        firstName: 'user1',
        lastName: 'test',
        email: 'user1@test.com',
        phone: '1234567890',
    },
    {
        id: 'auth0|5ec2af33d047c90cc5717155',
        firstName: 'venumanager',
        lastName: 'manager',
        email: 'venuemanager@seecity.com',
        phone: '1234567890',
    }
];
