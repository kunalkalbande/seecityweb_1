export const products = [
    {
        ID: 1,
        currentImage:
            'https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg',
        newImage:
            'https://image.shutterstock.com/image-photo/autumn-forest-nature-vivid-morning-600w-766886038.jpg',
    },
    {
        ID: 2,
        currentImage:
            'https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg',
        newImage:
            'https://image.shutterstock.com/image-photo/mountains-during-sunset-beautiful-natural-260nw-407021107.jpg',
    },
    {
        ID: 3,
        currentImage:
            'https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg',
        newImage:
            'https://image.shutterstock.com/image-photo/autumn-forest-nature-vivid-morning-600w-766886038.jpg',
    },
];
