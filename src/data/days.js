export const daysOfTheWeek = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
];

export const dayParts = [
    { name: 'Early Morning', start: 7, end: 10 },
    { name: 'Late Morning', start: 10, end: 12 },
    { name: 'Mid Day', start: 12, end: '14' },
    { name: 'Afternoon', start: 14, end: 16 },
    { name: 'Early Evening', start: 16, end: 18 },
    { name: 'Evening', start: 18, end: 20 },
];
