const callback = async (handler) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            handler();
            resolve();
        }, 1000);
    });
};

const fetchMarkets = async () => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve([
                {
                    id: '1',
                    name: 'Dallas Forth Worth',
                    code: 'DFW',
                },
                {
                    id: '2',
                    name: 'New York City',
                    code: 'NYC',
                },
                {
                    id: '3',
                    name: 'San Diego',
                    code: 'SDG',
                },
            ]);
        }, 1000);
    });
};

const fetchAccounts = async (market) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            if (!market) resolve([]);
            else {
                resolve([
                    {
                        id: 1,
                        name: 'The Star',
                        market: '1',
                        address: { city: 'Frisco', state: 'TX' },
                    },
                    {
                        id: 2,
                        name: 'Pizza Hut Park',
                        market: '1',
                        address: { city: 'Frisco', state: 'TX' },
                    },
                    {
                        id: 3,
                        name: 'Soccer Hall of Fame',
                        market: '1',
                        address: { city: 'Frisco', state: 'TX' },
                    },
                    {
                        id: 4,
                        name: 'Plano Zoo',
                        market: '1',
                        address: { city: 'Plano', state: 'TX' },
                    },
                    {
                        id: 5,
                        name: 'Plano Museum',
                        market: '1',
                        address: { city: 'Plano', state: 'TX' },
                    },
                ]);
            }
        }, 1000);
    });
};

export default { fetchMarkets, fetchAccounts };
