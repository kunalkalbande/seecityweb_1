import { apolloClient } from './apolloClient';
import { saveProducts, updateProducts } from './mutations';
import { productsByAccountId, getProductById } from './queries';

const getProductsByAccountId = async (accountId) => {
    var response = await apolloClient.performQuery(productsByAccountId, {
        accountId: accountId,
    });
    return response;
};

const productById = async (productId) => {
    var response = await apolloClient.performQuery(getProductById, {
        productId: productId,
    });
    return response;
};

const saveProduct = async (product, productImages) => {
    var res = await apolloClient.performMutation(saveProducts, {
        product: product,
        file: productImages,
    });
    console.log(res);
    return res;
};
const updateProduct = async (product, file) => {
    var res = await apolloClient.performMutation(updateProducts, {
        product: product,
        file: file,
    });
    console.log(res);
    return res;
};
export const product = {
    saveProduct,
    getProductsByAccountId,
    updateProduct,
    productById,
};
