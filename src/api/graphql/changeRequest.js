import { apolloClient } from './apolloClient';
import { changeRequestsMany } from './queries';

import { saveChangeRequest } from './mutations';

const getChangeRequests = async () => {
    var response = await apolloClient.performQuery(changeRequestsMany, {});
    return response;
};

const saveChangeRequests = async (changeRequest) => {
    var res = await apolloClient.performMutation(saveChangeRequest, {
        changeRequest: changeRequest,
    });
    console.log(res);
    return res;
};

export const changeRequest = {
    getChangeRequests,
    saveChangeRequests,
};
