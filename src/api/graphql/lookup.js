import { apolloClient } from './apolloClient';
import { lookupByType, getAllLookups as allLookups } from './queries';
import { updateLookup as lookupUpdate } from './mutations';

const getLookupByType = async (type) => {
    return new Promise((resolve, reject) => {
        setTimeout(async () => {
            var response = await apolloClient.performQuery(lookupByType, {
                type: type,
            });
            if (response.lookupByType) {
                resolve(response.lookupByType.values);
            } else {
                reject();
            }
        }, 1000);
    });
};

const getAllLookups = async () => {
    return new Promise((resolve) => {
        setTimeout(async () => {
            var response = await apolloClient.performQuery(allLookups, {});
            resolve(response.lookups);
        }, 1000);
    });
};

const updateLookup = async (lookup) => {
    return new Promise((resolve) => {
        setTimeout(async () => {
            var response = await apolloClient.performMutation(lookupUpdate, {
                lookup: lookup,
            });
            resolve(response.updateLookup);
        }, 1000);
    });
};

const getCities = async () => {
    return new Promise((resolve) => {
        setTimeout(async () => {
            var response = await apolloClient.performQuery(lookupByType, {
                type: 'Cities',
            });
            resolve(response.lookupByType);
        }, 1000);
    });
};
export const lookup = {
    getLookupByType,
    getAllLookups,
    updateLookup,
    getCities,
};
