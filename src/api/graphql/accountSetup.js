import { apolloClient } from './apolloClient';
import { loader } from 'graphql.macro';
import { accountById, accountsByStatus } from './queries';

import {
    createAccounts,
    updateAccounts,
    updateAccountsById,
} from './mutations';
import { uploadLogo ,sendEmail} from './mutations';
const getAccountSetupDetails = async (accountId) => {
    var response = await apolloClient.performQuery(accountById, {
        accountId: accountId,
    });
    return response;
};
const getAccountsByStatus = async (status) => {
    var response = await apolloClient.performQuery(accountsByStatus, {
        status: status,
    });
    return response;
};
const createAccount = async (account, logo) => {
    var res = await apolloClient.performMutation(createAccounts, {
        account: account,
        file: logo,
    });
    console.log(res);
    return res;
};

const updateAccount = async (account, logo) => {
    var res = await apolloClient.performMutation(updateAccounts, {
        account: account,
        file: logo,
    });
    console.log(res);
    return res;
};

const updateAccountById = async (account) => {
    var res = await apolloClient.performMutation(updateAccountsById, {
        account: account,
    });
    console.log(res);
    return res;
};
const uploadLogos = async (accountId,file) => {
    var res = await apolloClient.performMutation(uploadLogo, {
        accountId: accountId,
        file: file,
    });
    console.log(res);
    return res;
};
const sendEmails=async(email)=>{
    var res=await apolloClient.performMutation(sendEmail,{email});
}
export const accountSetup = {
    getAccountSetupDetails,
    createAccount,
    uploadLogos,
    updateAccount,
    updateAccountById,
    getAccountsByStatus,
    sendEmails
};
