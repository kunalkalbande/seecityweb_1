import {apolloClient} from "./apolloClient";
import {operatingPeriods} from "./queries";
import {updateAccountOperatingPeriods} from "./mutations";
const getAccountOperatingPeriodsQuery=async(accountId)=>{
var response= await apolloClient.performQuery(operatingPeriods,{accountId:accountId});
return response;
}
const updateOperatingPeriods=async(accountId,operatingPeriods)=>
{
        var res= await apolloClient.performMutation(
                            updateAccountOperatingPeriods,
                            {accinput:{_id :accountId,operatingPeriods: operatingPeriods}});
        return res;
                         
}
export const operatingPeriodapi=
{
getAccountOperatingPeriodsQuery,
updateOperatingPeriods
}
