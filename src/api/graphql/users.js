import {apolloClient} from "./apolloClient";
import {getAllUser} from "./queries";
const getAllUsers=async()=>{
var response= await apolloClient.performQuery(getAllUser,{});
return response;
}

export const usersApi=
{
getAllUsers
}
