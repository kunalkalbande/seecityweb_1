import { loader } from 'graphql.macro';
export const operatingPeriods = loader('./accountOperatingPeriods.graphql');
export const userById = loader('./userById.graphql');
export const accountById = loader('./accountById.graphql');
export const getAllUser = loader('./users.graphql');
export const productsByAccountId = loader('./productByAccountId.graphql');
export const getAllMarkets = loader('./markets.graphql');
export const citiesByMarketId = loader('./citiesByMarketId.graphql');
export const lookupByType = loader('./lookupByType.graphql');
export const getAllLookups = loader('./lookups.graphql');
export const accountsByStatus = loader('./accountsByStatus.graphql');
export const changeRequestsMany = loader('./changeRequestMany.graphql');
export const getProductById = loader('./productById.graphql');
