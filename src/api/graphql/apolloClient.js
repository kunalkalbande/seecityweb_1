import ApolloClient from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloLink } from "apollo-link";
import { createUploadLink } from 'apollo-upload-client';
const omitDeepArrayWalk = (arr, key) => {
    return arr.map(val => {
        if (Array.isArray(val)) return omitDeepArrayWalk(val, key);
        else if (typeof val === "object") return omitDeep(val, key);
        return val;
    });
};

const omitDeep = (obj, key) => {
    const keys = Object.keys(obj);
    const newObj = {};
    keys.forEach(i => {
        if (i !== key) {
            const val = obj[i];
            console.log("val",val);
            if(typeof val === "object" && val !== null && val[0] && val[0].type && val[0].webkitRelativePath==='')
            {
                newObj[i] = val;
            }
            else if (val instanceof Date) newObj[i] = val;
            else if (Array.isArray(val)) newObj[i] = omitDeepArrayWalk(val, key);
            else if (typeof val === "object" && val !== null)
            {
                newObj[i] = omitDeep(val, key);
            }
            else newObj[i] = val;
        }
    });

    return newObj;

};
const cleanTypenameLink = new ApolloLink((operation, forward) => {
    if (operation.variables ) {
        // eslint-disable-next-line
        console.log("remove");
        operation.variables = omitDeep(operation.variables, "__typename");
    }

    operation.setContext({
        headers: {
            authorization: localStorage.getItem('access_token')
        }
    });

    return forward(operation);
});
const link = createUploadLink({
    uri: 'https://myseecityapi.netlify.app/'
});
const defaultOptions: DefaultOptions = {
    watchQuery: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'ignore',
    },
    query: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'all',
    },
}
const client = new ApolloClient({
    link: ApolloLink.from([
        cleanTypenameLink, link
    ]),
    cache: new InMemoryCache(),
    defaultOptions: defaultOptions
});


const performQuery = async (query, variables) => {
    try {
        console.log(link);
        console.log(client.cache);
        console.log(query, variables);
        const resp = (await client.query({ query: query, variables: variables }));
        return resp.data;
    }
    catch (ex) {
        return ex;
    }
}

const performMutation = async (mutation, variables) => {
    try {
        console.log(mutation, variables);

        const resp = (await client.mutate({ mutation: mutation, variables: variables }))
        return resp.data;
    }
    catch (ex) {
        return ex;
    }
}

export const apolloClient = {
    performQuery, performMutation
}