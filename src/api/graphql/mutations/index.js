import { loader } from 'graphql.macro';
export const updateAccountOperatingPeriods = loader(
    './operatingPeriods.graphql'
);
export const addUsers = loader('./user.graphql');
export const updateUser = loader('./updateUser.graphql');
 export const deleteUser ='';// loader('./deleteuser.graphql');
export const createAccounts = loader('./account.graphql');
export const saveProducts = loader('./product.graphql');
export const uploadLogo = loader('./logo.graphql');
export const updateAccounts = loader('./updateAccount.graphql');
export const createMarket = loader('./createMarket.graphql');
export const updateMarket = loader('./updateMarket.graphql');
export const createCity = loader('./createCity.graphql');
export const updateCity = loader('./updateCity.graphql');
export const updateAccountsById = loader('./updateAccountById.graphql');
export const saveChangeRequest = loader('./saveChangeRequest.graphql');
export const updateLookup = loader('./updateLookup.graphql');
export const sendEmail = loader('./email.graphql');
export const updateProducts = loader('./updateProduct.graphql');
