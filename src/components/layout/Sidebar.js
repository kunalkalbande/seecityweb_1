import React, { useEffect, useState } from 'react';
import { NavLink, useHistory, useRouteMatch } from 'react-router-dom';
import { useSelector } from 'react-redux';
import logo from '../../assets/img/logos/logo-primary-full-color-tm.png';
import { authenticationService } from '../../services';
// TODO: refactor how sidebar images are configured
import sblogo1 from '../../assets/img/Woman_Managing_the_System.png';
import sblogo2 from '../../assets/img/Woman_Painting_a_Masterpiece.png';

const Sidebar = ({ links }) => {
    let { path, url } = useRouteMatch();
    const history = useHistory();

    const [navLinks, setNavLinks] = useState([]);
    const [navImage, setNavImage] = useState(sblogo1);
    const currentUser = useSelector((state) => state.user);
    const className =
        // 'mt-1 mx-2 group flex items-center px-2 py-2 text-sm leading-5 font-medium text-sc-blue-200 rounded-md hover:text-white focus:text-white hover:bg-gray-700 focus:outline-none transition ease-in-out duration-150';
        'mt-1 mx-2 group flex items-center px-2 py-2 text-sm leading-5 font-medium text-gray-300 rounded-md hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-800 transition ease-in-out duration-150';
    const activeClassName =
        // 'text-white bg-gray-800 rounded-md focus:outline-none focus:text-white';
        'text-sc-blue-200 bg-gray-800 rounded-md focus:outline-none focus:text-sc-blue-200';

    useEffect(() => {
        return history.listen((location) => {
            if (location.pathname.includes('reports')) setNavImage(sblogo2);
            else setNavImage(sblogo1);
        });
    }, [history]);

    useEffect(() => {
        const results = links.filter((l) => {
            if (authenticationService.hasRoles(currentUser.roles, l.roles))
                return l;
        });
        setNavLinks(results);
    }, []);

    return (
        <div className="hidden md:flex md:flex-shrink-0">
            <div className="flex flex-col w-64">
                <div className="flex items-center h-16 flex-shrink-0 px-4 bg-gradient-b-light shadow">
                    <img className="h-8 w-auto" src={logo} alt="see.city" />
                </div>
                <div
                    className="h-0 flex-1 flex flex-col overflow-y-auto bg-sc-blue-500 bg-no-repeat bg-bottom "
                    style={{
                        backgroundImage: `url(${sblogo1})`,
                        backgroundSize: 260,
                    }}
                >
                    <nav className="flex-1 px-2 py-4">
                        {navLinks.map((route, index) => {
                            return (
                                <NavLink
                                    key={index}
                                    exact={route.exact}
                                    to={`${url}${route.to}`}
                                    className={className}
                                    activeClassName={activeClassName}
                                >
                                    <span className="pl-6 ">{route.name}</span>
                                </NavLink>
                            );
                        })}
                    </nav>
                    {/* <div className="bg-sc-blue-500">
                        <img className="w-auto" src={navImage} />
                    </div> */}
                </div>
            </div>
        </div>
    );
};

export default Sidebar;
