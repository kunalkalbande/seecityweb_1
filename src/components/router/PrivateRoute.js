/* eslint-disable react/prop-types */
import React from 'react';
import { Route, Redirect, useRouteMatch } from 'react-router-dom';
import { authenticationService } from '../../services';
import config from '.';

export const PrivateRoute = ({ component: Component, roles, ...rest }) => {
	const currentUser = authenticationService.currentUserValue;
	let { path } = useRouteMatch();
	return (
		<Route
			{...rest}
			render={(props) => {
				if (!currentUser) {
					// not logged in so redirect to login page with the return url
					console.log('login');
					return (
						<Redirect
							to={{
								pathname: '/login',
								state: { from: props.location },
							}}
						/>
					);
				}

				// check if route is restricted by role
				//if (roles && roles.indexOf(currentUser.role) === -1) {
				if (
					roles &&
					!authenticationService.hasRoles(currentUser.roles, roles)
				) {
					// role not authorised so redirect to home page
					return <Redirect to={{ pathname: '/403' }} />;

					//return <Redirect to={{ pathname: `/` }} />;
				}
				// authorised so return component
				return (
					<Component
						{...props}
						// TODO: move below to App.js/store
						router={config[currentUser.role.toLowerCase()].routes}
						links={config[currentUser.role.toLowerCase()].links}
					/>
				);
			}}
		/>
	);
};
