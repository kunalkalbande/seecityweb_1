import adminRoutes from './admin/Router';
import adminLinks from './admin/navigation';
import memberRoutes from './member/Router';
import memberLinks from './member/navigation';

const config = {
    admin: {
        routes: adminRoutes,
        links: adminLinks,
    },
    member: {
        routes: memberRoutes,
        links: memberLinks,
    },
};

export default config;
