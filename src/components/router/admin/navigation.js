const links = [
    { name: 'Dashboard', to: '', exact: true },
    { name: 'Markets & Cities', to: '/markets', exact: true },
    { name: 'Accounts', to: '/accounts' },
    { name: 'Lookups', to: '/lookups' },
    // { name: 'Accounts Types', to: '/account-types' },
    { name: 'Reports', to: '/reports' },
    { name: 'Administration', to: '/administration' },
    { name: 'Change Requests', to: '/pending-changes' },
];

export default links;
