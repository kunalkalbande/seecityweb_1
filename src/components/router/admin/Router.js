import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import {
    MarketListPage,
    AccountListPage,
    AccountPage,
    LookupsPage,
    CreateUser,
} from '../../../pages';
import PendingChanges from '../../../pages/pending-changes';

const NotAuthorized = () => (
    <span className="px-4 text-red-500">Not Authorized</span>
);

const PageNotFound = () => (
    <span className="px-4 text-red-500">Page Not Found</span>
);

const BasePage = ({ title }) => (
    <span className="px-4 text-gray-500">{title}</span>
);

const Routes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/accounts`}>
                <AccountListPage />
            </Route>
            <Route path={`${path}/account-types`}>
                <BasePage title="Account Types"></BasePage>
            </Route>
            <Route path={`${path}/lookups`}>
                <LookupsPage title="Lookups"></LookupsPage>
            </Route>
            <Route path={`${path}/markets`}>
                <MarketListPage />
            </Route>
            <Route path={`${path}/reports`}>
                <BasePage title="Reports"></BasePage>
            </Route>
            <Route exact path={`${path}`}>
                <BasePage title="Dashboard"></BasePage>
            </Route>
            <Route exact path={`${path}/administration`}>
                <CreateUser />
            </Route>
            <Route exact path={`${path}/pending-changes`}>
                <PendingChanges />
            </Route>
            <Route exact path={`${path}/403`}>
                <NotAuthorized />
            </Route>

            {/* <Route path="/accounts" exact component={AccountListPage} />
            <Route path="/accounts/:id" component={AccountPage} /> */}
        </Switch>
    );
};

export default Routes;
