const links = [
    {
        name: 'Account Setup',
        to: '/setup',
        exact: true,
        roles: ['Member', 'Setup'],
    },
    {
        name: 'Dashboard',
        to: '/dashboard',
        exact: true,
        roles: ['Member', 'MemberAdmin'],
    },
    {
        name: 'Profile',
        to: '/profile',
        exact: true,
        roles: ['Member', 'MemberAdmin'],
    },
    {
        name: 'Operations',
        to: '/operations',
        roles: ['Member', 'MemberAdmin'],
    },
    { name: 'Products', to: '/products', roles: ['Member', 'MemberAdmin'] },
    {
        name: 'Marketplace',
        to: '/marketplaceaaa',
        roles: ['Member', 'MemberAdmin'],
    },

    // { name: 'Redeem', to: '/redeem', roles: ['Member', 'Redeem'] },
    // { name: 'Sell', to: '/sell', roles: ['Member', 'Sell'] },
    // { name: 'Live View', to: '/live', roles: ['Member', 'MemberAdmin'] },
    // { name: 'Insights', to: '/insights', roles: ['Member', 'MemberAdmin'] },
    { name: 'Reports', to: '/reports', roles: ['Member', 'MemberAdmin'] },
    { name: 'Product Wizard', to: '/pw', roles: ['Member', 'MemberAdmin'] },
    {
        name: 'Administration',
        to: '/administration',
        exact: true,
        roles: ['Member', 'MemberAdmin'],
    },
];

export default links;
