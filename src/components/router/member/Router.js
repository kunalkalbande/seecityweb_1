/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import {
    Redirect,
    Route,
    Switch,
    useHistory,
    useRouteMatch,
} from 'react-router-dom';
import { useSelector } from 'react-redux';
import { PrivateRoute } from '../PrivateRoute';
import {
    AccountDashboardPage,
    AccountOperationsPage,
    AccountProductsPage,
    AccountProfilePage,
    AccountRedeemPage,
    AccountSellPage,
    AccountSetupPage,
    ProductWizard,
    Page1,
    Page2,
    Page3,
    Page4,
    CreateUser,
} from '../../../pages';
import Wizard from '../../../pages/product/product-wizard/ProductWizard';
import DashboardPage from '../../../pages/account/dashboard/Dashboard';

const NotAuthorized = () => (
    <span className="px-4 text-red-500">Not Authorized</span>
);

const PageNotFound = () => (
    <span className="px-4 text-red-500">Page Not Found</span>
);

const BasePage = ({ title }) => (
    <span className="px-4 text-gray-500">{title}</span>
);

const Routes = (props) => {
    let { path } = useRouteMatch();
    const history = useHistory();
    const [user, setUser] = useState(null);

    const currentUser = useSelector((state) => state.user);

    return (
        <div className="h-full">
            <Switch>
                <Route path={`${path}/dashboard`}>
                    <DashboardPage />
                </Route>
                <Route path={`${path}/setup`}>
                    <AccountSetupPage />
                </Route>
                <Route path={`${path}/profile`}>
                    <AccountProfilePage />
                </Route>
                <Route path={`${path}/operations`}>
                    <AccountOperationsPage />
                </Route>
                <Route path={`${path}/products`}>
                    <AccountProductsPage />
                </Route>
                <Route path={`${path}/marketplace`}>
                    <BasePage title="Marketplace"></BasePage>
                </Route>
                <PrivateRoute
                    path={`${path}/redeem`}
                    roles={['Member', 'Redeem']}
                    component={AccountRedeemPage}
                />
                <PrivateRoute
                    path={`${path}/sell`}
                    roles={['Member', 'Redeem']}
                    component={AccountSellPage}
                />
                <PrivateRoute
                    path={`${path}/administration`}
                    roles={['Member', 'MemberAdmin']}
                    component={CreateUser}
                />
                <Route path={`${path}/insights`}>
                    <BasePage title="Insights"></BasePage>
                </Route>
                <Route path={`${path}/reports`}>
                    <BasePage title="Reports"></BasePage>
                </Route>
                <Route exact path={`${path}`}>
                    {!currentUser.bag ? (
                        <Redirect to={`${path}/setup`} />
                    ) : (
                        <AccountDashboardPage />
                    )}
                    {/* <Wizard /> */}
                </Route>
                <Route exact path={`${path}/p1`}>
                    <Page1 />
                </Route>
                <Route exact path={`${path}/p2`}>
                    <Page2 />
                </Route>
                <Route exact path={`${path}/p3`}>
                    <Page3 />
                </Route>
                <Route exact path={`${path}/pw`}>
                    <ProductWizard />
                </Route>

                <Route exact path={`${path}/403`}>
                    <NotAuthorized />
                </Route>
                <Route>
                    <PageNotFound />
                </Route>
            </Switch>
        </div>
    );
};

export default Routes;
