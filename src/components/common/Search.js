import React, { useState, useEffect } from 'react';
import DebounceInput from 'react-debounce-input';
import { Icons } from '../icons';

const Search = ({ onSearch, value = '', minChars = 3 }) => {
    const [search, setSearch] = useState(value);

    useEffect(() => {
        onSearch(search);
    }, [search]);

    return (
        <div className="relative text-gray-300 focus-within:text-gray-400">
            <div className="absolute inset-y-0 left-2 flex items-center pointer-events-none">
                <Icons.SEARCH />
            </div>
            <div className="mt-1 shadow-sm">
                <DebounceInput
                    maxLength="10"
                    minLength={minChars}
                    debounceTimeout={200}
                    id="search_form"
                    value={search}
                    onChange={(event) => setSearch(event.target.value)}
                    className="form-input block w-full pl-8 transition duration-150 ease-in-out sm:text-sm sm:leading-5 rounded-none sm:rounded-sm placeholder-gray-300 "
                    placeholder="Search"
                />
                {!search ? (
                    ''
                ) : (
                    <div className="absolute inset-y-0 pt-1 right-0 flex items-center pointer-events-auto text-red">
                        <button
                            type="button"
                            className="px-2 py-2"
                            onClick={() => setSearch('')}
                        >
                            <Icons.SM_X_CIRCLE />
                        </button>
                    </div>
                )}
            </div>
        </div>
    );
};

export default Search;
