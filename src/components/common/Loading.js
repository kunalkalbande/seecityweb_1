import React from 'react';
const Loading = () => {
    return (
        <div className="flex flex-col items-center py-4">
            <div className="mt-4 loader ease-linear rounded-full border-8 border-t-8 border-gray-300 h-32 w-32 text-center"></div>
            <span className="mt-4 text-sm text-gray-400">Loading...</span>
        </div>
    );
};
export default Loading;
