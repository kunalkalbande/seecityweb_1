import React from 'react';
const LoadingV2 = () => {
    return (
        <div className="opacity-25 fixed inset-0 z-40 bg-black">
            <div className="flex flex-col items-center py-4 min-h-full">
                <div className="loader ease-linear rounded-full border-8 border-t-8 border-gray-300 h-32 w-32 text-center m-auto"></div>
                <span className="mt-4 text-sm text-gray-400">Loading...</span>
            </div>
        </div>
    );
};
export default LoadingV2;
