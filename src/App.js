import React from 'react';
import { Redirect, Router, Route, useHistory } from 'react-router-dom';
import { PrivateRoute } from './components/router/PrivateRoute';
import { LoginPage, VenueWelcomeScreen } from './pages';
import EmailVerificationPage from './pages/auth/confirmation/EmailVerification'; 

import { Shell } from './components/layout';
import { authenticationService } from './services';
import Callback from './services/Callback'

const NotAuthorized = () => (
	<span className="px-4 text-red-500">Not Authorized</span>
);

const App = () => {
	const currentUser = authenticationService.currentUserValue;
	const history = useHistory();

	return (
		<Router history={history}>
			<PrivateRoute path="/admin" roles={['Admin']} component={Shell} />
			<PrivateRoute path="/member" roles={['Member']} component={Shell} />
			<Route exact path="/">
				{!currentUser ? (
					// <Redirect to={`/login`} />
					<VenueWelcomeScreen />
				) : (
						<Redirect to={`/${currentUser.role.toLowerCase()}`} />
					)}
			</Route>
			<Route path="/callback" component={Callback} />
			<Route path="/login" component={LoginPage} />
 			<Route path="/verifyemail" component={EmailVerificationPage} />

			<Route exact path="/403">
				<NotAuthorized />
			</Route>
		</Router>
	);
};

export default App;
