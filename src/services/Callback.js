import React, { useEffect, useState } from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import { authenticationService } from './authentication.service';
import { useHistory } from 'react-router-dom';
import { setUser } from '../store/actions';
import { useDispatch } from 'react-redux';
import ForgotPassword from '../pages/auth/reset/ResetPassword';

const Callback = () => {
    const currentUser = authenticationService.currentUserValue;
    const history = useHistory();
    const dispatch = useDispatch();
    const [isValidUser, setIsValidUser] = useState(false);
    const [isError, setIsError] = useState(false);
    const [isReset, setIsForceReset] = useState(false);
    const [error, setError] = useState('');
    const [errorDescription, setErrorDescription] = useState('');
    useEffect(() => {
        // if (currentUser) {
        //     history.goBack();
        // }
        authenticationService.handleAuthentication().then((user) => {
            if (user) {
                if (user.error) {
                    setIsError(true);
                    setError(user.error);
                    setErrorDescription(user.errorDescription);
                } else {
                    if (user.forceReset) {
                        setIsForceReset(true);
                        localStorage.removeItem('currentUser');
                    } else {
                        dispatch(setUser(user));
                        setIsValidUser(true);
                    }
                }
            }
            //history.push('/');
        });
    });

    if (isValidUser) {
        return <Redirect to={`/${currentUser.role.toLowerCase()}`} />;
    }
    if (isReset) {
        return <ForgotPassword></ForgotPassword>;
        //setIsForceReset(true);
    }
    if (isError) {
        return (
            <div className="justify-center mt-10 flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                <div className="w-full max-w-4xl">
                    <div className="block cursor-pointer"></div>
                    <div
                        className="bg-red-100 h-48 border border-red-500 text-red-700 px-4 py-3"
                        role="alert"
                    >
                        <p className="font-bold text-2xl">{error}</p>
                        <p className="text-sm text-lg">{errorDescription}</p>
                        <p className="text-sm text-lg">
                            Please contact administrator
                        </p>
                        <div className="mt-7">
                            <a href="/" className="text-blue-500 mr-5">
                                Home
                            </a>
                            <a href="/login" className="text-blue-500 mr-5">
                                Click here to login
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    return <p>Loading profile...</p>;
};

export default withRouter(Callback);
