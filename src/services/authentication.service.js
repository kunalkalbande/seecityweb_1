/* eslint-disable no-undef */
import { BehaviorSubject } from 'rxjs';
import auth0 from 'auth0-js';
import users_db from '../data/users';
import { apolloClient } from '../api/graphql/apolloClient';
import { loader } from 'graphql.macro';
import ForgotPassword from '../pages/auth/reset/ResetPassword';
import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { userById } from '../api/graphql/queries';
import { from } from 'apollo-link';
import { addUsers } from '../api/graphql/mutations';
const currentUserSubject = new BehaviorSubject(
    JSON.parse(localStorage.getItem('currentUser'))
);

const loginFacebook = () => {
    authenticationService.webAuth.authorize({
        connection: 'facebook',
    });
};

const loginGoogle = () => {
    authenticationService.webAuth.authorize({
        connection: 'google-oauth2',
    });
};

const login = (username, password) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            authenticationService.webAuth.login(
                {
                    username: username,
                    password: password,
                    realm: 'Username-Password-Authentication',
                },
                function (err) {
                    if (err) resolve(err);
                }
            );
        }, 1000);
    });
};

const hasRoles = (assigned = [], required = []) => {
    let intersection = required.filter((x) => {
        if (assigned.indexOf(x) != -1) return true;
        else return false;
    });
    let filtered = [...new Set(intersection)];
    return filtered.length === required.length;
};

const SignUp = (email, password, firstName, lastName, phone) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            var isEmailConfirmationPage = false;
            authenticationService.webAuth.signup(
                {
                    connection: 'Username-Password-Authentication',
                    email: email,
                    password: password,
                    user_metadata: { email, firstName, lastName, phone },
                },
                async function (err, result) {
                    if (result) {
                        isEmailConfirmationPage = true;
                        sessionStorage.setItem(
                            'isEmailVerified',
                            isEmailConfirmationPage
                        );
                        await apolloClient.performMutation(addUsers, {
                            user: {
                                name: firstName + ' ' + lastName,
                                userId: 'auth0|' + result.Id,
                                firstName: firstName,
                                lastName: lastName,
                                email: email,
                                phone: phone,
                            },
                        });
                        resolve(result);
                    } else if (err) {
                        isEmailConfirmationPage = false;
                        sessionStorage.setItem(
                            'isEmailVerified',
                            isEmailConfirmationPage
                        );
                        resolve(err);
                    }
                }
            );
        }, 1000);
    });
};

const findAccount = (email) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            authenticationService.webAuth.changePassword(
                {
                    connection: 'Username-Password-Authentication',
                    email: email,
                },
                function (err, result) {
                    if (err) alert(err);
                }
            );
        }, 1000);
    });
};

const handleAuthentication = () => {
    return new Promise((resolve, reject) => {
        authenticationService.webAuth.parseHash(async (err, authResult) => {
            //console.log(authResult);
            // if (err) return reject(err);
            if (err) return resolve(err);
            if (!authResult || !authResult.idToken) {
                //return reject(err);
                return resolve(err);
            }

            localStorage.setItem('access_token', authResult.accessToken);
            localStorage.setItem('id_token', authResult.idToken);

            let user = {};
            if (authResult.idTokenPayload['https://guestx.com/user_metadata']) {
                user.forceReset =
                    authResult.idTokenPayload[
                        'https://guestx.com/user_metadata'
                    ].forceReset;
            }

            user.firstName = authResult.idTokenPayload.name;
            user.lastName = authResult.idTokenPayload.nickname;
            user.role =
                authResult.idTokenPayload['https://guestx.com/roles'][0];
            //user.email = authResult.idTokenPayload["https://guestx.com/app_metadata"].Email;
            user.email = authResult.idTokenPayload.name;

            //user.roles = ['Member', 'MemberAdmin', 'Sella', 'Redeema'];
            //user.bag = { biff: 'tanner' };
            user.roles = authResult.idTokenPayload['https://guestx.com/roles'];
            const userId = authResult.idTokenPayload.sub;
            user.userId = userId;
            sessionStorage.setItem('id', userId);

            var userqueryresp = await apolloClient.performQuery(userById, {
                userId: userId,
            });
            console.log(userqueryresp);
            if (userqueryresp.userById) {
                var data = { ...userqueryresp.userById };

                user.firstName = data.email;
                if (data.account != null) {
                    user.bag = { accountId: data.account._id };
                }
            } else {
                const userData = users_db.find((a) => a.id === userId);
                if (userData) {
                    user.bag = { id: userId, email: userData.email };
                }
            }
            localStorage.setItem('currentUser', JSON.stringify(user));
            currentUserSubject.next(user);
            console.log(user);
            resolve(user);
        });
    });
};

const getManagementAccessToken = async () => {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            grant_type: 'client_credentials',
            client_id: 'kNL4I9HYvXMigJezbtYTqYTPuISsmjRL',
            client_secret:
                'kxaV2fI_TrRGI2WuYvphHFqYgtFM6o2Z0QXc1RgGE-bGegVHZOL39urHj4Eu-NO0',
            audience: 'https://seecity.auth0.com/api/v2/',
        }),
    };
    await fetch('https://seecity.auth0.com/oauth/token', requestOptions)
        .then((response) => response.json())
        .then((data) => {
            sessionStorage.setItem('Token', data.access_token);
        });
};

const logout = () => {
    localStorage.removeItem('currentUser');
    localStorage.clear();
    currentUserSubject.next(null);
};

export const authenticationService = {
    webAuth: new auth0.WebAuth({
        domain: process.env.REACT_APP_AUTH0_DOMAIN,
        audience: process.env.REACT_APP_AUTH0_AUDIENCE,
        clientID: process.env.REACT_APP_AUTH0_CLIENT_ID,
        redirectUri: 'https://myseecity.netlify.app/callback',
        responseType: 'token id_token',
        scope: 'openid profile',
    }),
    login,
    logout,
    SignUp,
    hasRoles,
    findAccount,
    handleAuthentication,
    loginFacebook,
    loginGoogle,
    getManagementAccessToken,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue() {
        return currentUserSubject.value;
    },
};
