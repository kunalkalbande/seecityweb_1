import { createStore } from 'redux';
import rootReducer from './reducers';

const persistedState = localStorage.getItem('state')
    ? JSON.parse(localStorage.getItem('state'))
    : {};

console.log(persistedState);
const store = createStore(rootReducer, persistedState);

store.subscribe(() => {
    localStorage.setItem('state', JSON.stringify(store.getState()));
});

export default store;
