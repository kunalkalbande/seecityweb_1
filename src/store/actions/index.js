import actionTypes from './../actionTypes';

export const setAccount = (args) => {
    return {
        type: actionTypes.SET_ACCOUNT,
        payload: { ...args },
    };
};

export const setAccountFilter = (filter) => {
    return {
        type: actionTypes.SET_ACCOUNT_FILTER,
        filter,
    };
};

export const setMarketFilter = (args) => {
    return {
        type: actionTypes.SET_MARKET_FILTER,
        payload: { id: args },
    };
};

export const setUser = (user) => {
    return {
        type: actionTypes.SET_USER,
        user,
    };
};
