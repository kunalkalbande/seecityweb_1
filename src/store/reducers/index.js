import { combineReducers } from 'redux';
import actionTypes from './../actionTypes';

const selectedAccount = (state = {}, action) => {
    if (action.type === actionTypes.SET_ACCOUNT) {
        return { ...state, ...action.payload };
    }
    return state;
};

const marketFilter = (state = {}, action) => {
    if (action.type === actionTypes.SET_MARKET_FILTER) {
        return { ...state, ...action.payload };
    }
    return state;
};

const accountFilter = (state = '', action) => {
    if (action.type === actionTypes.SET_ACCOUNT_FILTER) {
        return action.filter;
    }
    return state;
};

const user = (state = {}, action) => {
    let initials = '';
    if (action.type === actionTypes.SET_USER) {
        initials = action.user.firstName
            .charAt(0)
            .concat(action.user.lastName.charAt(0));
        return { initials, ...action.user };
    }
    return state;
};

export default combineReducers({
    user,
    accountFilter,
    marketFilter,
    selectedAccount,
});
